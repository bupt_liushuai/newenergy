package com.newenergy.ui.equitpment;

import com.newenergy.ui.base.bean.BaseListBean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ls on 17/9/22.
 */

public class EquipmentItemBean extends BaseListBean {

    private List<DevwarinfoBean> devwarinfo;

    public List<DevwarinfoBean> getDevwarinfo() {
        return devwarinfo;
    }

    public void setDevwarinfo(List<DevwarinfoBean> devwarinfo) {
        this.devwarinfo = devwarinfo;
    }

    public static class DevwarinfoBean implements Serializable{
        /**
         * warninginfo : 温度过高
         * createtime : 2017-08-29 10:44:07
         * id : 1
         * devname : 设备1_1
         * blockname : 地块1
         */

        private String warninginfo;
        private String createtime;
        private int id;
        private String devname;
        private String blockname;

        public String getWarninginfo() {
            return warninginfo;
        }

        public void setWarninginfo(String warninginfo) {
            this.warninginfo = warninginfo;
        }

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDevname() {
            return devname;
        }

        public void setDevname(String devname) {
            this.devname = devname;
        }

        public String getBlockname() {
            return blockname;
        }

        public void setBlockname(String blockname) {
            this.blockname = blockname;
        }
    }
}
