package com.newenergy.ui.equitpment.detail;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.patrol.viewer.PicViewerFragment;

/**
 * Created by ls on 17/9/12.
 */

public class EquipmentDetailListAdapter extends BaseQuickAdapter<EquipmentDetailBean.DevrecordsBean, BaseViewHolder> {
    public EquipmentDetailListAdapter() {
        super(R.layout.item_equipment_detail);
    }


    @Override
    protected void convert(BaseViewHolder helper, final EquipmentDetailBean.DevrecordsBean item) {
        //helper.setText()
        helper.setBackgroundRes(R.id.check, R.drawable.ic_equipment_blue);
        if (item.getStatus() == 1) {//未上传
            helper.setText(R.id.check, mContext.getText(R.string.equipment_detail_upload));
            helper.setBackgroundRes(R.id.check, R.drawable.ic_equipment_camera);
        } else if (item.getStatus() == 3) {//已上传
            helper.setText(R.id.check, mContext.getText(R.string.equipment_detail_check));
        }

        helper.setText(R.id.device_name, String.format(mContext.getString(R.string.equipment_name), item.getDevname()));

        helper.getView(R.id.check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentArgs args = new FragmentArgs();
                args.add(PicViewerFragment.PARAM_ID, String.valueOf(item.getId()));
                args.add(PicViewerFragment.PARAM_TYPE, PicViewerFragment.PARAM_TYPE_MAINTAIN);
                FragmentContainerActivity.launch((BaseActivity) mContext, PicViewerFragment.class, args);
            }
        });

    }
}
