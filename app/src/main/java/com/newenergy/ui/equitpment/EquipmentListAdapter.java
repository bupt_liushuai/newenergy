package com.newenergy.ui.equitpment;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;

/**
 * Created by ls on 17/9/12.
 */

public class EquipmentListAdapter extends BaseQuickAdapter<EquipmentItemBean.DevwarinfoBean, BaseViewHolder> {
    public EquipmentListAdapter() {
        super(R.layout.item_equipment_list_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, final EquipmentItemBean.DevwarinfoBean item) {
        helper.setText(R.id.equipmentName, String.format(mContext.getResources().getString(R.string.equipment_name), item.getDevname()));
        helper.setText(R.id.equipmentBlock, String.format(mContext.getResources().getString(R.string.equipment_section), item.getBlockname()));
        helper.setBackgroundRes(R.id.equipmentIcon, R.drawable.ic_reason);
        helper.setText(R.id.equipmentIcon, item.getWarninginfo());
        helper.setText(R.id.equipmentDate, item.getCreatetime());
    }
}
