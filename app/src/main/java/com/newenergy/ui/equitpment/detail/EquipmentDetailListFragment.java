package com.newenergy.ui.equitpment.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.BaseDatePickFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

/**
 * Created by liushuai on 2017/9/4.
 */

public class EquipmentDetailListFragment extends BaseDatePickFragment<EquipmentDetailBean.DevrecordsBean, EquipmentDetailBean> {

    public final static String TAG = "EquipmentDetailListFragment";
    public final static String PARAM_DEVICE_ID = "param_device_id";
    public final static String PARAM_TITLE = "param_title";

    private String mId;
    private String mTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = getArguments().getString(PARAM_DEVICE_ID);
            mTitle = getArguments().getString(PARAM_TITLE);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!TextUtils.isEmpty(mTitle)) {
            setActionBarTitle(mTitle);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDatePickView().setDayTimeInterval(7);//设置间隔为一周
    }

    @Override
    protected BaseVolleyRequest<EquipmentDetailBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getDevRec(getDatePickView().getLeftTime(), getDatePickView().getRightTime(), getPageIndex(), mId);
        return new CommonRequest<>(r, new IParseNetwork<EquipmentDetailBean>() {
            @Override
            public EquipmentDetailBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, EquipmentDetailBean.class);
            }
        });
    }

    @Override
    protected boolean checkHasMore(EquipmentDetailBean response) {
        return response != null && response.getDevrecords() != null && response.getDevrecords().size() == response.getPageinfo().getPageNumber();
    }

    @Override
    protected boolean checkValidResponse(EquipmentDetailBean response) {
        return response != null && response.getDevrecords() != null;
    }

    @Override
    protected void updateAdapterData(BaseQuickAdapter<EquipmentDetailBean.DevrecordsBean, BaseViewHolder> adapter, EquipmentDetailBean response, boolean isRefresh, boolean isNetResponse) {
        if (isRefresh) {
            adapter.setNewData(response.getDevrecords());
        } else {
            adapter.addData(response.getDevrecords());
        }
    }

    @Override
    protected BaseQuickAdapter<EquipmentDetailBean.DevrecordsBean, BaseViewHolder> createAdapter() {
        return new EquipmentDetailListAdapter();
    }
}
