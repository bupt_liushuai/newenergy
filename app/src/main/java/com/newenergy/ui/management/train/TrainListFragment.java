package com.newenergy.ui.management.train;

import android.os.Bundle;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.BaseDatePickFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestConstant;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

/**
 * Created by liushuai on 2017/9/21.
 */

public class TrainListFragment extends BaseDatePickFragment<TrainListBean.TrainingBean, TrainListBean> {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle(getResources().getString(R.string.train_title));
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_train_list;
    }

    @Override
    protected BaseVolleyRequest<TrainListBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getTraining(getDatePickView().getLeftTime(), getDatePickView().getRightTime(), getPageIndex());
        return new CommonRequest<>(r, new IParseNetwork<TrainListBean>() {
            @Override
            public TrainListBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, TrainListBean.class);
            }
        });
    }

    @Override
    protected boolean checkHasMore(TrainListBean response) {
        return response != null && response.getTraining() != null && response.getTraining().size() == RequestConstant.REQUEST_PAGE_SIZE;
    }

    @Override
    protected boolean checkValidResponse(TrainListBean response) {
        return false;
    }

    @Override
    protected void updateAdapterData(BaseQuickAdapter<TrainListBean.TrainingBean, BaseViewHolder> adapter, TrainListBean response, boolean isRefresh, boolean isNetResponse) {
        if (isRefresh) {
            adapter.setNewData(response.getTraining());
        } else {
            adapter.addData(response.getTraining());
        }
    }

    @Override
    protected BaseQuickAdapter<TrainListBean.TrainingBean, BaseViewHolder> createAdapter() {
        return new TrainListAdapter();
    }
}
