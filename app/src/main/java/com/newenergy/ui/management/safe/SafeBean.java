package com.newenergy.ui.management.safe;

import com.newenergy.ui.base.bean.BaseListBean;

import java.util.List;

/**
 * Created by tongwenwen on 2018/3/13.
 */

public class SafeBean extends BaseListBean {

    /**
     * status : 1
     * patrollist : [{"createtime":"2018-01-19","id":8,"username":"张三","manageoptimizetype":1,"memo":"123","name":"2018年01月18日安全巡查报告"}]
     * pageinfo : {"totalNumber":1,"currentPage":1,"totalPage":1,"pageNumber":20,"dbIndex":0,"dbNumber":20,"lpage":1,"rpage":1}
     */

    private List<PatrollistBean> patrollist;

    public List<PatrollistBean> getPatrollist() {
        return patrollist;
    }

    public void setPatrollist(List<PatrollistBean> patrollist) {
        this.patrollist = patrollist;
    }


    public static class PatrollistBean {
        /**
         * createtime : 2018-01-19
         * id : 8
         * username : 张三
         * manageoptimizetype : 1
         * memo : 123
         * name : 2018年01月18日安全巡查报告
         */

        private String createtime;
        private int id;
        private String username;
        private int manageoptimizetype;
        private String memo;
        private String name;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getManageoptimizetype() {
            return manageoptimizetype;
        }

        public void setManageoptimizetype(int manageoptimizetype) {
            this.manageoptimizetype = manageoptimizetype;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
