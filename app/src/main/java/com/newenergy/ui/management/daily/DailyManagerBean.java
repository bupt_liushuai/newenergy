package com.newenergy.ui.management.daily;

import com.newenergy.ui.base.bean.BaseBean;

import java.util.List;

/**
 * Created by tongwenwen on 2018/3/13.
 */

public class DailyManagerBean extends BaseBean {

    /**
     * dailymanagelist : [{"createtime":1516185287000,"id":3,"manageoptimizetype":2,"name":"2018年01月17日日报管理报告"},{"createtime":1516185415000,"id":4,"username":"张三","manageoptimizetype":1,"memo":"123","name":"2018年01月17日日报管理报告"},{"createtime":1516238253000,"id":5,"username":"张三","manageoptimizetype":1,"memo":"123","name":"2018年01月18日日报管理报告"},{"createtime":1516239743000,"id":7,"username":"张三","manageoptimizetype":1,"memo":"123","name":"2018年01月18日日报管理报告"},{"createtime":1516255912000,"id":9,"username":"张三","manageoptimizetype":1,"memo":"123","name":"2018年01月18日日报管理报告"},{"createtime":1516255923000,"id":10,"username":"张三","manageoptimizetype":1,"memo":"123123","name":"2018年01月18日日报管理报告"},{"createtime":1516869745000,"id":11,"username":"张三","manageoptimizetype":1,"memo":"","name":"2018年01月25日日报管理报告"}]
     * status : 1
     * pageinfo : {"totalNumber":7,"currentPage":1,"totalPage":1,"pageNumber":10,"dbIndex":0,"dbNumber":10,"lpage":1,"rpage":1}
     */
    private PageinfoBean pageinfo;
    private List<DailymanagelistBean> dailymanagelist;

    public PageinfoBean getPageinfo() {
        return pageinfo;
    }

    public void setPageinfo(PageinfoBean pageinfo) {
        this.pageinfo = pageinfo;
    }

    public List<DailymanagelistBean> getDailymanagelist() {
        return dailymanagelist;
    }

    public void setDailymanagelist(List<DailymanagelistBean> dailymanagelist) {
        this.dailymanagelist = dailymanagelist;
    }

    public static class PageinfoBean {
        /**
         * totalNumber : 7
         * currentPage : 1
         * totalPage : 1
         * pageNumber : 10
         * dbIndex : 0
         * dbNumber : 10
         * lpage : 1
         * rpage : 1
         */

        private int totalNumber;
        private int currentPage;
        private int totalPage;
        private int pageNumber;
        private int dbIndex;
        private int dbNumber;
        private int lpage;
        private int rpage;

        public int getTotalNumber() {
            return totalNumber;
        }

        public void setTotalNumber(int totalNumber) {
            this.totalNumber = totalNumber;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getTotalPage() {
            return totalPage;
        }

        public void setTotalPage(int totalPage) {
            this.totalPage = totalPage;
        }

        public int getPageNumber() {
            return pageNumber;
        }

        public void setPageNumber(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        public int getDbIndex() {
            return dbIndex;
        }

        public void setDbIndex(int dbIndex) {
            this.dbIndex = dbIndex;
        }

        public int getDbNumber() {
            return dbNumber;
        }

        public void setDbNumber(int dbNumber) {
            this.dbNumber = dbNumber;
        }

        public int getLpage() {
            return lpage;
        }

        public void setLpage(int lpage) {
            this.lpage = lpage;
        }

        public int getRpage() {
            return rpage;
        }

        public void setRpage(int rpage) {
            this.rpage = rpage;
        }
    }

    public static class DailymanagelistBean {
        /**
         * createtime : 1516185287000
         * id : 3
         * manageoptimizetype : 2
         * name : 2018年01月17日日报管理报告
         * username : 张三
         * memo : 123
         */

        private String createtime;
        private int id;
        private int manageoptimizetype;
        private String name;
        private String username;
        private String memo;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getManageoptimizetype() {
            return manageoptimizetype;
        }

        public void setManageoptimizetype(int manageoptimizetype) {
            this.manageoptimizetype = manageoptimizetype;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }
    }
}
