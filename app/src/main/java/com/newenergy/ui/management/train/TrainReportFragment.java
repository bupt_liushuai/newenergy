package com.newenergy.ui.management.train;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.newenergy.R;
import com.newenergy.common.constant.Constant;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.common.utils.PrHelper;
import com.newenergy.common.utils.ViewUtils;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.web.WebFragment;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.Call;

/**
 * Created by liushuai on 2017/9/20.
 */

public class TrainReportFragment extends ABaseRequestFragment<TrainReportBean> {
    public final static String PARAM_TRAIN_ID = "param_train_id";
    public final static String PARAM_TRAIN_PATH = "param_train_path";
    private static final int EXTERNAL_STORAGE_PERMISSION = 1;

    private String mTrainId;
    @BindView(R.id.train_mark)
    ImageView mTrainMark;
    @BindView(R.id.train_des)
    TextView mTrainDes;

    @BindView(R.id.download)
    Button mBtn;
    @BindView(R.id.check)
    Button mCheckBtn;
    private String mFileName;

    private MaterialDialog mDialog;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarTitle(getString(R.string.train_report_title));
        if (getArguments() != null) {
            mTrainId = getArguments().getString(PARAM_TRAIN_ID);
        }
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_train_pdf_viewer;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDialog = new MaterialDialog.Builder(getActivity())
                .content("正在下载")
                .progress(true, 100)
                .build();

    }

    @Override
    public void onDestroy() {
        if (mDialog != null) {
            mDialog.dismiss();
            mDialog = null;
        }
        super.onDestroy();

    }

    @OnClick({R.id.download, R.id.check})
    void onClick(View v) {
        switch (v.getId()) {
            case R.id.download:
                downOrUploadPdf((Button) v);
                break;
            case R.id.check:
                checkPdf();
                break;

        }
    }


    //直接跳转H5不在走原生页
    private void checkPdf() {
//        FragmentArgs args = new FragmentArgs();
//        args.add(TrainReportFragment.PARAM_TRAIN_PATH, mFileName);
//        FragmentContainerActivity.launch(getActivity(), TrainReportViewerFragment.class, args);
        //直接跳转H5不在走原生页
        String url = String.format(RequestUrl.VIEW_RPT, Constant.PXYL, mTrainId, "");
        WebFragment.Launch(getActivity(), url, getResources().getString(R.string.manager_train));
    }

    private void downOrUploadPdf(Button b) {
        if (getString(R.string.train_report_download).equals(b.getText().toString())) {
            downloadPdf();
        } else {
            FragmentArgs args = new FragmentArgs();
            args.add(TrainReportFragment.PARAM_TRAIN_ID, mTrainId);
            FragmentContainerActivity.launch(getActivity(), TrainPdfUploadFragment.class, args);
        }
    }

    @Override
    public TrainReportBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(TrainReportBean response) {

    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }


    @Override
    protected BaseVolleyRequest<TrainReportBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getTrainReport(mTrainId);
        return new CommonRequest<>(r, new IParseNetwork<TrainReportBean>() {
            @Override
            public TrainReportBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, TrainReportBean.class);
            }
        });
    }


    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, TrainReportBean response) {
        super.onResponse(isNetResponse, isRefresh, response);
        TrainReportBean.TrainingBean trainingBean = null;
        if (response != null) {
            List<TrainReportBean.TrainingBean> list = response.getTraining();
            if (list != null && !list.isEmpty()) {
                //已上传
                trainingBean = list.get(0);
            }
        }
        initView(trainingBean);
    }

    private void initView(TrainReportBean.TrainingBean trainingBean) {
        if (trainingBean != null && !TextUtils.isEmpty(trainingBean.getReportpath())) { //已上传
            mTrainMark.setImageResource(R.drawable.ic_train_yi_bg);
            mFileName = trainingBean.getReportpath();
            mBtn.setText(getString(R.string.train_report_download));
            ViewUtils.setVisible(mCheckBtn, true);
        } else {
            mTrainMark.setImageResource(R.drawable.ic_train_wei_bg);
            mBtn.setText(getString(R.string.train_report_upload));
            ViewUtils.setVisible(mCheckBtn, false);
        }
    }

    private void downloadPdf() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION);
        } else {

            mDialog.show();
            OkHttpUtils.get()
                    .addHeader("Cookie", PrHelper.getPrSession())
                    .url(String.format(RequestUrl.GET_REPROT_FILE, mFileName))
                    .build().execute(new FileCallBack(Constant.PATH_PDF, mFileName) {
                @Override
                public void onError(Call call, Exception e, int id) {
                    if (getView() != null) {
                        Toast.makeText(getContext(), "下载失败," + e.getMessage(), Toast.LENGTH_SHORT).show();
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                    }
                }

                @Override
                public void onResponse(File response, int id) {
                    if (getView() != null) {
                        Toast.makeText(getContext(), "下载成功, 文件下载路径:" + response.getAbsolutePath(), Toast.LENGTH_LONG).show();
                        if (mDialog != null) {
                            mDialog.dismiss();
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //goToScanActivity();
                    downloadPdf();
                } else {
                    Toast.makeText(getActivity(), "请开权限", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

}
