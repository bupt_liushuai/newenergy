package com.newenergy.ui.management.daily;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.bean.BaseBean;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.base.network.task.VolleyManager;
import com.newenergy.ui.management.upload.ManagerDataBean;
import com.newenergy.ui.patrol.upload.FileUploadBean;
import com.newenergy.ui.patrol.upload.PicSelectFragment;

import java.util.List;

import butterknife.BindView;

/**
 * Created by tongwenwen on 2018/3/14.
 */

public class DailyManagerUploadFragment extends PicSelectFragment {


    @BindView(R.id.recyclerView1)
    RecyclerView mRecyclerView1;
    @BindView(R.id.group1)
    TextView mGroup1;
    @BindView(R.id.group2)
    TextView mGroup2;
    @BindView(R.id.et_desc)
    EditText mEditText;
    private int mGroupId = 1;//所属分组 默认物业
    private ManagerAdapter mManagerAdapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle(getResources().getString(R.string.manager_daily));
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_daily_manager_pic_select;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getUploadUrl() {
        return RequestUrl.POST_UPLOAD;
    }

    @Override
    protected void dealResponse(int requestId, FileUploadBean response) {
        super.dealResponse(requestId, response);

        if (response != null && response.getFilepath() != null) {
            //图片上传成功
            String memo = mEditText.getText().toString();
            List<FileUploadBean.FilepathBean> filepathBeans = response.getFilepath();
            String filePath = "";
            for (int i = 0; i < filepathBeans.size(); i++) {
                filePath += filepathBeans.get(i).getPicurl() + ",";
                if (i == filepathBeans.size() - 1) {
                    if (!TextUtils.isEmpty(filePath)) {
                        filePath = filePath.substring(0, filePath.length() - 1);
                    }
                }
            }

            String id = "";
            if (mManagerAdapter.getData() != null) {
                int count = mManagerAdapter.getItemCount();
                for (int j = 0; j < count; j++) {
                    View v = mManagerAdapter.getViewByPosition(mRecyclerView1, j, R.id.content);
                    if (v != null && v.isSelected() && mManagerAdapter.getItem(j) != null) {
                        id += mManagerAdapter.getItem(j).getId() + ",";
                    }
                    if (j == count - 1) {
                        if (!TextUtils.isEmpty(id)) {
                            id = id.substring(0, id.length() - 1);
                        }
                    }
                }
            }
            System.out.println("id =" + id + "filePath = " + filePath + "memo = " + memo);

            String url = String.format(RequestUrl.POST_UPLOAD_INFO, id, mGroupId, memo, filePath);


            CommonRequest<BaseBean> r = new CommonRequest<BaseBean>(RequestDefine.postManagerInfo(url), new IParseNetwork<BaseBean>() {
                @Override
                public BaseBean parseNetworkResponse(String jsonStr) {
                    return JsonUtils.fromJson(jsonStr, BaseBean.class);
                }
            });
            r.setResponseListener(new IResponseListener<BaseBean>() {
                @Override
                public void onResponse(int requestId, BaseBean response) {
                    if (response.getStatus().equals("1")) {
                        Toast.makeText(getContext(), "保存成功", Toast.LENGTH_SHORT).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getActivity().finish();
                            }
                        }, 300);
                        return;
                    }
                    Toast.makeText(getContext(), TextUtils.isEmpty(response.getMsg()) ? response.getMsg() : "保存失败", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onErrorResponse(int requestId, VolleyError error) {

                }
            });
            VolleyManager.addRequest(r);

        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        CommonRequest<ManagerDataBean> request = new CommonRequest<>(RequestDefine.getManagerDatas(), new IParseNetwork<ManagerDataBean>() {
            @Override
            public ManagerDataBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, ManagerDataBean.class);
            }
        });

        request.setResponseListener(new IResponseListener<ManagerDataBean>() {
            @Override
            public void onResponse(int requestId, ManagerDataBean response) {
                initRoll(response);
                updateGroup();
            }

            @Override
            public void onErrorResponse(int requestId, VolleyError error) {

            }
        });

        VolleyManager.addRequest(request);

        mGroup1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGroupId = 1;
                updateGroup();
            }
        });

        mGroup2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGroupId = 2;
                updateGroup();
            }
        });
    }

    private void initRoll(ManagerDataBean response) {
        mRecyclerView1.setLayoutManager(new GridLayoutManager(getContext(), 4));
        if (mManagerAdapter == null) {
            mManagerAdapter = new ManagerAdapter();
        }
        mManagerAdapter.addData(response.getRoles());
        mRecyclerView1.setAdapter(mManagerAdapter);
    }


    private class ManagerAdapter extends BaseQuickAdapter<ManagerDataBean.RolesBean, BaseViewHolder> {

        public ManagerAdapter() {
            super(R.layout.item_manager_upload);
        }

        @Override
        protected void convert(final BaseViewHolder helper, final ManagerDataBean.RolesBean item) {
            //helper.setText()
            helper.setText(R.id.content, item.getName());
            helper.getView(R.id.content).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.isSelected()) {
                        v.setSelected(false);
                    } else {
                        v.setSelected(true);
                    }
                }
            });
        }
    }

    @Override
    protected boolean readyCommit() {
        //图片上传成功
        String memo = mEditText.getText().toString();

        String id = "";
        if (mManagerAdapter.getData() != null) {
            int count = mManagerAdapter.getItemCount();
            for (int i = 0; i < count; i++) {
                View v = mManagerAdapter.getViewByPosition(mRecyclerView1, i, R.id.content);
                if (v != null && v.isSelected() && mManagerAdapter.getItem(i) != null) {
                    id += mManagerAdapter.getItem(i).getId() + ",";
                }
                if (i == count - 1) {
                    if (!TextUtils.isEmpty(id)) {
                        id.substring(0, id.length() - 1);
                    }
                }
            }
        }
        if (TextUtils.isEmpty(memo) || TextUtils.isEmpty(id)) {
            Toast.makeText(getContext(), "请填写完整信息", Toast.LENGTH_SHORT).show();
            return false;
        }
        return super.readyCommit();
    }

    private void updateGroup() {
        if (mGroupId == 1) {
            mGroup1.setSelected(true);
            mGroup1.setTextColor(getResources().getColor(R.color.colorWhite));
            mGroup2.setSelected(false);
            mGroup2.setTextColor(getResources().getColor(R.color.colorBlack));
        } else {
            mGroup1.setSelected(false);
            mGroup1.setTextColor(getResources().getColor(R.color.colorBlack));
            mGroup2.setSelected(true);
            mGroup1.setTextColor(getResources().getColor(R.color.colorWhite));
        }
    }
}
