package com.newenergy.ui.management;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.SegmentTabLayout;
import com.newenergy.R;
import com.newenergy.ui.base.fragment.ABaseTabFragment;
import com.newenergy.ui.management.daily.DailyManagerFragment;
import com.newenergy.ui.management.safe.SafeFragment;
import com.newenergy.ui.management.train.TrainListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 管理优化
 * Created by ls on 2018/3/8.
 */

public class ManagerFragment extends ABaseTabFragment {

    @Override
    public int inflateContentView() {
        return R.layout.fragment_manager;
    }

    @Override
    protected List<FragmentParam> initFramgentData() {
        ArrayList<FragmentParam> list = new ArrayList<>();
        list.add(new FragmentParam(getResources().getString(R.string.manager_daily), DailyManagerFragment.class.getName()));
        list.add(new FragmentParam(getResources().getString(R.string.manager_safe), SafeFragment.class.getName()));
        list.add(new FragmentParam(getResources().getString(R.string.manager_train), TrainListFragment.class.getName()));
        return list;
    }

}
