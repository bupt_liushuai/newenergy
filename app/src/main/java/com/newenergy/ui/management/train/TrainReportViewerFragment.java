package com.newenergy.ui.management.train;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.newenergy.R;
import com.newenergy.common.utils.PrHelper;
import com.newenergy.common.utils.ViewUtils;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.base.network.RequestUrl;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import java.io.File;

import butterknife.BindView;
import okhttp3.Call;

/**
 * Created by liushuai on 2017/10/18.
 */

public class TrainReportViewerFragment extends ABaseFragment {
    public final static String PARAM_REPORT_TYPE = "param_report_type";
    public final static int PARAM_REPORT_TYPE_REMOTE = 0;
    public final static int PARAM_REPORT_TYPE_LOCAL = 1;
    @BindView(R.id.pdfView)
    PDFView mPDFView;
    @BindView(R.id.progress)
    LinearLayout mProgress;

    private String mPdfName;
    private int mReportType = 0;

    @Override
    public int inflateContentView() {
        return R.layout.fragment_train_report_viewer;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarTitle(getString(R.string.train_report_title));

        if (getArguments() != null) {
            mPdfName = getArguments().getString(TrainReportFragment.PARAM_TRAIN_PATH);
            mReportType = getArguments().getInt(PARAM_REPORT_TYPE, 0);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mReportType == PARAM_REPORT_TYPE_REMOTE) {
            String filePath = getActivity().getFilesDir().getAbsolutePath();
            String url = String.format(RequestUrl.GET_REPROT_FILE, mPdfName);
            OkHttpUtils.get()
                    .addHeader("Cookie", PrHelper.getPrSession())
                    .url(url)
                    .build().execute(new FileCallBack(filePath, mPdfName) {
                @Override
                public void onError(Call call, Exception e, int id) {
                    if (getView() != null) {
                        mProgress.setVisibility(View.GONE);
                        Toast.makeText(getContext(), "加载失败," + e.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onResponse(File response, int id) {
                    if (getView() != null) {
                        mProgress.setVisibility(View.GONE);
                        showPdf(getActivity().getFilesDir().getAbsolutePath() + "/" + mPdfName);
                    }
                }
            });
        } else {
            ViewUtils.setVisible(mProgress, false);
            showPdf(mPdfName);
        }


    }

    private void showPdf(String path) {
        File f = new File(path);
        if (f.exists()) {
            mPDFView.fromFile(f)
                    .enableSwipe(true) // allows to block changing pages using swipe
                    .swipeHorizontal(false)
                    .enableDoubletap(true)
                    .defaultPage(0)
                    .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
                    .scrollHandle(null)
                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    .spacing(0)
                    .load();
        } else {
            Toast.makeText(getContext(), "文件加载失败", Toast.LENGTH_SHORT).show();
        }
    }
}
