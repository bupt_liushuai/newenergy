package com.newenergy.ui.management.safe;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.fragment.BaseDatePickFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.patrol.upload.PicSelectFragment;

import butterknife.OnClick;

/**
 * 日报管理
 * Created by liushuai on 2018/3/8.
 */

public class SafeFragment extends BaseDatePickFragment<SafeBean.PatrollistBean, SafeBean> {

    @Override
    public int inflateContentView() {
        return R.layout.fragment_daily_manger_list;
    }

    @Override
    protected BaseQuickAdapter<SafeBean.PatrollistBean, BaseViewHolder> createAdapter() {
        return new SafeListAdapter();
    }

    @Override
    protected BaseVolleyRequest<SafeBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getPatrolList(getDatePickView().getLeftTime(), getDatePickView().getRightTime(), getPageIndex());
        return new CommonRequest<>(r, new IParseNetwork<SafeBean>() {
            @Override
            public SafeBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, SafeBean.class);
            }
        });
    }

    @Override
    protected boolean checkHasMore(SafeBean response) {
        return response != null && response.getPatrollist() != null && !response.getPatrollist().isEmpty();
    }

    @Override
    protected boolean checkValidResponse(SafeBean response) {
        return response != null && response.getPatrollist() != null;
    }

    @Override
    protected void updateAdapterData(BaseQuickAdapter<SafeBean.PatrollistBean, BaseViewHolder> adapter, SafeBean response, boolean isRefresh, boolean isNetResponse) {
        if (isRefresh) {
            adapter.setNewData(response.getPatrollist());
        } else {
            adapter.addData(response.getPatrollist());
        }
    }

    @OnClick(R.id.add)
    void click(View v) {
        switch (v.getId()) {
            case R.id.add:
                FragmentArgs args = new FragmentArgs();
                args.add(PicSelectFragment.PARAM_COUNT, 3);
                FragmentContainerActivity.launch(getActivity(), SafeUploadFragment.class, args);
                break;
        }
    }


}
