package com.newenergy.ui.management.train;

import com.newenergy.ui.base.bean.BaseBean;

import java.util.List;

/**
 * Created by ls on 17/9/24.
 */

public class TrainReportBean extends BaseBean{

    private List<TrainingBean> training;

    public List<TrainingBean> getTraining() {
        return training;
    }

    public void setTraining(List<TrainingBean> training) {
        this.training = training;
    }

    public static class TrainingBean {
        /**
         * reportpath : d:/tt/ee/ww.pdf
         */

        private String reportpath;

        public String getReportpath() {
            return reportpath;
        }

        public void setReportpath(String reportpath) {
            this.reportpath = reportpath;
        }
    }
}
