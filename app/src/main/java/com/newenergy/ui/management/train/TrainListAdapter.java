package com.newenergy.ui.management.train;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;

/**
 * Created by ls on 17/9/12.
 */

public class TrainListAdapter extends BaseQuickAdapter<TrainListBean.TrainingBean, BaseViewHolder> {
    public TrainListAdapter() {
        super(R.layout.item_train_list_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, final TrainListBean.TrainingBean item) {
        helper.setText(R.id.trainName, String.format(mContext.getResources().getString(R.string.train_name), item.getName()));
        helper.setText(R.id.trainFrom, String.format(mContext.getResources().getString(R.string.train_from), item.getUsername()));
        if (1 == item.getTrainstatus()) {
            helper.setBackgroundRes(R.id.trainIcon, R.drawable.ic_train_wei);
        } else {
            helper.setBackgroundRes(R.id.trainIcon, R.drawable.ic_train_yi);
        }
        helper.setText(R.id.trainIcon, item.getTrainstatus_name());
        helper.setText(R.id.trainDate, item.getTraintime());

        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentArgs args = new FragmentArgs();
                args.add(TrainReportFragment.PARAM_TRAIN_ID, item.getTrain_id());
                FragmentContainerActivity.launch((BaseActivity) mContext, TrainReportFragment.class, args);
            }
        });
    }
}
