package com.newenergy.ui.management.daily;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.fragment.BaseDatePickFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.patrol.upload.PicSelectFragment;

import butterknife.OnClick;

/**
 * 日报管理
 * Created by tongwenwen on 2018/3/8.
 */

public class DailyManagerFragment extends BaseDatePickFragment<DailyManagerBean.DailymanagelistBean, DailyManagerBean> {

    @Override
    public int inflateContentView() {
        return R.layout.fragment_daily_manger_list;
    }

    @Override
    protected BaseQuickAdapter<DailyManagerBean.DailymanagelistBean, BaseViewHolder> createAdapter() {
        return new DailyManagerListAdapter();
    }

    @Override
    protected BaseVolleyRequest<DailyManagerBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getDailyManager(getDatePickView().getLeftTime(), getDatePickView().getRightTime(), getPageIndex());
        return new CommonRequest<>(r, new IParseNetwork<DailyManagerBean>() {
            @Override
            public DailyManagerBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, DailyManagerBean.class);
            }
        });
    }

    @Override
    protected boolean checkHasMore(DailyManagerBean response) {
        return response != null && response.getDailymanagelist() != null && !response.getDailymanagelist().isEmpty();
    }

    @Override
    protected boolean checkValidResponse(DailyManagerBean response) {
        return response != null && response.getDailymanagelist() != null;
    }

    @Override
    protected void updateAdapterData(BaseQuickAdapter<DailyManagerBean.DailymanagelistBean, BaseViewHolder> adapter, DailyManagerBean response, boolean isRefresh, boolean isNetResponse) {
        if (isRefresh) {
            adapter.setNewData(response.getDailymanagelist());
        } else {
            adapter.addData(response.getDailymanagelist());
        }
    }

    @OnClick(R.id.add)
    void click(View v) {
        switch (v.getId()) {
            case R.id.add:
                FragmentArgs args = new FragmentArgs();
                args.add(PicSelectFragment.PARAM_COUNT, 3);
                FragmentContainerActivity.launch(getActivity(), DailyManagerUploadFragment.class, args);
                break;
        }
    }


}
