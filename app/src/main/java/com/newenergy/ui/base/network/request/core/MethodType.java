package com.newenergy.ui.base.network.request.core;

/**
 * Created by lmhit on 16/7/19.
 */
public enum MethodType {
    //volley 不支持connect了
    @Deprecated
    CONNECT,
    DELETE,
    GET,
    HEAD,
    OPTIONS,
    PATCH,
    POST,
    PUT,
    TRACE
}
