package com.newenergy.ui.base.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.loadmore.SimpleLoadMoreView;
import com.newenergy.R;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;

import butterknife.BindView;
import butterknife.Unbinder;

/**
 * T : item
 * D : all
 * HD ：headerData
 */
public abstract class ABaseRequestListFragment<T, D, HD> extends ABaseRequestFragment<D> implements SwipeRefreshLayout.OnRefreshListener, BaseQuickAdapter.RequestLoadMoreListener {

    static final String TAG = "ABaseRequestListFragment";
    @BindView(R.id.recylcerView)
    RecyclerView mRecylcerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;

    private BaseQuickAdapter<T, BaseViewHolder> mAdapter;
    private int mPageIndex;
    private int mPrePageIndex; // 刷新成功才改变mPageIndex的值

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_base_list;
    }

    @Override
    public void setContentView(ViewGroup view) {
        super.setContentView(view);

        mRefreshLayout.setOnRefreshListener(this);
        mRefreshLayout.setColorSchemeColors(Color.rgb(47, 223, 189));
        mRecylcerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = createAdapter();
        mAdapter.setLoadMoreView(new SimpleLoadMoreView());
        mAdapter.setOnLoadMoreListener(this, mRecylcerView);
        mRecylcerView.setAdapter(mAdapter);
    }

    @Override
    protected BaseVolleyRequest<D> createNetRequest(boolean isRefresh) {
        return null;
    }

    @Override
    public void onRefresh() {
        loadNetData(true);
    }

    @Override
    public boolean loadNetData(boolean isRefresh) {
        return super.loadNetData(isRefresh);
    }



    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, D response) {
        super.onResponse(isNetResponse, isRefresh, response);
        //setRefreshCompleted(true);
        if (isRefresh) {
            mRefreshLayout.setRefreshing(false);
        } else {
            mAdapter.loadMoreComplete();
        }
        //mAdapter.setEnableLoadMore(true);
        if (getAdapter() != null) {
            updateAdapterData(getAdapter(), response, isRefresh, isNetResponse);
            refreshStateView(isNetResponse, response);

            updateFooterState(response);
            /*if (checkNeedUpdateFooter(isNetResponse, isRefresh, response)) {
            }*/
        }
        if (isNetResponse) {
//            resetLoadingFlag(isRefresh);
            if (checkValidResponse(response)) {
                mPageIndex++;
            } else {
                mPageIndex = mPrePageIndex;
            }
//            mIsRefreshTriggeredAuto = false;
        }
    }

    protected void updateFooterState(D response) {
        if (getAdapter() == null) {
            return;
        }
        if (supportLoadMore() && checkHasMore(response)) {
            getAdapter().loadMoreEnd(false);
        } else {
            getAdapter().loadMoreEnd(true);
        }
    }

    @Override
    protected void beforeLoadData(boolean isNetLoad, boolean isRefresh) {

        mPrePageIndex = mPageIndex;
        if (isRefresh) {
            mPageIndex = 0;
        }

        if (!getAdapter().getData().isEmpty()) {
            showProgressBar(isRefresh && shouldShowProgressBeforeLoad());
            showEmptyView(false);
            showErrorView(false);
        }else {
            super.beforeLoadData(isNetLoad, isRefresh);
        }
    }

    public String getPageIndex() {
        return String.valueOf(mPageIndex);
    }

    /**
     * 是否支持加载更多
     */
    protected boolean supportLoadMore() {
        return true;
    }

    abstract protected boolean checkHasMore(D response);

    /**
     * 是否需要更新footer
     * 触发条件：本地加载，下拉刷新(请求成功)，上拉刷新
     */
    protected boolean checkNeedUpdateFooter(boolean isNetResponse, boolean isRefresh, D response) {
        boolean needUpdate = true;
        if (isNetResponse && isRefresh) {
            needUpdate = checkValidResponse(response);
        }
        return needUpdate;
    }

    abstract protected boolean checkValidResponse(D response);

    @Override
    protected boolean shouldShowProgressBeforeLoad() {
        return getAdapter() == null || getAdapter().getData().isEmpty();
    }

    /**
     * empty errorView 的显示逻辑
     */
    protected void refreshStateView(boolean isNetResponse, D response) {
        if (isNetResponse && getAdapter() != null) {
            showEmptyView(getAdapter().getData().isEmpty());
        }
    }

    public BaseQuickAdapter<T, BaseViewHolder> getAdapter() {
        return mAdapter;
    }

    abstract protected void updateAdapterData(BaseQuickAdapter<T, BaseViewHolder> adapter, D response, boolean isRefresh, boolean isNetResponse);

    protected abstract BaseQuickAdapter<T, BaseViewHolder> createAdapter();

    @Override
    public void onError(boolean isRefresh, VolleyError error) {
        refreshStateView(isRefresh, null);
    }

    @Override
    public D loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(D response) {

    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }

    @Override
    public void onLoadMoreRequested() {
        mRefreshLayout.setEnabled(false);
        loadNetData(false);
        mRefreshLayout.setEnabled(true);
    }
}
