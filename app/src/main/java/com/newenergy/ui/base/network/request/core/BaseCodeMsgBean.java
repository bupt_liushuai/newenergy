package com.newenergy.ui.base.network.request.core;

import com.google.gson.annotations.SerializedName;

/**
 * Created by lijie on 16/9/8.
 * 带有msg字段的BaseCodeBean
 */
public class BaseCodeMsgBean extends BaseCodeBean {
    @SerializedName(value = "msg", alternate = {"message"})
    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
