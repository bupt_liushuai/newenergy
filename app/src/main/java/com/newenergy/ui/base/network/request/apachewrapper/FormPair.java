package com.newenergy.ui.base.network.request.apachewrapper;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

/**
 * Created by lijie on 16/12/28.
 * 封装{@link NameValuePair}和{@link BasicNameValuePair}
 */

public class FormPair {

    private NameValuePair value;

    public FormPair(String name, String value) {
        this.value = new BasicNameValuePair(name, value);
    }

    public String getName() {
        return value.getName();
    }

    public String getValue() {
        return value.getValue();
    }

    public NameValuePair getRealValue() {
        return value;
    }

}
