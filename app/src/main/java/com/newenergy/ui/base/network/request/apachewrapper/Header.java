package com.newenergy.ui.base.network.request.apachewrapper;

import org.apache.http.message.BasicHeader;

/**
 * Created by lijie on 16/12/28.
 * 封装{@link org.apache.http.Header}和{@link BasicHeader}
 */

public class Header {

    private org.apache.http.Header value;

    public Header(String name, String value) {
        this.value = new BasicHeader(name, value);
    }

    public String getName() {
        return value.getName();
    }

    public String getValue() {
        return value.getValue();
    }

    public org.apache.http.Header getRealValue() {
        return value;
    }

}
