package com.newenergy.ui.base.network;

/**
 * Created by liushuai on 2017/9/13.
 */

public class RequestUrl {
    public final static String BASE_URL = "http://120.77.50.222:8092/wit";
    //    public final static String BASE_URL = "http://60.30.168.34:9095/wit";
    //登录
    public final static String LOGIN_URL = BASE_URL + "/app/users/userlogin";//登录
    // 获取用户信息
    public final static String GET_USERINFO = BASE_URL + "/app/users/getuserinfo";
    //首页头图
    public final static String MAIN_HEAD_IMG = BASE_URL + "/images/android_home.jpg";
    public final static String MAINTAIN_ALL = BASE_URL + "/app/quemaint/getallquerymaintain";
    public final static String GET_DEV_MAININFO = BASE_URL + "/app/device/getdevmaininfo_new";
    public final static String GET_DEV_REPAIR = BASE_URL + "app/device/getdevrepair";
    //查询地块信息和设备列表
    public final static String DEVICEINFO = BASE_URL + "/app/device/getdeviceinfo";
    //表查询系统信息和设备列表
    public final static String GET_SYSTEM_DEVICEINFO = BASE_URL + "/app/device/getsystemdevinfo";
    //扫描二维码保存巡查维护信息
    public final static String SAVE_QUERY_MAINTAIN = BASE_URL + "/app/quemaint/savequerymaintain";
    //联系方式
    public final static String CONTACT_INFO = BASE_URL + "/app/contact/getcontactinfo";
    //群组管理
    public final static String GROUP_USER = BASE_URL + "/app/groupmanage/getgroupuser";
    //群组移动
    public final static String GROUP_USER_NODIFY = BASE_URL + "/app/groupmanage/modifyusergroup";
    //查询培训演练
    public final static String GET_TRAINING = BASE_URL + "/app/train/gettraining";
    //查询报警信息列表
    public final static String GET_WARN_INFO = BASE_URL + "/app/device/getdevwarinfo";
    //根据设备id查询设备维护记录
    public final static String GET_DEVREC = BASE_URL + "/app/device/getdevrec";
    //查询培训演练报告路径
    public final static String GET_TRAIN_REPORT = BASE_URL + "/app/train/gettrainreport";
    //上传演练报告
    public final static String POST_TRAIN_REPORT = BASE_URL + "/app/train/uploadtrainingreport?train_id=%s";
    //上传
    public final static String POST_UPLOADFILE = BASE_URL + "/app/quemaint/uploadquemaiimg?querymaintain_id=%s";
    //查看演练报告
    public final static String GET_REPROT_FILE = BASE_URL + "/app/download/trainingreport?path=%s";

    //巡查图片查看
    public final static String GET_QUERY_MAINTAIN_IMG = BASE_URL + "/app/quemaint/getQueryMaintainImg";
    //维护图片查看
    public final static String GET_DEVMAIN_RESULT_IMG = BASE_URL + "/app/device/getdevmainresultimg";
    public final static String QUEMAIIMG = BASE_URL + "/app/download/quemaiimg?path=%s";
    //头像上传
    public final static String UPLOAD_HEAD_IMG = BASE_URL + "/app/users/uploadheadimg";
    public final static String USER_HEAD_IMG = BASE_URL + "/app/download/userheadimg?path=%s";

    //监控列表
    public final static String GET_VIDEO_LIST = BASE_URL + "/app/vedio/vedioList";
    //监控详情
    public final static String GET_VIDEO_RUN = BASE_URL + "/app/vedio/runVedioUrl";

    //获取地块信息
    public final static String GET_COLD_BLOCK = BASE_URL + "/app/stati/getcoldblock";
    // 查询用冷信息
    public final static String GET_COLD_BLOCK_INFO = BASE_URL + "/app/stati/%s";
    //获取用电信息
    public final static String GET_ELEC_DATA = BASE_URL + "/app/stati/geteledatar";
    //成本计量
    public final static String GET_COST_COUNT = BASE_URL + "/app/costapp/getcostcount";
    ///管理优化

    //1.日报管理
    public final static String GET_DAILY_MANAGER = BASE_URL + "/app/manageopt/getdailymanage";
    //2.文件上传
    public final static String POST_UPLOAD = BASE_URL + "/app/manageopt/uploadailymanagerimg";

    public final static String POST_UPLOAD_INFO = BASE_URL + "/app/manageopt/savesecpatmemo?patgroup_ids=%s&managetype=%s&memo=%s&filepath=%s";
    //获取提示人员和所属分组
    public final static String GET_MANAGER_DATAS = BASE_URL + "/app/manageopt/getManageNeedDatas";

    //1.日报管理
    public final static String GET_PATROL_LIST = BASE_URL + "/app/manageopt/getsecpat";
    //实时参数
    public final static String POST_REAL_TIME_H5 = BASE_URL + "/app/realtime/toPage_app";

    //九、客户端管理
    public final static String GET_CUSTOMERS = BASE_URL + "/app/coustomer/getCustomers";
    //客户信息
    public final static String GET_CUSTOMERS_INFO = BASE_URL + "/app/coustomer/getcustomerinfo";

    //H5预览
    public final static String VIEW_RPT = BASE_URL + "/app/viewrpt?rptType=%s&rptId=%s&pro_id=%s";

}
