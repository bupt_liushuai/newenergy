package com.newenergy.ui.base.network.task;

import java.util.concurrent.TimeUnit;

import okhttp3.Dispatcher;
import okhttp3.OkHttpClient;

/**
 * Created by heq on 16/11/15.
 */

public class OkHttpManager {
    public static final int DEFAULT_READ_TIMEOUT_MILLIS = 15 * 1000;
    public static final int DEFAULT_WRITE_TIMEOUT_MILLIS = 15 * 1000;
    public static final int DEFAULT_CONNECT_TIMEOUT_MILLIS = 15 * 1000;
    private static final int MAX_REQUESTS_PER_HOST = 10;
    private static OkHttpManager sInstance = new OkHttpManager();
    private OkHttpClient mOkHttpClient;

    private OkHttpManager() {
        Dispatcher dispatcher = new Dispatcher();
        dispatcher.setMaxRequestsPerHost(MAX_REQUESTS_PER_HOST);
        mOkHttpClient = new OkHttpClient.Builder()
                .dispatcher(dispatcher)
                .readTimeout(DEFAULT_READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                .writeTimeout(DEFAULT_WRITE_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                .connectTimeout(DEFAULT_CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
                //.addNetworkInterceptor(new GzipRequestInterceptor())    //添加压缩的拦截器
//                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }

    public static OkHttpManager getInstance() {
        return sInstance;
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

}
