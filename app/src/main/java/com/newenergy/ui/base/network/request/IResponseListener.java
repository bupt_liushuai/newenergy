package com.newenergy.ui.base.network.request;

import com.android.volley.VolleyError;

/**
 * 封装volley的onresponse和onerror
 */
public interface IResponseListener<T> {
    /**
     * Called when a response is received.
     */
    void onResponse(int requestId, T response);

    /**
     * Callback method that an error has been occurred with the
     * provided error code and optional user-readable message.
     */
    void onErrorResponse(int requestId, VolleyError error);
}
