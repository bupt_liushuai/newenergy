package com.newenergy.ui.base.network.request;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.android.internal.http.multipart.Part;
import com.newenergy.ui.base.network.request.apachewrapper.FormPair;
import com.newenergy.ui.base.network.request.apachewrapper.Header;
import com.newenergy.ui.base.network.request.core.HttpConstants;
import com.newenergy.ui.base.network.request.core.MethodType;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.core.RequestBuilder;

import java.util.List;


/**
 * Created by lmhit on 16/7/20.
 */

//TODO  just  completed basic method ,  copy all Request here when replaced all
public class BaseRequestGenerator {

    /**
     * requestGetType
     *
     * @param url
     * @param params
     * @return
     */
    public static Request requestGetTypeWithParams(String url, List<FormPair> params) {
        return requestGetTypeWithParams(url, params, null);
    }

    public static Request requestGetTypeWithHeader(String url, List<Header> headers) {
        return requestGetTypeWithParams(url, null, headers);
    }

    public static Request requestGetTypeWithParams(String url, List<FormPair> params, List<Header> header) {
        RequestBuilder builder = new RequestBuilder(MethodType.GET);
        builder.setUrl(url);
        if (HttpConstants.isNonEmpty(params)) {
            builder.setQueryParams(params);
        }

        if (HttpConstants.isNonEmpty(header)) {
            builder.setHeaders(header);
        }

        return builder.build();
    }

    /**
     * @Name requestPostType
     * @Description 注：理论上Params不应为空，params为空情况应通知接口提供相应同事修改接口
     * <p>
     * params为空时传参数 example：
     * {@link RequestDefine#postRequestSyncInfo(int, String, String, String, String, String, String)}
     * <p>
     * code:
     * List<FormPair> params = new ArrayList<FormPair>();
     * params.add(new BasicNameValuePair("null", "null"));
     * @Params [url, params]
     * @Return com.netease.nr.base.request.core.Request
     */
    public static Request requestPostType(String url, @NonNull List<FormPair> params) {
        return requestPostType(url, params, null, null, null);
    }

    public static Request requestPostTypeWithEntity(String url, String entity) {
        return requestPostType(url, null, null, null, entity);
    }

    public static Request requestPostTypeWithoutPart(String url, List<FormPair> params, List<Header> headers) {
        return requestPostType(url, params, headers, null, null);
    }

    public static Request requestPostTypeWithoutHeader(String url, List<FormPair> params, List<Part> parts) {
        return requestPostType(url, params, null, parts, null);
    }

    public static Request requestPostType(String url, List<FormPair> params, List<Header> header, List<Part> parts, String entity) {
        RequestBuilder builder = new RequestBuilder(MethodType.POST);
        builder.setUrl(url);
        if (!TextUtils.isEmpty(entity)) {
            builder.setStringEntity(entity);
        }

        if (HttpConstants.isNonEmpty(params)) {
            builder.setQueryParams(params);
        }

        if (HttpConstants.isNonEmpty(header)) {
            builder.setHeaders(header);
        }

        if (HttpConstants.isNonEmpty(parts)) {
            builder.setBodyParts(parts);
        }

        return builder.build();
    }

    /**
     * requestHeadType
     *
     * @param url
     * @param params
     * @return
     */
    public static Request requestHeadType(String url, List<FormPair> params) {
        return requestHeadType(url, params, null);
    }

    public static Request requestHeadType(String url, List<FormPair> params, List<Header> header) {
        RequestBuilder builder = new RequestBuilder(MethodType.HEAD);
        builder.setUrl(url);
        if (HttpConstants.isNonEmpty(params)) {
            builder.setQueryParams(params);
        }

        if (HttpConstants.isNonEmpty(header)) {
            builder.setHeaders(header);
        }

        return builder.build();
    }

 /*   public static String getHttpResponse(Request request) {
        if (request == null) {
            return "";
        }

        if (request.getMethod() == MethodType.GET) {
            return VolleyManager.addSyncRequest(new CommonRequest<String>(request, new StringParseNetwork()));
        } else if (request.getMethod() == MethodType.POST) {

            if (!TextUtils.isEmpty(request.getStringEntity())) {
                return VolleyManager.addSyncRequest(new StringEntityRequest<String>(request, new StringParseNetwork()));
            }
            if (HttpConstants.isNonEmpty(request.getBodyParts())) {
                return VolleyManager.addSyncRequest(new MultiEntityRequest<String>(request, new StringParseNetwork()));
            } else {
                return VolleyManager.addSyncRequest(new CommonRequest<String>(request, new StringParseNetwork()));
            }
        }
        return "";
    }*/

}
