package com.newenergy.ui.base.network.request.apachewrapper;

/**
 * Created by lijie on 16/12/28.
 * 封装{@link org.apache.http.util.EncodingUtils}
 */

public class EncodingUtils {

    private EncodingUtils() {

    }

    public static byte[] getBytes(String data, String charset) {
        return org.apache.http.util.EncodingUtils.getBytes(data, charset);
    }

    public static byte[] getAsciiBytes(String data) {
        return org.apache.http.util.EncodingUtils.getAsciiBytes(data);
    }
}
