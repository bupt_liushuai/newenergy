package com.newenergy.ui.base.network.request;

/**
 * Created by heq on 17/7/4.
 */

public interface IRequestListener {
    void onCancel(int requestId);
}
