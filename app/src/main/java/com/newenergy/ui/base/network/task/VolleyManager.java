package com.newenergy.ui.base.network.task;

import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.RequestFuture;
import com.newenergy.common.cache.NRCache;
import com.newenergy.common.utils.NetUtil;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.IResponseListener;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import okhttp3.OkHttpClient;

public class VolleyManager {
    private static final String TAG = "Volley";
    //request打此标记，basefragment,baseactivity ondestroy时不清理
    public static final String REQUEST_TAG_NOT_CANCEL = "request_tag_not_cancel";
    private static final String DISK_CACHE_DIR_NAME = "VolleyCache";
    private static final int DISK_CACHE_SIZE = 10 * 1024 * 1024;
    private static File DISK_CACHE_DIR;
    private static VolleyManager sVolleyManager;
    private static Context mContext;
    private static int sThreadPoolSize;
    private static boolean sIsInited;
    private RequestQueue mRequestQueue;
    private List<WeakReference<Request>> mRequestList = new ArrayList<>();
    private Cache mCache;

    private VolleyManager() {
        DISK_CACHE_DIR = NRCache.getDiskCacheDir(mContext, DISK_CACHE_DIR_NAME);
//        DISK_CACHE_DIR = new File(mContext.getCacheDir(), DISK_CACHE_DIR_NAME);
        mRequestQueue = newRequestQueue(newHttpStack());
        mRequestQueue.start();
    }

    private static VolleyManager getInstance() {
        if (!sIsInited) {
            throw new RuntimeException("must init the VolleyManager before use...");
        }
        if (sVolleyManager == null) {
            synchronized (VolleyManager.class) {
                if (sVolleyManager == null) {
                    sVolleyManager = new VolleyManager();
                }
            }
        }
        return sVolleyManager;
    }

    public static void init(Context context, int threadPoolSize) {
        if (sIsInited) {
            return;
        }
        mContext = context;
        sThreadPoolSize = threadPoolSize;
        sIsInited = true;
    }

    //尽量使用basefragment baseactivity中封装的sendRequest方法，以便统一cancel
    public static boolean addRequest(Request request) {
        if (request == null) {
            return false;
        }
        //request如果不使用cache，并且没网的情况下deliverError
        if (!request.shouldCache() && !NetUtil.checkNetwork(mContext)) {
            request.deliverError(new NetworkError());
            return false;
        }
        addRequestInner(request);
        return true;
    }

    private static void addRequestInner(Request request) {
        if (request != null) {
//            try {
//                request.getHeaders().put(HttpUtil.HEADER_ADD_TO_QUEUE, String.valueOf(System.currentTimeMillis()));
//            } catch (AuthFailureError authFailureError) {
//                authFailureError.printStackTrace();
//            }
            synchronized (getInstance().mRequestList) {
                getInstance().mRequestList.add(new WeakReference<Request>(request));
            }
            getInstance().mRequestQueue.add(request);
        }
    }

    public static <T> T addSyncRequest(BaseVolleyRequest<T> request) {
        try {
            return addSyncRequestAndThrowsException(request);
        } catch (Exception e) {
        }
        return null;
    }

    public static <T> T addSyncRequestAndThrowsException(BaseVolleyRequest<T> request) throws ExecutionException, InterruptedException, TimeoutException {
        return addSyncRequestAndThrowsException(request, OkHttpManager.DEFAULT_CONNECT_TIMEOUT_MILLIS);
    }

    public static <T> T addSyncRequestAndThrowsException(BaseVolleyRequest<T> request, long timeoutInMillis) throws ExecutionException, InterruptedException, TimeoutException {
        if (!NetUtil.checkNetwork(mContext)) {
            request.deliverError(new NetworkError());
            return null;
        }
        final RequestFuture<T> requestFuture = RequestFuture.newFuture();
        request.setResponseListener(new IResponseListener<T>() {
            @Override
            public void onResponse(int requestId, T response) {
                requestFuture.onResponse(response);
            }

            @Override
            public void onErrorResponse(int requestId, VolleyError error) {
                requestFuture.onErrorResponse(error);
            }
        });
        addRequestInner(request);
        return requestFuture.get(timeoutInMillis, TimeUnit.MILLISECONDS);
    }

    public static void cancelAllByTag(final Object tag) {
        cancelAllByFilter(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return request.getTag() == tag;
            }
        });
    }

    public static void cancelAll() {
        cancelAllByFilter(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                return true;
            }
        });
    }

    public static void cancelAllByFilter(RequestQueue.RequestFilter requestFilter) {
        List<WeakReference<Request>> list = new ArrayList<>(getInstance().mRequestList);
        for (WeakReference<Request> requestWeakReference : list) {
            if (requestWeakReference != null && requestWeakReference.get() != null && requestFilter != null && requestFilter.apply(requestWeakReference.get())) {
                requestWeakReference.get().cancel();
            }
        }
    }

    private HttpStack newHttpStack() {
        OkHttpClient.Builder builder = OkHttpManager.getInstance().getOkHttpClient()
                .newBuilder();
        //.addInterceptor(new CommonHeaderInterceptor())
        //.addInterceptor(new HttpLoggingInterceptor(TAG, HttpLoggingInterceptor.Level.BODY));

        HttpStack stack = new OKHttpStack(builder.build());
        return stack;
    }

    private RequestQueue newRequestQueue(HttpStack stack) {
        Network network = new BasicNetwork(stack);
        mCache = new DiskBasedCache(DISK_CACHE_DIR, DISK_CACHE_SIZE);
        return new RequestQueue(mCache, network, sThreadPoolSize);
    }

    public static Cache getCache() {
        return getInstance().mCache;
    }
}
