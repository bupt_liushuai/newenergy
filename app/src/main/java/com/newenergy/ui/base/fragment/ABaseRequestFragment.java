package com.newenergy.ui.base.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.newenergy.R;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.bean.BaseBean;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.IRequestListener;
import com.newenergy.ui.base.network.request.RequestConstant;
import com.newenergy.ui.base.network.task.LoadManager;
import com.newenergy.ui.login.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 */
public abstract class ABaseRequestFragment<T> extends ABaseFragment implements LoadManager.ILoadLocalAction<T>, LoadManager.ILoadNetAction<T> {

    static final String TAG = "ABaseRequestFragment";
    @Nullable
    @BindView(R.id.empty_view)
    RelativeLayout mEmpty;
    @Nullable
    @BindView(R.id.error_view)
    RelativeLayout mError;
    @Nullable
    @BindView(R.id.progress)
    LinearLayout mProgress;
    Unbinder unbinder;
    private LoadManager<T> mLoadManager;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoadManager = new LoadManager<>(this, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FrameLayout frameLayout = (FrameLayout) inflater.inflate(R.layout.na_fragment_base, container, false);
        if (inflateContentView() > 0) {
            View v = super.onCreateView(inflater, container, savedInstanceState);
            frameLayout.addView(v, 0);
        }
        unbinder = ButterKnife.bind(this, frameLayout);
        return frameLayout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setClick();
    }


    @Override
    public void setContentView(ViewGroup view) {
        super.setContentView(view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null) {
            loadNetData(true);
        }

    }

    public boolean loadNetData(boolean isRefresh) {
        beforeLoadData(true, isRefresh);
        return mLoadManager.loadNet(isRefresh);
    }

    protected void beforeLoadData(boolean isNetLoad, boolean isRefresh) {
        showProgressBar(isRefresh && shouldShowProgressBeforeLoad());
        showEmptyView(false);
        showErrorView(false);
        showContentView(false);
    }

    protected boolean shouldShowProgressBeforeLoad() {
        return true;
    }

    protected void showProgressBar(boolean b) {
        setViewVisiable(mProgress, b ? View.VISIBLE : View.GONE);
    }

    protected void showEmptyView(boolean b) {
        setViewVisiable(mEmpty, b ? View.VISIBLE : View.GONE);
    }

    protected void showErrorView(boolean b) {
        setViewVisiable(mError, b ? View.VISIBLE : View.GONE);
    }

    protected void showContentView(boolean b) {
        if (inflateContentView() > 0) {
            setViewVisiable(getContentView(), b || forceShowContentView() ? View.VISIBLE : View.GONE);
        }
    }

    protected boolean forceShowContentView() {
        return false;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @Override
    public BaseVolleyRequest<T> createRequest(boolean isRefresh) {
        //为net request包装一层，使ondestory时能根据tag取消掉request
        BaseVolleyRequest<T> request = createNetRequest(isRefresh);
        if (request != null) {
            if (request.getTag() == null) {
                request.setTag(this);
            }
            request.setRequestListener(createNetRequestListener(isRefresh));
        }
        return request;
    }

    public IRequestListener createNetRequestListener(boolean isRefresh) {
        return null;
    }

    protected abstract BaseVolleyRequest<T> createNetRequest(boolean isRefresh);

    @Override
    public void onNetResponse(boolean isRefresh, T response) {

        onResponse(true, isRefresh, response);

        showProgressBar(false);

        showContentView(true);
    }

    protected void onResponse(boolean isNetResponse, boolean isRefresh, T response) {
        if (!isAdded()) {
            return;
        }
        if (isNetResponse) {
        }

        if (response instanceof BaseBean) {
            BaseBean baseBean = (BaseBean) response;
            if (RequestConstant.RESPONSE_SESSTION_TIMEOUT.equals(baseBean.getStatus())) {//session过期
                LoginActivity.launch((BaseActivity) getActivity());
                getActivity().finish();
            }
        }
    }

    @Override
    public void onError(boolean isRefresh, VolleyError error) {
        showProgressBar(false);
        showErrorView(true);
    }

    void setClick() {
        if (getView() != null) {
            getView().findViewById(R.id.empty_icon).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadNetData(true);
                }
            });

            getView().findViewById(R.id.empty_icon).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadNetData(true);
                }
            });
        }
    }
}
