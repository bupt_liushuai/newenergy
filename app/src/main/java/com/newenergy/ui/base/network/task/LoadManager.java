package com.newenergy.ui.base.network.task;

import android.support.annotation.NonNull;

import com.android.volley.VolleyError;
import com.newenergy.common.threadpool.NRTask;
import com.newenergy.common.threadpool.NRThreadPool;
import com.newenergy.common.threadpool.base.Task;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.IResponseListener;

/**
 * Created by heq on 16/5/17.
 */
public class LoadManager<T> {
    private ILoadLocalAction<T> mLoadLocalAction;
    private ILoadNetAction<T> mLoadNetAction;
    private LoadLocalTask mLoadLocalAsyncTask;

    public LoadManager(@NonNull ILoadLocalAction<T> loadLocalAction, ILoadNetAction<T> loadNetAction) {
        mLoadLocalAction = loadLocalAction;
        mLoadNetAction = loadNetAction;
    }


    public boolean loadNet(final boolean isRefresh) {
        BaseVolleyRequest<T> request = mLoadNetAction.createRequest(isRefresh);
        if (request == null) {
            return false;
        }
        request.setResponseListener(new IResponseListener<T>() {
            @Override
            public void onResponse(int requestId, T response) {
                if (mLoadNetAction != null && mLoadNetAction.isUIAdded()) {
                    mLoadNetAction.onNetResponse(isRefresh, response);
                }
            }

            @Override
            public void onErrorResponse(int requestId, VolleyError error) {
                if (mLoadNetAction != null && mLoadNetAction.isUIAdded()) {
                    mLoadNetAction.onError(isRefresh, error);
                }
            }
        });
        return VolleyManager.addRequest(request);
    }

    public void loadLocal(ILoadListener<T> listener) {
        cancelLoadLocalTask();
        mLoadLocalAsyncTask = new LoadLocalTask(mLoadLocalAction, listener);
        NRThreadPool.runNRTask(mLoadLocalAsyncTask, Task.Priority.VENTI);
    }

    public void cancelLoadLocalTask() {
        if (mLoadLocalAsyncTask != null) {
            mLoadLocalAsyncTask.cancel();
            mLoadLocalAsyncTask.release();
        }
    }

    public void onDestroy() {
        cancelLoadLocalTask();
    }

    public interface ILoadListener<T> {
        void afterLoadFinished(T data);
    }

    public interface ILoadLocalAction<T> extends IUIListener {
        T loadLocal();

        void onLocalResponse(T response);
    }


    public interface ILoadNetAction<T> extends IUIListener {

        BaseVolleyRequest<T> createRequest(boolean isRefresh);

        void onNetResponse(boolean isRefresh, T response);

        void onError(boolean isRefresh, VolleyError error);
    }

    public interface IUIListener {
        boolean isUIAdded();
    }

    private static class LoadLocalTask<T> extends NRTask<T> {
        private ILoadLocalAction<T> mLoadLocalAction;
        private ILoadListener<T> mLoadLocalListener;

        /**
         * 注意及时asynctask release方法
         *
         * @param loadLocalAction
         * @param loadLocalListener
         */
        public LoadLocalTask(ILoadLocalAction<T> loadLocalAction, ILoadListener<T> loadLocalListener) {
            mLoadLocalAction = loadLocalAction;
            mLoadLocalListener = loadLocalListener;
        }

        @Override
        protected T doInBackground() {
            if (mLoadLocalAction != null) {
                return mLoadLocalAction.loadLocal();
            }
            return null;
        }

        @Override
        protected void onPostExecute(T data) {
            super.onPostExecute(data);
            if (mLoadLocalAction != null && mLoadLocalAction.isUIAdded()) {
                mLoadLocalAction.onLocalResponse(data);
                if (mLoadLocalListener != null) {
                    mLoadLocalListener.afterLoadFinished(data);
                }
            }
        }

        @Override
        protected void onCancelled() {
            release();
            super.onCancelled();
        }

        public void release() {
            mLoadLocalAction = null;
            mLoadLocalListener = null;
        }
    }
}
