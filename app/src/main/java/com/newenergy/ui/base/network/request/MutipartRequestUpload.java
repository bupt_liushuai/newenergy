package com.newenergy.ui.base.network.request;

import com.android.internal.http.multipart.FilePart;
import com.android.internal.http.multipart.Part;
import com.android.volley.AuthFailureError;
import com.newenergy.ui.base.network.request.apachewrapper.EncodingUtils;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Created by ls on 17/9/24.
 */

public class MutipartRequestUpload<T> extends BaseVolleyRequest<T> {
    private List<File> mFileParts;
    private String BOUNDARY = "---------8888888888888"; //数据分隔线
    private Part[] mParts;

    public MutipartRequestUpload(String url, List<File> fileParts, IParseNetwork<T> iParseNetwork) {
        super(Method.POST, url);
        mFileParts = fileParts;
        initEntity();
        parseNetwork(iParseNetwork);
    }

    private void initEntity() {
        mParts = new Part[mFileParts.size()];

        for (int i = 0; i < mFileParts.size(); i++) {
            try {
                mParts[i] = new FilePart("file", mFileParts.get(i));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        if (mParts == null || mParts.length == 0) {
            return null;
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            Part.sendParts(baos, mParts, EncodingUtils.getAsciiBytes(BOUNDARY));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }

    @Override
    public String getBodyContentType() {
        StringBuffer buffer = new StringBuffer("multipart/form-data");
        buffer.append("; boundary=");
        buffer.append(BOUNDARY);
        return buffer.toString();
    }
}
