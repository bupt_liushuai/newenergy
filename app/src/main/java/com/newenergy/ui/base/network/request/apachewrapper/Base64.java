package com.newenergy.ui.base.network.request.apachewrapper;

/**
 * Created by lijie on 16/12/28.
 * 封装{@link org.apache.commons.codec.binary.Base64}
 */

public class Base64 {

    private org.apache.commons.codec.binary.Base64 value;

    public Base64() {
        value = new org.apache.commons.codec.binary.Base64();
    }

    public static byte[] encodeBase64(byte[] binaryData) {
        return org.apache.commons.codec.binary.Base64.encodeBase64(binaryData);
    }

}
