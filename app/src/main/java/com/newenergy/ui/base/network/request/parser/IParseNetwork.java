package com.newenergy.ui.base.network.request.parser;

/**
 * Created by heq on 16/9/6.
 */
public interface IParseNetwork<T> {
    T parseNetworkResponse(String jsonStr);
}
