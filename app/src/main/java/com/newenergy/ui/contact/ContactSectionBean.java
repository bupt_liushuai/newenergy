package com.newenergy.ui.contact;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * Created by ls on 17/9/18.
 */

public class ContactSectionBean extends SectionEntity<ContactBean.GroupContactBean.ContactsBean> {
    private String count;

    public ContactSectionBean(String header, String count) {
        super(true, header);
        this.count = count;
    }

    public ContactSectionBean(ContactBean.GroupContactBean.ContactsBean contactsBean) {
        super(contactsBean);
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
