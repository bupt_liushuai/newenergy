package com.newenergy.ui.contact;

import com.newenergy.ui.base.bean.BaseBean;

import java.util.List;

/**
 * Created by ls on 17/9/18.
 */

public class ContactBean extends BaseBean {
    private List<GroupContactBean> group_contact;

    public List<GroupContactBean> getGroup_contact() {
        return group_contact;
    }

    public void setGroup_contact(List<GroupContactBean> group_contact) {
        this.group_contact = group_contact;
    }

    public static class GroupContactBean {
        /**
         * group_name : 维护
         * group_id : 1
         * contacts : [{"id":1,"phone":"15588885555","name":"李四"},{"id":2,"phone":"15230302323","name":"张三"},{"id":4,"phone":"321","name":"123"},{"id":6,"phone":"1","name":"1"},{"id":7,"phone":"2","name":"2"},{"id":8,"phone":"3","name":"3"},{"id":9,"phone":"4","name":"4"},{"id":10,"phone":"5","name":"5"},{"id":11,"phone":"6","name":"6"},{"id":12,"phone":"7","name":"7"},{"id":13,"phone":"9","name":"9"}]
         */

        private String group_name;
        private int group_id;
        private List<ContactsBean> contacts;

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public int getGroup_id() {
            return group_id;
        }

        public void setGroup_id(int group_id) {
            this.group_id = group_id;
        }

        public List<ContactsBean> getContacts() {
            return contacts;
        }

        public void setContacts(List<ContactsBean> contacts) {
            this.contacts = contacts;
        }

        public static class ContactsBean {
            /**
             * id : 1
             * phone : 15588885555
             * name : 李四
             */

            private int id;
            private String phone;
            private String name;
            private boolean hasDivider;

            public boolean isHasDivider() {
                return hasDivider;
            }

            public void setHasDivider(boolean hasDivider) {
                this.hasDivider = hasDivider;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }




    /*private List<GroupUsersBean> group_users;

    public List<GroupUsersBean> getGroup_users() {
        return group_users;
    }

    public void setGroup_users(List<GroupUsersBean> group_users) {
        this.group_users = group_users;
    }

    public static class GroupUsersBean {
        *//**
         * group_name : 管理员
         * users : [{"user_name":"张三","user_id":1},{"user_name":"李三","user_id":3},{"user_name":"王柳","user_id":7},{"user_name":"钱八","user_id":9}]
         * group_id : 1
         *//*

        private String group_name;
        private int group_id;
        private List<UsersBean> users;

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public int getGroup_id() {
            return group_id;
        }

        public void setGroup_id(int group_id) {
            this.group_id = group_id;
        }

        public List<UsersBean> getUsers() {
            return users;
        }

        public void setUsers(List<UsersBean> users) {
            this.users = users;
        }

        public static class UsersBean {
            *//**
             * user_name : 张三
             * user_id : 1
             *//*

            private String user_name;
            private int user_id;
            private boolean hasDivider;

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public boolean isHasDivider() {
                return hasDivider;
            }

            public void setHasDivider(boolean hasDivider) {
                this.hasDivider = hasDivider;
            }
        }
    }*/
}
