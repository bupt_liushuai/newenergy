package com.newenergy.ui.group;

import com.chad.library.adapter.base.entity.MultiItemEntity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tongwenwen on 17/9/18.
 */

public class GroupUserBean {

    /**
     * status : 1
     * group_users : [{"group_name":"管理员","users":[{"user_name":"李三","user_id":3},{"user_name":"王柳","user_id":7},{"user_name":"钱八","user_id":9}],"group_id":1},{"group_name":"员工","users":[{"user_name":"李五","user_id":6},{"user_name":"赵七","user_id":8}],"group_id":3},{"group_name":"aaa","users":[{"user_name":"李四","user_id":5}],"group_id":4},{"group_name":"bbb","users":[],"group_id":5},{"group_name":"ddd","users":[],"group_id":6},{"group_name":"fff","users":[],"group_id":7},{"group_name":"iii","users":[],"group_id":8},{"group_name":"yyy","users":[{"user_name":"张三","user_id":1}],"group_id":9},{"group_name":"aaa","users":[],"group_id":10},{"group_name":"eee","users":[],"group_id":11},{"group_name":"111","users":[],"group_id":12}]
     */

    private List<GroupUsersBean> group_users;

    public List<GroupUsersBean> getGroup_users() {
        return group_users;
    }

    public void setGroup_users(List<GroupUsersBean> group_users) {
        this.group_users = group_users;
    }

    public static class GroupUsersBean {
        /**
         * group_name : 管理员
         * users : [{"user_name":"李三","user_id":3},{"user_name":"王柳","user_id":7},{"user_name":"钱八","user_id":9}]
         * group_id : 1
         */

        private String group_name;
        private int group_id;
        private List<UsersBean> users;

        public String getGroup_name() {
            return group_name;
        }

        public void setGroup_name(String group_name) {
            this.group_name = group_name;
        }

        public int getGroup_id() {
            return group_id;
        }

        public void setGroup_id(int group_id) {
            this.group_id = group_id;
        }

        public List<UsersBean> getUsers() {
            return users;
        }

        public void setUsers(List<UsersBean> users) {
            this.users = users;
        }

        public static class UsersBean implements MultiItemEntity, Serializable {
            /**
             * user_name : 李三
             * user_id : 3
             */

            private String user_name;
            private int user_id;
            private int groupId;

            public int getGroupId() {
                return groupId;
            }

            public void setGroupId(int groupId) {
                this.groupId = groupId;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            @Override
            public int getItemType() {
                return GroupSectionAdapter.TYPE_LEVEL_1;
            }
        }
    }
}
