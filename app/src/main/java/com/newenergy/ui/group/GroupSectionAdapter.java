package com.newenergy.ui.group;

import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.newenergy.R;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ls on 17/9/18.
 */

public class GroupSectionAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> implements BaseQuickAdapter.OnItemChildClickListener {
    public static final int TYPE_LEVEL_0 = 0;
    public static final int TYPE_LEVEL_1 = 1;

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public GroupSectionAdapter(List<MultiItemEntity> data) {
        super(data);
        addItemType(TYPE_LEVEL_0, R.layout.item_group_section_head);
        addItemType(TYPE_LEVEL_1, R.layout.item_group_section_child);
        //setOnItemChildClickListener(this);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final MultiItemEntity item) {

        switch (helper.getItemViewType()) {
            case TYPE_LEVEL_0:
//                expand(helper.getAdapterPosition());
                final GroupSectionBean group = (GroupSectionBean) item;
                helper.setText(R.id.group, group.groupTitle);
                if (mContext.getResources().getString(R.string.group_manager).equals(group.groupTitle)) {
                    helper.setImageResource(R.id.group_icon, R.drawable.ic_group_a);
                } else {
                    helper.setImageResource(R.id.group_icon, R.drawable.ic_group_b);
                }

                if (((GroupSectionBean) item).isExpanded()) {
                    helper.setImageResource(R.id.group_arrow, R.drawable.ic_group_bottom);
                } else {
                    helper.setImageResource(R.id.group_arrow, R.drawable.ic_group_right);
                }

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = helper.getAdapterPosition();
                        if (group.isExpanded()) {
                            collapse(pos);
                        } else {
                            expand(pos);
                        }
                        notifyItemChanged(helper.getLayoutPosition());
                    }
                });
                break;
            case TYPE_LEVEL_1:
                final GroupUserBean.GroupUsersBean.UsersBean usersBean = (GroupUserBean.GroupUsersBean.UsersBean) item;
                helper.setText(R.id.name, usersBean.getUser_name());
                helper.addOnClickListener(R.id.move);
                break;
        }

    }

    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        switch (view.getId()) {
            case R.id.move:
                ArrayList<GroupSectionBean> data = new ArrayList<>();
                for (MultiItemEntity multiItemEntity : mData) {
                    if (multiItemEntity instanceof GroupSectionBean)
                        data.add((GroupSectionBean) multiItemEntity);
                }
                FragmentArgs args = new FragmentArgs();
                args.add("group", data);
                GroupUserBean.GroupUsersBean.UsersBean usersBean = (GroupUserBean.GroupUsersBean.UsersBean) adapter.getItem(position);
                args.add("child", usersBean);
                FragmentContainerActivity.launch((BaseActivity) mContext, GroupMoveFragment.class, args);
                break;
        }
    }
}
