package com.newenergy.ui.group;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;

/**
 * Created by ls on 17/9/18.
 */

public class GroupManagerFragment extends ABaseRequestFragment<GroupUserBean> {


    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private List<MultiItemEntity> mSections = new ArrayList<>();
    private GroupSectionAdapter mAdapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle(getResources().getString(R.string.title_group_manager));
    }

    @Override
    public GroupUserBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(GroupUserBean response) {

    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new GroupSectionAdapter(mSections);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.expandAll();

        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.move:
                        FragmentArgs args = new FragmentArgs();
                        GroupUserBean.GroupUsersBean.UsersBean userBean = (GroupUserBean.GroupUsersBean.UsersBean) adapter.getItem(position);
                        args.add("child", userBean);
                        ArrayList<GroupSectionBean> data = new ArrayList<>();
                        for (Object multiItemEntity : adapter.getData()) {
                            if (multiItemEntity instanceof GroupSectionBean) {
                                GroupSectionBean b = (GroupSectionBean) multiItemEntity;
                                data.add(b);
                            }
                        }
                        args.add("group", data);
                        FragmentContainerActivity.launchForResult(GroupManagerFragment.this, GroupMoveFragment.class, args, 101);
                        break;
                }
            }
        });
    }

    @Override
    protected BaseVolleyRequest<GroupUserBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getGroupUser();
        return new CommonRequest<>(r, new IParseNetwork<GroupUserBean>() {
            @Override
            public GroupUserBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, GroupUserBean.class);
            }
        });
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, GroupUserBean response) {
        super.onResponse(isNetResponse, isRefresh, response);
        if (response != null && response.getGroup_users() != null) {
            List<GroupUserBean.GroupUsersBean> groupUsers = response.getGroup_users(); //总数据
            if (groupUsers != null && !groupUsers.isEmpty()) {
                mSections.clear();
                for (GroupUserBean.GroupUsersBean groupUser : groupUsers) {
                    GroupSectionBean groupSectionBean = new GroupSectionBean(groupUser.getGroup_name(), groupUser.getGroup_id());
                    for (GroupUserBean.GroupUsersBean.UsersBean usersBean : groupUser.getUsers()) {
                        usersBean.setGroupId(groupSectionBean.groupId);//给用户提前设置分组，供后面移动分组使用
                        groupSectionBean.addSubItem(usersBean);
                    }
                    mSections.add(groupSectionBean);
                }
                if (mAdapter != null) {
                    mAdapter.setNewData(mSections);
                    mAdapter.expandAll();
                }
            }

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
////            Toast.makeText(getActivity(), "回调了", Toast.LENGTH_SHORT).show();
//            GroupUserBean.GroupUsersBean.UsersBean user = (GroupUserBean.GroupUsersBean.UsersBean) data.getSerializableExtra("child");
//            if (user != null) {
//                Iterator<MultiItemEntity> i = mSections.iterator();
//                while (i.hasNext()) {
//                    MultiItemEntity entity = i.next();
//                    if (entity instanceof GroupUserBean.GroupUsersBean.UsersBean) {
//                        GroupUserBean.GroupUsersBean.UsersBean u = (GroupUserBean.GroupUsersBean.UsersBean) entity;
//                        if (u.getUser_id() == user.getUser_id()) {
//                            i.remove();
//                            break;
//                        }
//                    }
//                }
//
//                int position = -1;
//                for (int j = 0; j < mSections.size(); j++) {
//                    MultiItemEntity section = mSections.get(j);
//                    if (section instanceof GroupSectionBean) {
//                        if (user.getGroupId() == ((GroupSectionBean) section).groupId) {
//                            position = j;
//                            continue;
//                        }
//                    }
//
//                    if (position != -1) {
//                        if (section instanceof GroupSectionBean || position == mSections.size() - 1) {
//                            mSections.add(j, user);
//                            break;
//                        }
//                    }
//                }
//
//                if (mAdapter != null) {
//                    mAdapter.setNewData(mSections);
//                }
//            }
//
            loadNetData(true);
        }

    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_group_manager;
    }
}
