package com.newenergy.ui.web;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.newenergy.R;
import com.newenergy.common.utils.PrHelper;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.fragment.ABaseFragment;

import butterknife.BindView;

/**
 * Created by liushuai on 2017/7/13.
 */

public class WebFragment extends ABaseFragment {
    public final static String PARAM_URL = "param_url";
    public final static String PARAM_TITLE = "param_title";

    private String mUrl;
    private String mTitle;

    @BindView(R.id.webview)
    WebView mWebview;
    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    public static void Launch(Activity activity, String url, String title) {
        FragmentArgs args = new FragmentArgs();
        args.add(PARAM_URL, url);
        if (!TextUtils.isEmpty(title)) {
            args.add(PARAM_TITLE, title);
        }
        FragmentContainerActivity.launch(activity, WebFragment.class, args);
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_web;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            mUrl = getArguments().getString(PARAM_URL);
            mTitle = getArguments().getString(PARAM_TITLE);
            synCookie();

            loadUrl();
        }
        if (!TextUtils.isEmpty(mTitle)) {
            setActionBarTitle(mTitle);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initActionBar();
        configWebview();
        configClient();
    }

    private void initActionBar() {
        BaseActivity activity = (BaseActivity) getActivity();
        ActionBar ab = activity.getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void loadUrl() {
        if (!TextUtils.isEmpty(mUrl) && mWebview != null) {
            mWebview.loadUrl(mUrl);
        }
    }

    private void configWebview() {
        WebSettings settings = mWebview.getSettings();
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setSupportZoom(true);
        settings.setSavePassword(true);
        settings.setSaveFormData(true);
        settings.setJavaScriptEnabled(true);
        mWebview.getSettings().setJavaScriptEnabled(true);
    }

    private void configClient() {
        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return WebFragment.this.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mProgressBar.setVisibility(View.GONE);
            }
        });

        mWebview.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress == 100) {
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.setProgress(newProgress);
                }
            }
        });
    }

    private boolean shouldOverrideUrlLoading(WebView view, String url) {
        return false;
    }


    @Override
    public boolean onBackClick() {
        if (mWebview.canGoBack()) {
            mWebview.goBack();
            return true;
        }
        return super.onBackClick();
    }

    @Override
    public void onDestroyView() {
        if (mWebview != null) {
            ViewGroup parent = (ViewGroup) mWebview.getParent();
            if (parent != null) {
                parent.removeView(mWebview);
            }
            mWebview.stopLoading();
            mWebview.clearView();
            mWebview.clearHistory();
            mWebview.removeAllViews();
            mWebview.destroyDrawingCache();
            try {
                mWebview.destroy();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        super.onDestroyView();
    }

    private void synCookie() {
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setCookie(mUrl, PrHelper.getPrSession());
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieSyncManager.getInstance().sync();
        }else {
            cookieManager.flush();
        }
    }
}
