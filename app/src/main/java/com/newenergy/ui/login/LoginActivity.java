package com.newenergy.ui.login;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.newenergy.MainActivity;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.common.utils.PrHelper;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.RequestConstant;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.base.network.task.VolleyManager;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ls on 17/9/3.
 */

public class LoginActivity extends BaseActivity {

    private static final int ZXING_CAMERA_PERMISSION = 1;

    @BindView(R.id.login_username)
    EditText loginUsername;
    @BindView(R.id.login_password)
    EditText loginPassword;
    @BindView(R.id.see)
    ImageView see;
    @BindView(R.id.cancel)
    ImageView cancel;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.logo)
    ImageView logo;

    private MaterialDialog mDialog;


    public static void launch(BaseActivity from) {
        Intent intent = new Intent(from, LoginActivity.class);
        from.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initView();
    }


    private void initView() {
        see.setImageResource(R.drawable.login_pass_selector);
        loginUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setLoginBtnState();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        loginUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (logo.getVisibility() == View.VISIBLE) {
                    transfromIcon();
                }
            }
        });

        loginPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (logo.getVisibility() == View.VISIBLE) {
                    transfromIcon();
                }
            }
        });


        loginPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setLoginBtnState();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mDialog = new MaterialDialog.Builder(this)
                .content("登录")
                .content("正在登录...")
                .progress(true, 0)
                .build();

        loginUsername.setText("admin");
        loginPassword.setText("222");
    }
    AnimatorSet mAnimatorSet;
    private void transfromIcon() {
        ObjectAnimator animator1 = ObjectAnimator.ofFloat(getRootView(), "translationY", 0f, -300f);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(logo, "alpha", 1f, 0f);

        mAnimatorSet = new AnimatorSet();
        mAnimatorSet.play(animator1).with(animator2);
        mAnimatorSet.setDuration(300);
        mAnimatorSet.start();

        mAnimatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (loginUsername != null) {
                    loginUsername.setClickable(false);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (logo != null) {
                    logo.setVisibility(View.INVISIBLE);
                }
                if (loginUsername != null) {
                    loginUsername.setClickable(true);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    @Override
    public void onDestroy() {
        if (mAnimatorSet != null) {
            mAnimatorSet.cancel();
            mAnimatorSet = null;
        }
        super.onDestroy();
    }

    private void setLoginBtnState() {
        if (TextUtils.isEmpty(loginUsername.getText().toString()) ||
                TextUtils.isEmpty(loginPassword.getText().toString())) {
            loginBtn.setClickable(false);
        }else {
            loginBtn.setClickable(true);
        }
    }

    @OnClick({R.id.see, R.id.cancel, R.id.login_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.see:
                see();
                break;
            case R.id.cancel:
                loginPassword.setText("");
                break;
            case R.id.login_btn:
                login();
                break;
        }
    }

    private void see() {
        if (loginPassword.getTransformationMethod() == PasswordTransformationMethod.getInstance()) {
            //如果隐藏，显示密码
            loginPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            loginPassword.setSelection(loginPassword.getText().length());
            see.setImageLevel(2);
        } else {
            //否则隐藏密码
            loginPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            loginPassword.setSelection(loginPassword.getText().length());
            see.setImageLevel(1);
        }
    }

    private void login() {
        String account = loginUsername.getText().toString();
        String password = loginPassword.getText().toString();
        if (TextUtils.isEmpty(account) || TextUtils.isEmpty(password)) {
            Toast.makeText(this, "用户名或密码不能为空", Toast.LENGTH_SHORT).show();
            return;
        }
        if (mDialog != null) {
            mDialog.show();
        }

        Request r = RequestDefine.loginRequest(account, password);
        CommonRequest<LoginBean> request = new CommonRequest<LoginBean>(r, new IParseNetwork<LoginBean>() {
            @Override
            public LoginBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, LoginBean.class);
            }
        }) {
            @Override
            protected Response<LoginBean> parseNetworkResponse(NetworkResponse response) {
                Response<LoginBean> res = super.parseNetworkResponse(response);
                Cache.Entry entry = res.cacheEntry;
                Map<String, String> headerrs = entry.responseHeaders;
                String sessionId = headerrs.get("Set-Cookie");
                PrHelper.setPrSession(sessionId);
                return res;
            }

            @Override
            protected Map<String, String> getHeaderMap() {
                return new HashMap<>();
            }
        };

        request.setResponseListener(new IResponseListener<LoginBean>() {
            @Override
            public void onResponse(int requestId, LoginBean response) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                if (response != null) {
                    if (RequestConstant.RESPONSE_OK.equals(response.getStatus())) {
                        Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    }else {
                        if (!TextUtils.isEmpty(response.getMsg())) {
                            Toast.makeText(LoginActivity.this, response.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }

            @Override
            public void onErrorResponse(int requestId, VolleyError error) {
                if (mDialog != null) {
                    mDialog.dismiss();
                }
                Toast.makeText(LoginActivity.this, "登录失败", Toast.LENGTH_SHORT).show();
            }
        });

        VolleyManager.addRequest(request);

    }


}
