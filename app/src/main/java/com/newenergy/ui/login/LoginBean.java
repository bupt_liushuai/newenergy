package com.newenergy.ui.login;

import com.newenergy.ui.base.bean.BaseBean;

/**
 * Created by liushuai on 2017/9/29.
 */

public class LoginBean extends BaseBean{

    /**
     * status : 1
     * user : {"id":1,"account":"admin","password":null,"passwordencode":"","username":"张三","userposttype_id":1,"userposttype":"经理","departtype_id":1,"departtype":"部门1","role_id":5,"createtime":1503629081000,"isuse":null,"headimgpath":"0ac4973db4134a448ed23c3f7fe9bfc6.png"}
     */

    private UserBean user;

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public static class UserBean {
        /**
         * id : 1
         * account : admin
         * password : null
         * passwordencode :
         * username : 张三
         * userposttype_id : 1
         * userposttype : 经理
         * departtype_id : 1
         * departtype : 部门1
         * role_id : 5
         * createtime : 1503629081000
         * isuse : null
         * headimgpath : 0ac4973db4134a448ed23c3f7fe9bfc6.png
         */

        private int id;
        private String account;
        private Object password;
        private String passwordencode;
        private String username;
        private int userposttype_id;
        private String userposttype;
        private int departtype_id;
        private String departtype;
        private int role_id;
        private long createtime;
        private Object isuse;
        private String headimgpath;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public Object getPassword() {
            return password;
        }

        public void setPassword(Object password) {
            this.password = password;
        }

        public String getPasswordencode() {
            return passwordencode;
        }

        public void setPasswordencode(String passwordencode) {
            this.passwordencode = passwordencode;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public int getUserposttype_id() {
            return userposttype_id;
        }

        public void setUserposttype_id(int userposttype_id) {
            this.userposttype_id = userposttype_id;
        }

        public String getUserposttype() {
            return userposttype;
        }

        public void setUserposttype(String userposttype) {
            this.userposttype = userposttype;
        }

        public int getDeparttype_id() {
            return departtype_id;
        }

        public void setDeparttype_id(int departtype_id) {
            this.departtype_id = departtype_id;
        }

        public String getDeparttype() {
            return departtype;
        }

        public void setDeparttype(String departtype) {
            this.departtype = departtype;
        }

        public int getRole_id() {
            return role_id;
        }

        public void setRole_id(int role_id) {
            this.role_id = role_id;
        }

        public long getCreatetime() {
            return createtime;
        }

        public void setCreatetime(long createtime) {
            this.createtime = createtime;
        }

        public Object getIsuse() {
            return isuse;
        }

        public void setIsuse(Object isuse) {
            this.isuse = isuse;
        }

        public String getHeadimgpath() {
            return headimgpath;
        }

        public void setHeadimgpath(String headimgpath) {
            this.headimgpath = headimgpath;
        }
    }
}
