package com.newenergy.ui.main;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.newenergy.R;
import com.newenergy.ui.analysis.DataAnalysisFragment;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.contact.ContactListFragment;
import com.newenergy.ui.cost.CostCountFragment;
import com.newenergy.ui.customer.CustomerManagerListFragment;
import com.newenergy.ui.group.GroupManagerFragment;
import com.newenergy.ui.management.ManagerFragment;
import com.newenergy.ui.patrol.DailyPatrolFragment;
import com.newenergy.ui.patrol.PatrolFragment;
import com.newenergy.ui.management.train.TrainListFragment;
import com.newenergy.ui.video.list.VideoListFragment;
import com.newenergy.ui.web.WebFragment;

/**
 * Created by liushuai on 2017/8/29.
 */

public class HomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private static final int ITEM_VIEW_TYPE_HEADER = 0;
    private static final int ITEM_VIEW_TYPE_ITEM = 1;
    private BaseActivity mActivity;
    private Item[] datas = new Item[9];
    private View mHeader;

    public HomeAdapter(BaseActivity activity, View header) {
        mActivity = activity;
        mHeader = header;
        initData();
    }

    private void initData() {
        String[] titles = mActivity.getResources().getStringArray(R.array.home_titles);
        TypedArray imgs = mActivity.getResources().obtainTypedArray(R.array.home_icons);

        for (int i = 0; i < titles.length; i++) {
            datas[i] = new Item(titles[i], imgs.getResourceId(i, 0));
        }
        imgs.recycle();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_HEADER) {
            return new HeaderViewHolder(mHeader);
        }
        View v = LayoutInflater.from(mActivity).inflate(R.layout.item_home, null);
        v.setOnClickListener(this);
        return new ViewHolder(v);
    }

    //将数据绑定到子View，会自动复用View
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        //viewHolder.textView.setText(datas[i]);
        if (isHeader(i)) {
            return;
        }
        if (viewHolder instanceof ViewHolder) {
            Item data = datas[i - 1];
            ViewHolder holder = (ViewHolder) viewHolder;
            holder.icon.setImageResource(data.icon);
            holder.title.setText(data.title);
            viewHolder.itemView.setTag(data.title);

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isHeader(position)) {
            return ITEM_VIEW_TYPE_HEADER;
        }
        return ITEM_VIEW_TYPE_ITEM;
    }

    public boolean isHeader(int position) {
        return position == 0;
    }

    //RecyclerView显示数据条数
    @Override
    public int getItemCount() {
        return datas.length + 1;
    }

    @Override
    public void onClick(View view) {
        String title = (String) view.getTag();
        if (title.equals(datas[0].title)) {

            FragmentArgs args = new FragmentArgs();
            args.add(WebFragment.PARAM_TITLE, datas[0].title);
            args.add(WebFragment.PARAM_URL, RequestUrl.POST_REAL_TIME_H5);
            FragmentContainerActivity.launch(mActivity, WebFragment.class, args);
        } else if (title.equals(datas[1].title)) {
            FragmentContainerActivity.launch(mActivity, VideoListFragment.class, null);

        } else if (title.equals(datas[2].title)) {
            FragmentContainerActivity.launch(mActivity, DataAnalysisFragment.class, null);

        } else if (title.equals(datas[3].title)) {
            FragmentContainerActivity.launch(mActivity, DailyPatrolFragment.class, null);
        } else if (title.equals(datas[4].title)) {
            FragmentContainerActivity.launch(mActivity, ManagerFragment.class, null);
        } else if (title.equals(datas[5].title)) {
            //客户管理
            FragmentContainerActivity.launch(mActivity, CustomerManagerListFragment.class, null);
        } else if (title.equals(datas[6].title)) {
            //成本计量
            FragmentContainerActivity.launch(mActivity, CostCountFragment.class, null);
        } else if (title.equals(datas[7].title)) {
            //群组管理
            FragmentContainerActivity.launch(mActivity, GroupManagerFragment.class, null);
        } else if (title.equals(datas[8].title)) {
            //联系方式
            FragmentContainerActivity.launch(mActivity, ContactListFragment.class, null);
            //FragmentContainerActivity.launch(mActivity, EquipmentFragment.class, null);
        }

    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        //在布局中找到所含有的UI组件
        public HeaderViewHolder(View itemView) {
            super(itemView);
            //textView = (TextView) itemView.findViewById(R.id.icon);
            //imageView = (ImageView) itemView.findViewById(R.id.icon);
        }
    }

    //自定义的ViewHolder,减少findViewById调用次数
    private class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        ImageView icon;

        //在布局中找到所含有的UI组件
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            icon = (ImageView) itemView.findViewById(R.id.icon);
        }
    }

    private static class Item {
        private String title;
        private int icon;

        public Item(String title, int icon) {
            this.title = title;
            this.icon = icon;
        }
    }
}
