package com.newenergy.ui.main;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.newenergy.R;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.base.network.RequestUrl;

import butterknife.BindView;

/**
 * Created by liushuai on 2017/8/29.
 */

public class HomeFragment extends ABaseFragment {
    @BindView(R.id.recylcerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.img)
    ImageView mImg;
    private HomeAdapter mAdapter;

    @Override
    public int inflateContentView() {
        return R.layout.fragment_home;
    }

    @Override
    public void setContentView(ViewGroup view) {
        super.setContentView(view);
        final GridLayoutManager manager = new GridLayoutManager(getActivity(), 3);
        mRecyclerView.setLayoutManager(manager);
        mAdapter = new HomeAdapter((BaseActivity) getActivity(), createHeaderView());
        mRecyclerView.setAdapter(mAdapter);

        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return mAdapter.isHeader(position) ? manager.getSpanCount() : 1;
            }
        });

        Glide.with(getActivity()).load(RequestUrl.MAIN_HEAD_IMG).apply(new RequestOptions().centerCrop()).into(mImg);
    }


    private View createHeaderView() {
        return new View(getActivity());
    }
}
