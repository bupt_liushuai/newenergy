package com.newenergy.ui.pc;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.lzy.imagepicker.view.CropImageView;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.MutipartRequestUpload;
import com.newenergy.ui.base.network.request.RequestConstant;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.base.network.task.VolleyManager;
import com.newenergy.ui.login.LoginActivity;
import com.newenergy.ui.login.LoginBean;
import com.newenergy.ui.patrol.upload.FileUploadBean;
import com.newenergy.ui.patrol.upload.GlideImageLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by liushuai on 2017/8/30.
 */

public class PersonCenterFragment extends ABaseFragment {
    private static final int REQUEST_CODE_SELECT = 100;
    public static final int REQUEST_CODE_PREVIEW = 101;

    private static final int EXTERNAL_STORAGE_PERMISSION = 1;

    @BindView(R.id.profile)
    ImageView mProfile;
    @BindView(R.id.account)
    TextView mAccount;
    @BindView(R.id.name)
    TextView mName;
    @BindView(R.id.org)
    TextView mOrg;

    private MaterialDialog mDialog;

    @Override
    public int inflateContentView() {
        return R.layout.fragment_pc;
    }

    @Override
    public void setContentView(ViewGroup view) {
        super.setContentView(view);


        final CommonRequest<LoginBean> request = new CommonRequest<LoginBean>(RequestDefine.getUserInfo(), new IParseNetwork<LoginBean>() {
            @Override
            public LoginBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, LoginBean.class);
            }
        });

        request.setResponseListener(new IResponseListener<LoginBean>() {
            @Override
            public void onResponse(int requestId, LoginBean response) {
                if (response != null){
                    if (RequestConstant.RESPONSE_OK.equals(response.getStatus())){
                        setProfile(response.getUser());
                    }else if (RequestConstant.RESPONSE_SESSTION_TIMEOUT.equals(response.getStatus())) {
                        LoginActivity.launch((BaseActivity) getActivity());
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onErrorResponse(int requestId, VolleyError error) {

            }
        });

        VolleyManager.addRequest(request);

        mDialog = new MaterialDialog.Builder(getActivity())
                .items(R.array.items)
                .widgetColor(getResources().getColor(android.R.color.transparent))
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View itemView, int position, CharSequence text) {
                        switch (position) {
                            case 0:
                                goToCamera();
                                break;
                            case 1:
                                goToPicSelect();
                                break;
                        }
                    }
                })
                .build();

        mDialog.getBuilder();
        initPicSelect();
    }

    private void initPicSelect() {
        ImagePicker imagePicker = ImagePicker.getInstance();
        imagePicker.setImageLoader(new GlideImageLoader());   //设置图片加载器
        imagePicker.setMultiMode(false);   //单选
        imagePicker.setShowCamera(false);                      //显示拍照按钮
        imagePicker.setCrop(true);                           //允许裁剪（单选才有效）
        imagePicker.setSaveRectangle(true);                   //是否按矩形区域保存
        imagePicker.setSelectLimit(1);              //选中数量限制
        imagePicker.setStyle(CropImageView.Style.CIRCLE);  //裁剪框的形状
        imagePicker.setFocusWidth(800);   //裁剪框的宽度。单位像素（圆形自动取宽高最小值）
        imagePicker.setFocusHeight(800);  //裁剪框的高度。单位像素（圆形自动取宽高最小值）
        imagePicker.setOutPutX(1000);//保存文件的宽度。单位像素
        imagePicker.setOutPutY(1000);//保存文件的高度。单位像素                  //裁剪框的高度。单位像素（圆形自动取宽高最小值）

    }

    private void goToPicSelect() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION);
        } else {

            //打开选择,本次允许选择的数量
//            ImagePicker.getInstance().setSelectLimit(PARAM_MAX_COUNT - mSelImageList.size());
            Intent intent = new Intent(getActivity(), ImageGridActivity.class);
            startActivityForResult(intent, REQUEST_CODE_SELECT);
        }
    }

    private void goToCamera() {
        Intent intent = new Intent(getActivity(), ImageGridActivity.class);
        intent.putExtra(ImageGridActivity.EXTRAS_TAKE_PICKERS, true); // 是否是直接打开相机
        startActivityForResult(intent, REQUEST_CODE_SELECT);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ArrayList<ImageItem> images;
        if (resultCode == ImagePicker.RESULT_CODE_ITEMS) {
            //添加图片返回
            if (data != null && requestCode == REQUEST_CODE_SELECT) {
                images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                if (images != null && images.size() > 0) {

                    final String path = images.get(0).path;
                    List<File> files = new ArrayList<>();
                    files.add(new File(path));

                    final MaterialDialog dia = new MaterialDialog.Builder(getActivity())
                            .progress(true, 0)
                            .content("头像正在上传")
                            .build();

                    MutipartRequestUpload<FileUploadBean.FilepathBean> requestUpload = new MutipartRequestUpload<>(RequestUrl.UPLOAD_HEAD_IMG, files, new IParseNetwork<FileUploadBean.FilepathBean>() {
                        @Override
                        public FileUploadBean.FilepathBean parseNetworkResponse(String jsonStr) {
                            return JsonUtils.fromJson(jsonStr, FileUploadBean.FilepathBean.class);
                        }
                    });
                    requestUpload.setResponseListener(new IResponseListener<FileUploadBean.FilepathBean>() {
                        @Override
                        public void onResponse(int requestId, FileUploadBean.FilepathBean response) {
                            if (dia != null) {
                                dia.dismiss();
                            }
                            if (response != null && RequestConstant.RESPONSE_OK.equals(response.getStatus())) {
                                //
                                Toast.makeText(getActivity(), "头像上传成功", Toast.LENGTH_SHORT).show();
                                Glide.with(getActivity()).load(new File(path))
                                        .apply(new RequestOptions()
                                                .fitCenter()
                                                .circleCrop())
                                        .into(mProfile);
                            } else {
                                Toast.makeText(getActivity(), "头像上传失败，请检测网络", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onErrorResponse(int requestId, VolleyError error) {
                            if (dia != null) {
                                dia.dismiss();
                            }
                            Toast.makeText(getActivity(), "头像上传失败，请检测网络", Toast.LENGTH_SHORT).show();
                        }
                    });
                    sendRequest(requestUpload);
                    dia.show();
                }
            }
        }
    }


    private void setProfile(LoginBean.UserBean userBean) {
        Glide.with(getActivity())
                .load(String.format(RequestUrl.USER_HEAD_IMG, userBean.getHeadimgpath()))
                .apply(new RequestOptions()
                        .fitCenter()
                        .circleCrop()
                        .placeholder(R.drawable.ic_pc)
                )
                .into(mProfile);


        mAccount.setText(userBean.getAccount());
        mName.setText(userBean.getUsername());
        mOrg.setText(userBean.getDeparttype());
    }


    @OnClick({R.id.logout, R.id.header})
    public void onViewClicked(View v) {
        switch (v.getId()) {
            case R.id.logout:
                logout();
                break;
            case R.id.header:
                if (mDialog != null) {
                    mDialog.show();
                }
                break;
        }
    }


    private void logout() {
        LoginActivity.launch((BaseActivity) getActivity());
        getActivity().finish();
    }

}
