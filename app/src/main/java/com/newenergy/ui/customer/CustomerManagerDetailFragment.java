package com.newenergy.ui.customer;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.common.constant.Constant;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.fragment.ABaseRequestListFragment;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.customer.bean.CustomerDetailBean;
import com.newenergy.ui.patrol.PatrolFragment;
import com.newenergy.ui.web.WebFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by liushuai on 2018/3/24.
 */

public class CustomerManagerDetailFragment extends ABaseRequestFragment<CustomerDetailBean> {

    public final static String PARAM_ID = "param_id";
    private String mId;

    @BindView(R.id.ll_container1)
    LinearLayout mLinearLayout;
    @BindView(R.id.tv_sign)
    TextView mSing;
    @BindView(R.id.tc_name)
    TextView mName;
    @BindView(R.id.ll_payList)
    LinearLayout mPayListContainer;
    @BindView(R.id.ll_visitList)
    LinearLayout mVisitListContainer;
    @BindView(R.id.rl_contfileContainer)
    RelativeLayout mContFileContainer;
    private LayoutInflater mLayoutInflater;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = String.valueOf(getArguments().getInt(PARAM_ID));
        }
        mLayoutInflater = LayoutInflater.from(getContext());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle("");
    }

    @Override
    public CustomerDetailBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(CustomerDetailBean response) {

    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }

    @Override
    protected BaseVolleyRequest<CustomerDetailBean> createNetRequest(boolean isRefresh) {

        return new CommonRequest<>(RequestDefine.getCustomersInfo(mId), new IParseNetwork<CustomerDetailBean>() {
            @Override
            public CustomerDetailBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, CustomerDetailBean.class);
            }
        });
    }


    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, CustomerDetailBean response) {
        super.onResponse(isNetResponse, isRefresh, response);
        if (response != null) {
            if (response != null && response.getCustomerinfo() != null) {
                CustomerDetailBean.CustomerinfoBean bean = response.getCustomerinfo();

                initCustomerView(mLinearLayout.getChildAt(0), "开发商:", bean.getDevelopers());
                initCustomerView(mLinearLayout.getChildAt(1), "物业公司:", bean.getPropertycompany());
                initCustomerView(mLinearLayout.getChildAt(2), "建筑面积:", bean.getBuiltarea());
                initCustomerView(mLinearLayout.getChildAt(3), "供冷面积:", bean.getCoolingarea());
                initCustomerView(mLinearLayout.getChildAt(4), "业主联系人:", bean.getOwnercontact());
                initCustomerView(mLinearLayout.getChildAt(5), "业主电话:", bean.getOwnertel());
                initCustomerView(mLinearLayout.getChildAt(6), "物业联系人:", bean.getPropertycontact());
                initCustomerView(mLinearLayout.getChildAt(7), "物业电话:", bean.getPropertytel());
                initCustomerView(mLinearLayout.getChildAt(8), "合同状态:", bean.getContractstatus() == 0 ? "未签" : "已签");
                mSing.setText(bean.getContractstatus() == 0 ? "未签" : "已签");
                mName.setText(bean.getName());
            }

            //合同
            if (response.getContfilelist() != null && !response.getContfilelist().isEmpty()) {
                mContFileContainer.setVisibility(View.VISIBLE);

                List<CustomerDetailBean.ContfilelistBean> contfilelist = response.getContfilelist();
                for (int i = 0; i < contfilelist.size(); i++) {
                    CustomerDetailBean.ContfilelistBean bean = contfilelist.get(i);
                    initContFileList(bean, i);
                }
            } else {
                mContFileContainer.setVisibility(View.GONE);
            }
            //支付列表
            if (response.getPaylist() != null && !response.getPaylist().isEmpty()) {
                mPayListContainer.setVisibility(View.VISIBLE);
                List<CustomerDetailBean.PayListBean> payListBeans = response.getPaylist();
                for (CustomerDetailBean.PayListBean payListBean : payListBeans) {
                    initPayList(payListBean);
                }

            } else {
                mPayListContainer.setVisibility(View.GONE);
            }

            //回访记录
            if (response.getVisitlist() != null && !response.getVisitlist().isEmpty()) {
                mVisitListContainer.setVisibility(View.VISIBLE);
                List<CustomerDetailBean.VisitlistBean> visitlist = response.getVisitlist();
                for (CustomerDetailBean.VisitlistBean visitlistBean : visitlist) {
                    initVisitList(visitlistBean);
                }

            } else {
                mVisitListContainer.setVisibility(View.GONE);
            }


        }
    }

    /**
     * 合同
     *
     * @param contfilelistBean
     */
    private void initContFileList(final CustomerDetailBean.ContfilelistBean contfilelistBean, int i) {
        View v = mLayoutInflater.inflate(R.layout.fragment_customer_detail_contfile_item, null);
        ((TextView) v.findViewById(R.id.name)).setText("与客户签订的合同" + i);
        v.findViewById(R.id.tv_preview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = String.format(RequestUrl.VIEW_RPT, Constant.KEGL_HT, contfilelistBean.getId(), "");
                WebFragment.Launch(getActivity(), url, "");

            }
        });
        mContFileContainer.addView(v);
    }

    /**
     * 回放
     *
     * @param visitlistBean
     */
    private void initVisitList(final CustomerDetailBean.VisitlistBean visitlistBean) {
        View v = mLayoutInflater.inflate(R.layout.fragment_customer_detail_visit_item, null);
        ((TextView) v.findViewById(R.id.date)).setText(visitlistBean.getVisittime());
        v.findViewById(R.id.tv_preview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = String.format(RequestUrl.VIEW_RPT, Constant.KEGL_HFJL, visitlistBean.getId(), "");
                FragmentArgs args = new FragmentArgs();
                args.add(WebFragment.PARAM_URL, url);
                FragmentContainerActivity.launch(getActivity(), WebFragment.class, args);

            }
        });
        mVisitListContainer.addView(v);
    }

    /**
     * 支付列表
     *
     * @param paylistBean
     */
    private void initPayList(CustomerDetailBean.PayListBean paylistBean) {
        View v = mLayoutInflater.inflate(R.layout.fragment_customer_detail_pay_item, null);
        ((TextView) v.findViewById(R.id.count)).setText(paylistBean.getActualpay());
        ((TextView) v.findViewById(R.id.date)).setText(paylistBean.getPaydate());
        mPayListContainer.addView(v);
    }

    private void initCustomerView(View v, String name, String desc) {
        ((TextView) v.findViewById(R.id.name)).setText(name);
        ((TextView) v.findViewById(R.id.desc)).setText(desc);
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_customer_detail;
    }
}
