package com.newenergy.ui.customer;

import android.app.Activity;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.customer.bean.CustomerBean;

/**
 * Created by ls on 17/9/12.
 */

public class CustomerManagerListAdapter extends BaseQuickAdapter<CustomerBean.CustomerlistBean, BaseViewHolder> {
    public CustomerManagerListAdapter() {
        super(R.layout.item_customer_list);
    }


    @Override
    protected void convert(BaseViewHolder helper, final CustomerBean.CustomerlistBean item) {
        //helper.setText()
        helper.setText(R.id.tv_customer_name, item.getName());
        helper.setText(R.id.tv_develop, String.format(mContext.getResources().getString(R.string.customer_developer), item.getDevelopers()));
        helper.setText(R.id.tv_company, String.format(mContext.getResources().getString(R.string.customer_company), item.getPropertycompany()));
        helper.setText(R.id.tv_ower, item.getOwnercontact());
        helper.setText(R.id.tv_property_contact, item.getPropertycontact());

        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //详情
                FragmentArgs args = new FragmentArgs();
                args.add(CustomerManagerDetailFragment.PARAM_ID, item.getId());
                FragmentContainerActivity.launch((Activity) mContext, CustomerManagerDetailFragment.class, args);

            }
        });
    }
}
