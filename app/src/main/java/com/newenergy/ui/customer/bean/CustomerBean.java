package com.newenergy.ui.customer.bean;

import com.newenergy.ui.base.bean.BaseListBean;

import java.util.List;

/**
 * Created by liushuai on 2018/3/24.
 */

public class CustomerBean extends BaseListBean {


    /**
     * customerlist : [{"id":5,"contractstatus":1,"name":"03-11","ownercontact":"123","developers":"123","propertycontact":"123","propertycompany":"123"},{"id":6,"contractstatus":1,"name":"03-12","ownercontact":"111","developers":"222","propertycontact":"111","propertycompany":"111"},{"id":7,"contractstatus":1,"name":"03-13","ownercontact":"123","developers":"jkl","propertycontact":"123","propertycompany":"jkl"}]
     * pageinfo : {"totalNumber":3,"currentPage":1,"totalPage":1,"pageNumber":20,"dbIndex":0,"dbNumber":20,"lpage":1,"rpage":1}
     */

    private List<CustomerlistBean> customerlist;

    public List<CustomerlistBean> getCustomerlist() {
        return customerlist;
    }

    public void setCustomerlist(List<CustomerlistBean> customerlist) {
        this.customerlist = customerlist;
    }


    public static class CustomerlistBean {
        /**
         * id : 5
         * contractstatus : 1
         * name : 03-11
         * ownercontact : 123
         * developers : 123
         * propertycontact : 123
         * propertycompany : 123
         */

        private int id;
        private int contractstatus;
        private String name;
        private String ownercontact;
        private String developers;
        private String propertycontact;
        private String propertycompany;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getContractstatus() {
            return contractstatus;
        }

        public void setContractstatus(int contractstatus) {
            this.contractstatus = contractstatus;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOwnercontact() {
            return ownercontact;
        }

        public void setOwnercontact(String ownercontact) {
            this.ownercontact = ownercontact;
        }

        public String getDevelopers() {
            return developers;
        }

        public void setDevelopers(String developers) {
            this.developers = developers;
        }

        public String getPropertycontact() {
            return propertycontact;
        }

        public void setPropertycontact(String propertycontact) {
            this.propertycontact = propertycontact;
        }

        public String getPropertycompany() {
            return propertycompany;
        }

        public void setPropertycompany(String propertycompany) {
            this.propertycompany = propertycompany;
        }
    }
}
