package com.newenergy.ui.customer.bean;

import com.newenergy.ui.base.bean.BaseBean;

import java.util.List;

/**
 * Created by tongwenwen on 2018/3/25.
 */

public class CustomerDetailBean extends BaseBean {

    /**
     * visitlist : [{"id":20,"visittime":1516377600000}]
     * contfilelist : [{"filepath":"361adecec4324a6e8818d3b4508aa8af.pdf","filename":"Python人工智能.pdf"}]
     * status : 1
     * paylist : []
     * customerinfo : {"createtime":1516442015000,"id":5,"propertytel":"123","coolingarea":"123","contractstatus":1,"name":"03-11","ownercontact":"123","builtarea":"123","developers":"123","propertycontact":"123","propertycompany":"123","ownertel":"123"}
     */

    private CustomerinfoBean customerinfo; //当前客户信息
    private List<VisitlistBean> visitlist;//客户回访文件信息
    private List<ContfilelistBean> contfilelist;//客户合同文件信息
    private List<PayListBean> paylist; //付款信息

    public CustomerinfoBean getCustomerinfo() {
        return customerinfo;
    }

    public void setCustomerinfo(CustomerinfoBean customerinfo) {
        this.customerinfo = customerinfo;
    }

    public List<VisitlistBean> getVisitlist() {
        return visitlist;
    }

    public void setVisitlist(List<VisitlistBean> visitlist) {
        this.visitlist = visitlist;
    }

    public List<ContfilelistBean> getContfilelist() {
        return contfilelist;
    }

    public void setContfilelist(List<ContfilelistBean> contfilelist) {
        this.contfilelist = contfilelist;
    }

    public List<PayListBean> getPaylist() {
        return paylist;
    }

    public void setPaylist(List<PayListBean> paylist) {
        this.paylist = paylist;
    }

    public static class CustomerinfoBean {
        /**
         * createtime : 1516442015000
         * id : 5
         * propertytel : 123
         * coolingarea : 123
         * contractstatus : 1
         * name : 03-11
         * ownercontact : 123
         * builtarea : 123
         * developers : 123
         * propertycontact : 123
         * propertycompany : 123
         * ownertel : 123
         */

        private long createtime;
        private int id;
        private String propertytel;
        private String coolingarea;
        private int contractstatus;
        private String name;
        private String ownercontact;
        private String builtarea;
        private String developers;
        private String propertycontact;
        private String propertycompany;
        private String ownertel;

        public long getCreatetime() {
            return createtime;
        }

        public void setCreatetime(long createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPropertytel() {
            return propertytel;
        }

        public void setPropertytel(String propertytel) {
            this.propertytel = propertytel;
        }

        public String getCoolingarea() {
            return coolingarea;
        }

        public void setCoolingarea(String coolingarea) {
            this.coolingarea = coolingarea;
        }

        public int getContractstatus() {
            return contractstatus;
        }

        public void setContractstatus(int contractstatus) {
            this.contractstatus = contractstatus;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOwnercontact() {
            return ownercontact;
        }

        public void setOwnercontact(String ownercontact) {
            this.ownercontact = ownercontact;
        }

        public String getBuiltarea() {
            return builtarea;
        }

        public void setBuiltarea(String builtarea) {
            this.builtarea = builtarea;
        }

        public String getDevelopers() {
            return developers;
        }

        public void setDevelopers(String developers) {
            this.developers = developers;
        }

        public String getPropertycontact() {
            return propertycontact;
        }

        public void setPropertycontact(String propertycontact) {
            this.propertycontact = propertycontact;
        }

        public String getPropertycompany() {
            return propertycompany;
        }

        public void setPropertycompany(String propertycompany) {
            this.propertycompany = propertycompany;
        }

        public String getOwnertel() {
            return ownertel;
        }

        public void setOwnertel(String ownertel) {
            this.ownertel = ownertel;
        }
    }

    public static class VisitlistBean {
        /**
         * id : 20
         * visittime : 1516377600000
         */

        private int id;
        private String visittime;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getVisittime() {
            return visittime;
        }

        public void setVisittime(String visittime) {
            this.visittime = visittime;
        }
    }

    public static class ContfilelistBean {
        /**
         * filepath : 361adecec4324a6e8818d3b4508aa8af.pdf
         * filename : Python人工智能.pdf
         */

        private String id;
        private String filepath;
        private String filename;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFilepath() {
            return filepath;
        }

        public void setFilepath(String filepath) {
            this.filepath = filepath;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }
    }

    public static class PayListBean {
        /**
         * actualpay : 200
         * paydate : 2018-01-20
         */

        private String actualpay;
        private String paydate;

        public String getActualpay() {
            return actualpay;
        }

        public void setActualpay(String actualpay) {
            this.actualpay = actualpay;
        }

        public String getPaydate() {
            return paydate;
        }

        public void setPaydate(String paydate) {
            this.paydate = paydate;
        }
    }
}
