package com.newenergy.ui.analysis;

import android.os.Bundle;

import com.newenergy.R;
import com.newenergy.ui.base.fragment.ABaseTabFragment;
import com.newenergy.ui.equitpment.EquipmentWarnListFragment;

import java.util.ArrayList;
import java.util.List;

public class DataAnalysisFragment extends ABaseTabFragment{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarTitle(getString(R.string.title_data_analysis));
    }

    @Override
    protected List<ABaseTabFragment.FragmentParam> initFramgentData() {
        ArrayList<FragmentParam> list = new ArrayList<>();
        list.add(new FragmentParam(getResources().getString(R.string.tab_usage_refrige), UsageRefrigeFragment.class.getName()));
        list.add(new FragmentParam(getResources().getString(R.string.tab_usage_electricity), UsageElectricityFragment.class.getName()));
        list.add(new FragmentParam(getResources().getString(R.string.tab_usage_water), UsageWaterFragment.class.getName()));
        return list;
    }
}
