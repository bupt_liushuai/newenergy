package com.newenergy.ui.analysis.bean;

import com.google.gson.annotations.SerializedName;
import com.newenergy.ui.base.bean.BaseBean;

import java.util.List;

/**
 * Created by Liushuai on 2018/3/27.
 */

public class PieBean extends BaseBean {
    /**
     * series : [{"name":"用冷量","type":"pie","data":[{"name":"03-14","value":0},{"name":"03-16","value":693735}]}]
     * status : 1
     * legend : ["03-14","03-16"]
     */

    private List<SeriesBean> series;
    private List<String> legend;


    public List<SeriesBean> getSeries() {
        return series;
    }

    public void setSeries(List<SeriesBean> series) {
        this.series = series;
    }

    public List<String> getLegend() {
        return legend;
    }

    public void setLegend(List<String> legend) {
        this.legend = legend;
    }

    public static class SeriesBean {
        /**
         * name : 用冷量
         * type : pie
         * data : [{"name":"03-14","value":0},{"name":"03-16","value":693735}]
         */

        private String name;
        private String type;
        private List<DataBean> data;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<DataBean> getData() {
            return data;
        }

        public void setData(List<DataBean> data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * name : 03-14
             * value : 0
             */

            private String name;
            private int value;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getValue() {
                return value;
            }

            public void setValue(int value) {
                this.value = value;
            }
        }
    }
}
