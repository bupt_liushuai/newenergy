package com.newenergy.ui.analysis.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.newenergy.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class RadioAdapter extends RecyclerView.Adapter<RadioAdapter.ViewHolder> implements OnCheckChangeListener {

    private List<List<String>> mTitles;
    private LayoutInflater mLayoutInflater;
    private Set<String> mRadioSelections = new HashSet<>();

    public RadioAdapter(Context context, List<List<String>> titles) {
        mTitles = titles;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public RadioAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RadioAdapter.ViewHolder(mLayoutInflater.inflate(R.layout.fragment_data_analysis_refrige_radio, null), this);
    }

    @Override
    public void onBindViewHolder(RadioAdapter.ViewHolder holder, int position) {
        holder.mCheckBox.setText(mTitles.get(position).get(0));
    }

    @Override
    public int getItemCount() {
        return mTitles != null ? mTitles.size() : 0;
    }


    @Override
    public void onCheckChange(String title, boolean select) {
        if (select) {
            mRadioSelections.add(title);
        } else {
            mRadioSelections.remove(title);
        }
    }

    public List<List<String>> getSelectins() {
        List<List<String>> result = new ArrayList<>();
        for (List<String> title : mTitles) {
            if (mRadioSelections.contains(title.get(0))) {
                result.add(title);
            }
        }

        return result;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CheckBox mCheckBox;

        public ViewHolder(View itemView, final OnCheckChangeListener checkChangeListener) {
            super(itemView);
            mCheckBox = (CheckBox) itemView.findViewById(R.id.checkBox);

            mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    checkChangeListener.onCheckChange(buttonView.getText().toString(), isChecked);
                }
            });
        }

    }
}
