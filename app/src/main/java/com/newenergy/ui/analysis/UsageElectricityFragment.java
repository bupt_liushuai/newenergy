package com.newenergy.ui.analysis;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.github.mikephil.charting.charts.BarChart;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.analysis.bean.BarBean;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.base.network.task.VolleyManager;
import com.newenergy.ui.base.widge.DatePickView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 用电信息Fragment
 * create by ls
 */
public class UsageElectricityFragment extends ABaseFragment {

    @BindView(R.id.datePickView)
    DatePickView mDatePickView;

    @BindView(R.id.chartContainer)
    FrameLayout mFrameLayout;

    @Override
    public int inflateContentView() {
        return R.layout.fragment_data_analysis_comm;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @OnClick({R.id.check})
    void OnClick(View v) {
        switch (v.getId()) {
            case R.id.check:
                check();
                break;
        }
    }

    private void check() {
        Request r = RequestDefine.getElecData(mDatePickView.getLeftTime(), mDatePickView.getRightTime());
        final CommonRequest<String> request = new CommonRequest<String>(r, new IParseNetwork<String>() {
            @Override
            public String parseNetworkResponse(String jsonStr) {
                return jsonStr;
            }
        });

        request.setResponseListener(new IResponseListener<String>() {
            @Override
            public void onResponse(int requestId, String response) {
                setBarData(response);
            }

            @Override
            public void onErrorResponse(int requestId, VolleyError error) {
                Toast.makeText(getContext(), error != null && TextUtils.isEmpty(error.getMessage()) ? error.getMessage() : "数据加载失败", Toast.LENGTH_SHORT).show();
            }
        });
        VolleyManager.addRequest(request);
    }

    private void setBarData(String json) {
        BarBean barBean = JsonUtils.fromJson(json, BarBean.class);
        if (barBean != null && barBean.getSeries() != null && !barBean.getSeries().isEmpty()) {
            BarChart chart = ChartUtils.createBarChart(getContext(), barBean.getSeries().get(0).getData(), barBean.getCategory(), "用电分析");
            if (mFrameLayout != null) {
                if (mFrameLayout.getChildCount() > 0) {
                    mFrameLayout.removeAllViews();
                }
                mFrameLayout.addView(chart);
            }
        } else {
            Toast.makeText(getContext(), barBean != null && !TextUtils.isEmpty(barBean.getMsg()) ? barBean.getMsg() : "数据为空", Toast.LENGTH_SHORT).show();
        }
    }

}
