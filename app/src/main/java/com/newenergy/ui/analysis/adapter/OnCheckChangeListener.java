package com.newenergy.ui.analysis.adapter;

public interface OnCheckChangeListener {
    void onCheckChange(String title, boolean select);
}
