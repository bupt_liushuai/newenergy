package com.newenergy.ui.analysis;

import java.util.List;

public class BlockBean {


    private List<List<String>> block;

    public List<List<String>> getBlock() {
        return block;
    }

    public void setBlock(List<List<String>> block) {
        this.block = block;
    }
}
