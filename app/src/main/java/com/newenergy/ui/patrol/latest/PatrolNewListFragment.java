package com.newenergy.ui.patrol.latest;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.BaseDatePickFragment;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.patrol.bean.PatrolBean;

/**
 * Created by liushuai on 2017/9/4.
 */

public class PatrolNewListFragment extends BaseDatePickFragment<PatrolBean.QuerymaintainBean, PatrolBean> {

    public final static String PARAM_TYPE = "type";
    public final static String TYPE_1 = "type1";
    public final static String TYPE_2 = "type2";
    public final static String TYPE_3 = "type3";
    private String mType = RequestUrl.MAINTAIN_ALL;
    public final static String TAG = "PatrolNewListFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mType = getArguments().getString(PARAM_TYPE);
        }
    }

    @Override
    protected BaseVolleyRequest<PatrolBean> createNetRequest(boolean isRefresh) {
        String url = RequestUrl.MAINTAIN_ALL;
        if (!TextUtils.isEmpty(mType)) {
            switch (mType) {
                case TYPE_1:
                    url = RequestUrl.MAINTAIN_ALL;
                    break;
                case TYPE_2:
                    url = RequestUrl.GET_DEV_MAININFO;
                    break;
                case TYPE_3:
                    url = RequestUrl.GET_DEV_REPAIR;
                    break;
            }
        }

        Request request = RequestDefine.getMainListRequest(url, getDatePickView().getLeftTime(), getDatePickView().getRightTime(), getPageIndex());
        CommonRequest<PatrolBean> r = new CommonRequest<>(request, new IParseNetwork<PatrolBean>() {
            @Override
            public PatrolBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, PatrolBean.class);
            }
        });
        return r;
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, PatrolBean response) {
        super.onResponse(isNetResponse, isRefresh, response);

        Log.d(TAG, "数据加载完成");
    }

    @Override
    protected boolean checkHasMore(PatrolBean response) {
        return response != null && response.getPageinfo() != null && response.getPageinfo().getPageNumber() == response.getPageinfo().getTotalNumber();
    }


    @Override
    protected boolean checkValidResponse(PatrolBean response) {
        return false;
    }

    @Override
    protected void updateAdapterData(BaseQuickAdapter<PatrolBean.QuerymaintainBean, BaseViewHolder> adapter, PatrolBean response, boolean isRefresh, boolean isNetResponse) {
        if (response != null && response.getQuerymaintain() != null) {
            if (isRefresh) {
                adapter.setNewData(response.getQuerymaintain());
            } else {
                adapter.addData(response.getQuerymaintain());
            }
        }
    }

    @Override
    protected BaseQuickAdapter<PatrolBean.QuerymaintainBean, BaseViewHolder> createAdapter() {
        return new PatrolNewListAdapter();
    }

}
