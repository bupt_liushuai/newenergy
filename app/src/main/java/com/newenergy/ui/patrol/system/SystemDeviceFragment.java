package com.newenergy.ui.patrol.system;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by Liushuai on 2018/3/26.
 */

public class SystemDeviceFragment extends ABaseRequestFragment<SystemDeviceBean> {

    @BindView(R.id.recylcerView)

    RecyclerView mRecyclerView;

    private List<SystemSectionEntity> mData = new ArrayList<>();

    private SystemDeviceSectionAdapter mAdapter;


    @Override
    public int inflateContentView() {
        return R.layout.fragment_device_list;
    }

    @Override
    public void setContentView(ViewGroup view) {
        super.setContentView(view);
    }

    @Override
    protected BaseVolleyRequest<SystemDeviceBean> createNetRequest(boolean isRefresh) {
        Request request = RequestDefine.getSystemDeviceInfo();
        return new CommonRequest<>(request, new IParseNetwork<SystemDeviceBean>() {
            @Override
            public SystemDeviceBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, SystemDeviceBean.class);
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new SystemDeviceSectionAdapter(mData);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, SystemDeviceBean response) {
        super.onResponse(isNetResponse, isRefresh, response);

        if (response != null && response.getSys_devs() != null) {
            List<SystemDeviceBean.SysDevsBean> list = response.getSys_devs(); //总数据
            if (list != null && !list.isEmpty()) {
                for (SystemDeviceBean.SysDevsBean devsBean : list) {
                    List<SystemDeviceBean.SysDevsBean.DevicesBean> devicesBeans = devsBean.getDevices();
                    mData.add(new SystemSectionEntity(devsBean.getSys_name(), String.valueOf(devicesBeans.size())));
                    mData.add(new SystemSectionEntity(devicesBeans));
                }
            }

            mAdapter.setNewData(mData);
        }
    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }


    @Override
    public SystemDeviceBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(SystemDeviceBean response) {

    }
}