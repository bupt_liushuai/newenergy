package com.newenergy.ui.patrol;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.newenergy.R;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.patrol.deviceinfo.DeviceListFragment;
import com.newenergy.ui.patrol.latest.PatrolNewListFragment;
import com.newenergy.ui.patrol.scan.ScanFragment;
import com.newenergy.ui.patrol.system.SystemDeviceFragment;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;

/**
 * 日常巡查
 * Created by liushuai on 2017/9/4.
 */

public class PatrolFragment extends ABaseFragment {

    private static final int ZXING_CAMERA_PERMISSION = 1;
    @BindView(R.id.tabLayout)
    protected SegmentTabLayout mTab;
    private Fragment mLastFragment;

    private Map<String, ABaseFragment> mMap = new HashMap<>();
    @Override
    public int inflateContentView() {
        return R.layout.fragment_patrol;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle("日常巡查");
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTab.setTabData(new String[]{"最新数据", "按地块", "按系统"});
        initView();
    }

    private void initView() {
        initChartFragment(PatrolNewListFragment.class.getName());
        mTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                String clas = PatrolNewListFragment.class.getName();
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        clas = DeviceListFragment.class.getName();
                        break;
                    case 2:
                        clas = SystemDeviceFragment.class.getName();
                        break;
                }

                initChartFragment(clas);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    private void initChartFragment(String name) {
        FragmentManager fm = getChildFragmentManager();//注：getChildFragmentManager
        FragmentTransaction transaction = fm.beginTransaction();
        if ( mLastFragment != null) {
            transaction.hide(mLastFragment);
        }
        Fragment fragment = fm.findFragmentByTag(name);
        if (fragment == null) {
            fragment = Fragment.instantiate(getContext(), name, getArguments());
            transaction.add(R.id.container, fragment, name);
        } else {
            if (!fragment.isAdded()) {
                transaction.add(R.id.container, fragment, name);
            }else {
                transaction.show(fragment);
            }
        }

        transaction.commit();
        mLastFragment = fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.partol_menu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.patrol_device:
                //Toast.makeText(getActivity(), "巡查设备", Toast.LENGTH_SHORT).show();
                //ScanFragment.launch((BaseActivity) getActivity());
                goToScanActivity();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void goToScanActivity() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            FragmentContainerActivity.launch(getActivity(), ScanFragment.class, null);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ZXING_CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    goToScanActivity();
                } else {
                    Toast.makeText(getActivity(), "请开启照相机权限", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }


}
