package com.newenergy.ui.patrol.viewer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.OnPhotoTapListener;
import com.github.chrisbanes.photoview.PhotoView;
import com.newenergy.R;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.base.widge.ViewPagerAdapter;

import java.util.List;

import butterknife.BindView;

/**
 * Created by liushuai on 2017/9/26.
 */

public class PicShowFragment extends ABaseFragment implements ViewPager.OnPageChangeListener {
    public final static String PARAM_IMGS = "param_imgs";
    public final static String PARAM_POSITION = "param_position";

    @BindView(R.id.picture_view_pager)
    ViewPager mPager;

    private List<String> mPaths;
    private int mPosition;

    private PagerAdapter mAdapter;
    private LayoutInflater mInflater;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInflater = LayoutInflater.from(getActivity());
        if (getArguments() != null) {
            mPaths = (List<String>) getArguments().getSerializable(PARAM_IMGS);
            mPosition = getArguments().getInt(PARAM_POSITION);
        }
        if (mPaths == null) return;
        setActionBarTitle(mPosition, mPaths.size());
        initAdapter();
    }

    public void setActionBarTitle(int current, int total) {
        super.setActionBarTitle((current + 1) + "/" + total);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        setActivityTheme();
        super.onActivityCreated(savedInstanceState);
    }

    private void setActivityTheme() {
        BaseActivity activity = (BaseActivity) getActivity();
        Toolbar toolbar = activity.getToolbar();
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorBlackTrans));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPager.setAdapter(mAdapter);
        mPager.setCurrentItem(mPosition);

        mPager.addOnPageChangeListener(this);
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_pic_show;
    }

    private void initAdapter() {
        mAdapter = new ViewPagerAdapter() {

            @Override
            public int getCount() {
                return mPaths != null ? mPaths.size() : 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = mInflater.inflate(R.layout.item_pic_show, parent, false);
                }

                PhotoView photoView = (PhotoView) convertView.findViewById(R.id.photoView);
                Glide.with(getActivity())
                        .load(mPaths.get(position))
                        .apply(new RequestOptions()
                                .centerCrop()
                        )
                        .into(photoView);

                photoView.setOnPhotoTapListener(new OnPhotoTapListener() {
                    @Override
                    public void onPhotoTap(ImageView view, float x, float y) {
                        if (getActionBar().isShowing()) {
                            getActionBar().hide();
                        } else {
                            getActionBar().show();
                        }
                    }
                });
                return convertView;
            }
        };
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        setActionBarTitle(position, mPaths.size());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
