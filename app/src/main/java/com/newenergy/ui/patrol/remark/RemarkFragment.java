package com.newenergy.ui.patrol.remark;

import android.os.Bundle;

import com.newenergy.R;
import com.newenergy.ui.base.fragment.ABaseFragment;

/**
 * Created by ls on 17/9/17.
 */

public class RemarkFragment extends ABaseFragment{
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle(getResources().getString(R.string.title_patrol));
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_remark;
    }
}
