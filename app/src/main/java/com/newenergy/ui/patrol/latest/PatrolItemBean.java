package com.newenergy.ui.patrol.latest;

import java.util.List;

/**
 * Created by ls on 17/9/5.
 */

public class PatrolItemBean {

    List<String> mTitles;

    public PatrolItemBean() {
    }

    public PatrolItemBean(List<String> mTitles) {
        this.mTitles = mTitles;
    }

    public List<String> getmTitles() {
        return mTitles;
    }

    public void setmTitles(List<String> mTitles) {
        this.mTitles = mTitles;
    }
}
