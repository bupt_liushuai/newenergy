package com.newenergy.ui.patrol.latest;

import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.patrol.bean.PatrolBean;
import com.newenergy.ui.patrol.viewer.PicViewerFragment;

/**
 * Created by ls on 17/9/12.
 */

public class PatrolNewListAdapter extends BaseQuickAdapter<PatrolBean.QuerymaintainBean, BaseViewHolder> {
    public PatrolNewListAdapter() {
        super(R.layout.item_patrol_new);
    }

    @Override
    protected void convert(BaseViewHolder helper, final PatrolBean.QuerymaintainBean item) {

        helper.setText(R.id.name, item.getUsername());
        helper.setText(R.id.date, item.getCreatetime());
        helper.setText(R.id.device_name, String.format(mContext.getResources().getString(R.string.device_name), item.getDevname()));
        helper.setText(R.id.block_name, String.format(mContext.getResources().getString(R.string.block_name), item.getBlockname()));
        if (TextUtils.isEmpty(item.getMemo())) {
            helper.setVisible(R.id.rl_extra, false);
        } else {
            helper.setVisible(R.id.rl_extra, true);
            helper.setText(R.id.tv_extra, item.getMemo());
        }
        helper.getView(R.id.check).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentArgs args = new FragmentArgs();
                args.add(PicViewerFragment.PARAM_ID, String.valueOf(item.getId()));
                args.add(PicViewerFragment.PARAM_TYPE, PicViewerFragment.PARAM_TYPE_PATROL);
                FragmentContainerActivity.launch((BaseActivity) mContext, PicViewerFragment.class, args);
            }
        });

    }
}
