package com.newenergy.ui.patrol.deviceinfo;

import com.chad.library.adapter.base.entity.SectionEntity;

import java.util.List;

/**
 * Created by ls on 17/9/5.
 */

public class DeviceSection extends SectionEntity<List<DeviceInfoBean.BlockDevsBean.DevicesBean>> {

    private String mCount;
    public DeviceSection(String header, String count) {
        super(true, header);
        mCount = count;
    }

    public DeviceSection(List<DeviceInfoBean.BlockDevsBean.DevicesBean> t) {
        super(t);
    }

    public String getCount() {
        return mCount;
    }

    public void setCount(String mCount) {
        this.mCount = mCount;
    }
}
