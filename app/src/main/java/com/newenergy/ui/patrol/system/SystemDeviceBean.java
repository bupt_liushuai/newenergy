package com.newenergy.ui.patrol.system;

import com.newenergy.ui.base.bean.BaseBean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Liushuai on 2018/3/26.
 */

public class SystemDeviceBean extends BaseBean {

    private List<SysDevsBean> sys_devs;

    public List<SysDevsBean> getSys_devs() {
        return sys_devs;
    }

    public void setSys_devs(List<SysDevsBean> sys_devs) {
        this.sys_devs = sys_devs;
    }

    public static class SysDevsBean {
        /**
         * sys_name : 制冷（冰）系统
         * sys_id : 1
         * devices : [{"id":1,"name":"双工况制冷主机"},{"id":2,"name":"双工况制冷主机"},{"id":3,"name":"双工况制冷主机"},{"id":4,"name":"双工况制冷主机"},{"id":5,"name":"双工况制冷主机"}]
         */

        private String sys_name;
        private int sys_id;
        private List<DevicesBean> devices;

        public String getSys_name() {
            return sys_name;
        }

        public void setSys_name(String sys_name) {
            this.sys_name = sys_name;
        }

        public int getSys_id() {
            return sys_id;
        }

        public void setSys_id(int sys_id) {
            this.sys_id = sys_id;
        }

        public List<DevicesBean> getDevices() {
            return devices;
        }

        public void setDevices(List<DevicesBean> devices) {
            this.devices = devices;
        }

        public static class DevicesBean implements Serializable {
            /**
             * id : 1
             * name : 双工况制冷主机
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
