package com.newenergy.ui.video.list;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.newenergy.R;
import com.newenergy.ui.video.bean.VideoListBean;
import com.newenergy.ui.video.bean.VideoListExpendSectionBean;
import com.newenergy.ui.video.detail.VideoDetailActivity;
import com.newenergy.ui.video.detail.VideoDetailFragment;

import java.util.List;

/**
 * Created by ls on 17/9/18.
 */

public class VideoListChildAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> implements BaseQuickAdapter.OnItemChildClickListener {
    public static final int TYPE_LEVEL_0 = 0;
    public static final int TYPE_LEVEL_1 = 1;
    public static final int TYPE_LEVEL_2 = 2;

    public VideoListChildAdapter(List<MultiItemEntity> data) {
        super(data);
        //super(R.layout.item_video_list_section_child_list_child, R.layout.item_video_list_section_child_list_head, data);
        setOnItemChildClickListener(this);

        addItemType(TYPE_LEVEL_0, R.layout.item_video_group_section_head);
        addItemType(TYPE_LEVEL_1, R.layout.item_video_group_section_child_head);
        addItemType(TYPE_LEVEL_2, R.layout.item_video_group_section_child);
    }


    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

    }


    private OnDialogClickListener mListener;

    @Override
    protected void convert(final BaseViewHolder helper, final MultiItemEntity item) {
        switch (helper.getItemViewType()) {
            case TYPE_LEVEL_0: {
                VideoListExpendSectionBean b = (VideoListExpendSectionBean) item;
                helper.setText(R.id.name, b.getName());
                break;
            }

            case TYPE_LEVEL_1: {
                final VideoListExpendSectionBean b = (VideoListExpendSectionBean) item;

                helper.setText(R.id.name, b.getName());

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        int pos = helper.getAdapterPosition();
                        if (b.isExpanded()) {
                            collapse(pos);
                            helper.setImageResource(R.id.arrow, R.drawable.ic_group_right);
                        } else {
                            expand(pos);
                            helper.setImageResource(R.id.arrow, R.drawable.ic_group_bottom);
                        }
                        notifyItemChanged(helper.getLayoutPosition());
                    }
                });
                break;
            }
            case TYPE_LEVEL_2: {
                final VideoListBean.ControlUnitListBean.RegionListBean.CameraListBean b = (VideoListBean.ControlUnitListBean.RegionListBean.CameraListBean) item;
                helper.setText(R.id.name, b.getNodename());
                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        FragmentArgs args = new FragmentArgs();
//                        args.add(VideoDetailFragment.PARAM_CAMERA_BEAN, b);
//                        FragmentContainerActivity.launch((Activity) mContext, VideoDetailFragment.class, args);
                        Bundle args = new Bundle();
                        args.putString(VideoDetailActivity.PARAM_CAMERA_BEAN, b.getNodetitle());
                        Intent i = new Intent(mContext, VideoDetailActivity.class);
                        i.putExtras(args);
                        mContext.startActivity(i);
                    }
                });

                break;
            }
        }
    }


    public interface OnDialogClickListener {
        boolean onItemClick(String phone);
    }

    public void setOnDialogClickListener(OnDialogClickListener listener) {
        this.mListener = listener;
    }

}
