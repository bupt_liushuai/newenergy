package com.newenergy.ui.video.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.newenergy.ui.base.bean.BaseBean;
import com.newenergy.ui.video.list.VideoListChildAdapter;

import java.io.Serializable;
import java.util.List;

/**
 * Created by liushuai on 2017/11/13.
 */

public class VideoListBean extends BaseBean{


    private List<ControlUnitListBean> controlUnitList;

    public List<ControlUnitListBean> getControlUnitList() {
        return controlUnitList;
    }

    public void setControlUnitList(List<ControlUnitListBean> controlUnitList) {
        this.controlUnitList = controlUnitList;
    }

    public static class ControlUnitListBean {
        /**
         * id : 41
         * regionList : [{"id":42,"isused":true,"nodetitle":"1","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":56,"isused":true,"nodetitle":"62","level":3,"remark":"SYS_VEDIO","nodename":"001-1F北入口（球）","pid":1},{"id":60,"isused":true,"nodetitle":"274","level":3,"remark":"SYS_VEDIO","nodename":"002-1F地面南入口-半","pid":1},{"id":63,"isused":true,"nodetitle":"273","level":3,"remark":"SYS_VEDIO","nodename":"003-1F地面中门入口-半","pid":1},{"id":65,"isused":true,"nodetitle":"55","level":3,"remark":"SYS_VEDIO","nodename":"004-1F南楼梯口（枪）","pid":1},{"id":67,"isused":true,"nodetitle":"53","level":3,"remark":"SYS_VEDIO","nodename":"005-BJ南楼梯口（枪）","pid":1},{"id":69,"isused":true,"nodetitle":"44","level":3,"remark":"SYS_VEDIO","nodename":"006-BJ参观走廊1（枪）","pid":1},{"id":71,"isused":true,"nodetitle":"48","level":3,"remark":"SYS_VEDIO","nodename":"007-BJ参观走廊2(枪)","pid":1},{"id":72,"isused":true,"nodetitle":"54","level":3,"remark":"SYS_VEDIO","nodename":"008-BJ电梯厅-枪","pid":1},{"id":73,"isused":true,"nodetitle":"10","level":3,"remark":"SYS_VEDIO","nodename":"009-BJUPS室-球","pid":1},{"id":74,"isused":true,"nodetitle":"12","level":3,"remark":"SYS_VEDIO","nodename":"010-BJ网络机房-球","pid":1},{"id":75,"isused":true,"nodetitle":"11","level":3,"remark":"SYS_VEDIO","nodename":"011-BJ控制室-球","pid":1},{"id":76,"isused":true,"nodetitle":"52","level":3,"remark":"SYS_VEDIO","nodename":"012-B1南楼梯口-枪","pid":1},{"id":77,"isused":true,"nodetitle":"45","level":3,"remark":"SYS_VEDIO","nodename":"013-B1走廊南1-枪","pid":1},{"id":78,"isused":true,"nodetitle":"49","level":3,"remark":"SYS_VEDIO","nodename":"014-B1走廊南2-枪","pid":1},{"id":79,"isused":true,"nodetitle":"59","level":3,"remark":"SYS_VEDIO","nodename":"015-B1走廊中1-枪","pid":1},{"id":80,"isused":true,"nodetitle":"60","level":3,"remark":"SYS_VEDIO","nodename":"016-B1走廊中2-枪","pid":1},{"id":81,"isused":true,"nodetitle":"57","level":3,"remark":"SYS_VEDIO","nodename":"017-B1走廊北-枪","pid":1},{"id":82,"isused":true,"nodetitle":"58","level":3,"remark":"SYS_VEDIO","nodename":"018-B1电梯厅-枪","pid":1},{"id":83,"isused":true,"nodetitle":"156","level":3,"remark":"SYS_VEDIO","nodename":"019-B1双工况1-球","pid":1},{"id":84,"isused":true,"nodetitle":"157","level":3,"remark":"SYS_VEDIO","nodename":"020-B1双工况2-球","pid":1},{"id":85,"isused":true,"nodetitle":"154","level":3,"remark":"SYS_VEDIO","nodename":"021-B1双工况3-球","pid":1},{"id":86,"isused":true,"nodetitle":"155","level":3,"remark":"SYS_VEDIO","nodename":"022-B1双工况4-球","pid":1},{"id":87,"isused":true,"nodetitle":"4","level":3,"remark":"SYS_VEDIO","nodename":"023-B1机载机室1-球","pid":1},{"id":88,"isused":true,"nodetitle":"13","level":3,"remark":"SYS_VEDIO","nodename":"024-B1机载机室2-球","pid":1},{"id":89,"isused":true,"nodetitle":"5","level":3,"remark":"SYS_VEDIO","nodename":"025-B1机载机室3-球","pid":1},{"id":90,"isused":true,"nodetitle":"6","level":3,"remark":"SYS_VEDIO","nodename":"026-B1机载机室4-球","pid":1},{"id":91,"isused":true,"nodetitle":"8","level":3,"remark":"SYS_VEDIO","nodename":"027-B1低压室1-球","pid":1},{"id":92,"isused":true,"nodetitle":"9","level":3,"remark":"SYS_VEDIO","nodename":"028-B1低压室2-球","pid":1},{"id":93,"isused":true,"nodetitle":"63","level":3,"remark":"SYS_VEDIO","nodename":"029-B1会议室-球","pid":1},{"id":94,"isused":true,"nodetitle":"7","level":3,"remark":"SYS_VEDIO","nodename":"030-B1高压值班室-球","pid":1},{"id":95,"isused":true,"nodetitle":"3","level":3,"remark":"SYS_VEDIO","nodename":"031-B1高压室-球","pid":1},{"id":96,"isused":true,"nodetitle":"47","level":3,"remark":"SYS_VEDIO","nodename":"032-B2南楼梯口-枪","pid":1},{"id":97,"isused":true,"nodetitle":"50","level":3,"remark":"SYS_VEDIO","nodename":"033-B2走廊南-枪","pid":1},{"id":98,"isused":true,"nodetitle":"18","level":3,"remark":"SYS_VEDIO","nodename":"034-B2走廊中-枪","pid":1},{"id":99,"isused":true,"nodetitle":"20","level":3,"remark":"SYS_VEDIO","nodename":"035-B2走廊中2-枪","pid":1},{"id":100,"isused":true,"nodetitle":"19","level":3,"remark":"SYS_VEDIO","nodename":"036-B2走廊北-枪","pid":1},{"id":101,"isused":true,"nodetitle":"21","level":3,"remark":"SYS_VEDIO","nodename":"037-B2电梯厅-枪","pid":1},{"id":102,"isused":true,"nodetitle":"46","level":3,"remark":"SYS_VEDIO","nodename":"038-B2内走廊1-枪","pid":1},{"id":103,"isused":true,"nodetitle":"51","level":3,"remark":"SYS_VEDIO","nodename":"039-B2内走廊2-枪","pid":1},{"id":104,"isused":true,"nodetitle":"16","level":3,"remark":"SYS_VEDIO","nodename":"040-B2融冰泵房1-球","pid":1},{"id":105,"isused":true,"nodetitle":"15","level":3,"remark":"SYS_VEDIO","nodename":"041-B2融冰泵房2-球","pid":1},{"id":106,"isused":true,"nodetitle":"14","level":3,"remark":"SYS_VEDIO","nodename":"042-B2融冰泵房3-球","pid":1},{"id":107,"isused":true,"nodetitle":"17","level":3,"remark":"SYS_VEDIO","nodename":"043-B2融冰泵房4-球","pid":1},{"id":108,"isused":true,"nodetitle":"56","level":3,"remark":"SYS_VEDIO","nodename":"044-B2空调机房-球","pid":1},{"id":109,"isused":true,"nodetitle":"61","level":3,"remark":"SYS_VEDIO","nodename":"045-B2钢球间-球","pid":1}],"nodename":"能源站内监控","pid":1},{"id":43,"isused":true,"nodetitle":"3","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":110,"isused":true,"nodetitle":"248","level":3,"remark":"SYS_VEDIO","nodename":"26地块1号枪机","pid":3},{"id":111,"isused":true,"nodetitle":"250","level":3,"remark":"SYS_VEDIO","nodename":"26地块2号枪机","pid":3},{"id":112,"isused":true,"nodetitle":"251","level":3,"remark":"SYS_VEDIO","nodename":"26地块3号枪机","pid":3},{"id":113,"isused":true,"nodetitle":"252","level":3,"remark":"SYS_VEDIO","nodename":"26地块4号枪机","pid":3}],"nodename":"26地块监控","pid":1},{"id":44,"isused":true,"nodetitle":"4","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":57,"isused":true,"nodetitle":"254","level":3,"remark":"SYS_VEDIO","nodename":"25地块1号枪机","pid":4},{"id":59,"isused":true,"nodetitle":"253","level":3,"remark":"SYS_VEDIO","nodename":"25地块2号枪机","pid":4},{"id":62,"isused":true,"nodetitle":"249","level":3,"remark":"SYS_VEDIO","nodename":"25地块3号枪机","pid":4}],"nodename":"25地块监控","pid":1},{"id":45,"isused":true,"nodetitle":"5","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":121,"isused":true,"nodetitle":"262","level":3,"remark":"SYS_VEDIO","nodename":"21地块1号枪机","pid":5},{"id":122,"isused":true,"nodetitle":"263","level":3,"remark":"SYS_VEDIO","nodename":"21地块2号枪机","pid":5},{"id":123,"isused":true,"nodetitle":"264","level":3,"remark":"SYS_VEDIO","nodename":"21地块3号枪机","pid":5}],"nodename":"21地块监控","pid":1},{"id":46,"isused":true,"nodetitle":"6","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":129,"isused":true,"nodetitle":"270","level":3,"remark":"SYS_VEDIO","nodename":"20地块3号枪机","pid":6},{"id":130,"isused":true,"nodetitle":"271","level":3,"remark":"SYS_VEDIO","nodename":"20地块1号枪机","pid":6},{"id":131,"isused":true,"nodetitle":"272","level":3,"remark":"SYS_VEDIO","nodename":"20地块2号枪机","pid":6}],"nodename":"20地块监控","pid":1},{"id":47,"isused":true,"nodetitle":"7","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":118,"isused":true,"nodetitle":"259","level":3,"remark":"SYS_VEDIO","nodename":"18地块1号枪机","pid":7},{"id":119,"isused":true,"nodetitle":"260","level":3,"remark":"SYS_VEDIO","nodename":"18地块2号枪机","pid":7},{"id":120,"isused":true,"nodetitle":"261","level":3,"remark":"SYS_VEDIO","nodename":"18地块3号枪机","pid":7}],"nodename":"18地块监控","pid":1},{"id":48,"isused":true,"nodetitle":"8","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":124,"isused":true,"nodetitle":"265","level":3,"remark":"SYS_VEDIO","nodename":"18影院1号机","pid":8},{"id":136,"isused":true,"nodetitle":"285","level":3,"remark":"SYS_VEDIO","nodename":"18影院2号机","pid":8}],"nodename":"18地块影院监控","pid":1},{"id":49,"isused":true,"nodetitle":"9","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":125,"isused":true,"nodetitle":"266","level":3,"remark":"SYS_VEDIO","nodename":"14地块1号枪机","pid":9},{"id":126,"isused":true,"nodetitle":"267","level":3,"remark":"SYS_VEDIO","nodename":"14地块2号枪机","pid":9},{"id":127,"isused":true,"nodetitle":"268","level":3,"remark":"SYS_VEDIO","nodename":"14地块3号枪机","pid":9},{"id":128,"isused":true,"nodetitle":"269","level":3,"remark":"SYS_VEDIO","nodename":"14地块4号枪机","pid":9}],"nodename":"14地块监控","pid":1},{"id":51,"isused":true,"nodetitle":"11","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":114,"isused":true,"nodetitle":"255","level":3,"remark":"SYS_VEDIO","nodename":"11地块1号枪机","pid":11},{"id":115,"isused":true,"nodetitle":"256","level":3,"remark":"SYS_VEDIO","nodename":"11地块2号枪机","pid":11},{"id":116,"isused":true,"nodetitle":"257","level":3,"remark":"SYS_VEDIO","nodename":"11地块3号枪机","pid":11},{"id":117,"isused":true,"nodetitle":"258","level":3,"remark":"SYS_VEDIO","nodename":"11地块4号枪机","pid":11}],"nodename":"11地块监控","pid":1},{"id":52,"isused":true,"nodetitle":"12","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":132,"isused":true,"nodetitle":"281","level":3,"remark":"SYS_VEDIO","nodename":"16地块1号枪机","pid":12},{"id":133,"isused":true,"nodetitle":"282","level":3,"remark":"SYS_VEDIO","nodename":"16地块2号枪机","pid":12},{"id":134,"isused":true,"nodetitle":"283","level":3,"remark":"SYS_VEDIO","nodename":"16地块3号枪机","pid":12},{"id":135,"isused":true,"nodetitle":"284","level":3,"remark":"SYS_VEDIO","nodename":"16地块4号枪机","pid":12}],"nodename":"16地块监控","pid":1},{"id":55,"isused":true,"nodetitle":"15","level":2,"remark":"SYS_VEDIO","cameraList":[{"id":58,"isused":true,"nodetitle":"275","level":3,"remark":"SYS_VEDIO","nodename":"冷却塔1号枪机","pid":15},{"id":61,"isused":true,"nodetitle":"276","level":3,"remark":"SYS_VEDIO","nodename":"冷却塔2号枪机","pid":15},{"id":64,"isused":true,"nodetitle":"280","level":3,"remark":"SYS_VEDIO","nodename":"冷却塔3号枪机","pid":15},{"id":66,"isused":true,"nodetitle":"278","level":3,"remark":"SYS_VEDIO","nodename":"冷却塔4号枪机","pid":15},{"id":68,"isused":true,"nodetitle":"279","level":3,"remark":"SYS_VEDIO","nodename":"冷却塔5号枪机","pid":15},{"id":70,"isused":true,"nodetitle":"277","level":3,"remark":"SYS_VEDIO","nodename":"冷却塔6号枪机","pid":15},{"id":137,"isused":true,"nodetitle":"286","level":3,"remark":"SYS_VEDIO","nodename":"冷却塔7号枪机","pid":15}],"nodename":"冷却塔监控","pid":1}]
         * isused : true
         * nodetitle : 1
         * level : 1
         * remark : SYS_VEDIO
         * nodename : 主控制中心
         * pid : 2
         */

        private int id;
        private boolean isused;
        private String nodetitle;
        private int level;
        private String remark;
        private String nodename;
        private int pid;
        private List<RegionListBean> regionList;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isIsused() {
            return isused;
        }

        public void setIsused(boolean isused) {
            this.isused = isused;
        }

        public String getNodetitle() {
            return nodetitle;
        }

        public void setNodetitle(String nodetitle) {
            this.nodetitle = nodetitle;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getNodename() {
            return nodename;
        }

        public void setNodename(String nodename) {
            this.nodename = nodename;
        }

        public int getPid() {
            return pid;
        }

        public void setPid(int pid) {
            this.pid = pid;
        }

        public List<RegionListBean> getRegionList() {
            return regionList;
        }

        public void setRegionList(List<RegionListBean> regionList) {
            this.regionList = regionList;
        }

        public static class RegionListBean {
            /**
             * id : 42
             * isused : true
             * nodetitle : 1
             * level : 2
             * remark : SYS_VEDIO
             * cameraList : [{"id":56,"isused":true,"nodetitle":"62","level":3,"remark":"SYS_VEDIO","nodename":"001-1F北入口（球）","pid":1},{"id":60,"isused":true,"nodetitle":"274","level":3,"remark":"SYS_VEDIO","nodename":"002-1F地面南入口-半","pid":1},{"id":63,"isused":true,"nodetitle":"273","level":3,"remark":"SYS_VEDIO","nodename":"003-1F地面中门入口-半","pid":1},{"id":65,"isused":true,"nodetitle":"55","level":3,"remark":"SYS_VEDIO","nodename":"004-1F南楼梯口（枪）","pid":1},{"id":67,"isused":true,"nodetitle":"53","level":3,"remark":"SYS_VEDIO","nodename":"005-BJ南楼梯口（枪）","pid":1},{"id":69,"isused":true,"nodetitle":"44","level":3,"remark":"SYS_VEDIO","nodename":"006-BJ参观走廊1（枪）","pid":1},{"id":71,"isused":true,"nodetitle":"48","level":3,"remark":"SYS_VEDIO","nodename":"007-BJ参观走廊2(枪)","pid":1},{"id":72,"isused":true,"nodetitle":"54","level":3,"remark":"SYS_VEDIO","nodename":"008-BJ电梯厅-枪","pid":1},{"id":73,"isused":true,"nodetitle":"10","level":3,"remark":"SYS_VEDIO","nodename":"009-BJUPS室-球","pid":1},{"id":74,"isused":true,"nodetitle":"12","level":3,"remark":"SYS_VEDIO","nodename":"010-BJ网络机房-球","pid":1},{"id":75,"isused":true,"nodetitle":"11","level":3,"remark":"SYS_VEDIO","nodename":"011-BJ控制室-球","pid":1},{"id":76,"isused":true,"nodetitle":"52","level":3,"remark":"SYS_VEDIO","nodename":"012-B1南楼梯口-枪","pid":1},{"id":77,"isused":true,"nodetitle":"45","level":3,"remark":"SYS_VEDIO","nodename":"013-B1走廊南1-枪","pid":1},{"id":78,"isused":true,"nodetitle":"49","level":3,"remark":"SYS_VEDIO","nodename":"014-B1走廊南2-枪","pid":1},{"id":79,"isused":true,"nodetitle":"59","level":3,"remark":"SYS_VEDIO","nodename":"015-B1走廊中1-枪","pid":1},{"id":80,"isused":true,"nodetitle":"60","level":3,"remark":"SYS_VEDIO","nodename":"016-B1走廊中2-枪","pid":1},{"id":81,"isused":true,"nodetitle":"57","level":3,"remark":"SYS_VEDIO","nodename":"017-B1走廊北-枪","pid":1},{"id":82,"isused":true,"nodetitle":"58","level":3,"remark":"SYS_VEDIO","nodename":"018-B1电梯厅-枪","pid":1},{"id":83,"isused":true,"nodetitle":"156","level":3,"remark":"SYS_VEDIO","nodename":"019-B1双工况1-球","pid":1},{"id":84,"isused":true,"nodetitle":"157","level":3,"remark":"SYS_VEDIO","nodename":"020-B1双工况2-球","pid":1},{"id":85,"isused":true,"nodetitle":"154","level":3,"remark":"SYS_VEDIO","nodename":"021-B1双工况3-球","pid":1},{"id":86,"isused":true,"nodetitle":"155","level":3,"remark":"SYS_VEDIO","nodename":"022-B1双工况4-球","pid":1},{"id":87,"isused":true,"nodetitle":"4","level":3,"remark":"SYS_VEDIO","nodename":"023-B1机载机室1-球","pid":1},{"id":88,"isused":true,"nodetitle":"13","level":3,"remark":"SYS_VEDIO","nodename":"024-B1机载机室2-球","pid":1},{"id":89,"isused":true,"nodetitle":"5","level":3,"remark":"SYS_VEDIO","nodename":"025-B1机载机室3-球","pid":1},{"id":90,"isused":true,"nodetitle":"6","level":3,"remark":"SYS_VEDIO","nodename":"026-B1机载机室4-球","pid":1},{"id":91,"isused":true,"nodetitle":"8","level":3,"remark":"SYS_VEDIO","nodename":"027-B1低压室1-球","pid":1},{"id":92,"isused":true,"nodetitle":"9","level":3,"remark":"SYS_VEDIO","nodename":"028-B1低压室2-球","pid":1},{"id":93,"isused":true,"nodetitle":"63","level":3,"remark":"SYS_VEDIO","nodename":"029-B1会议室-球","pid":1},{"id":94,"isused":true,"nodetitle":"7","level":3,"remark":"SYS_VEDIO","nodename":"030-B1高压值班室-球","pid":1},{"id":95,"isused":true,"nodetitle":"3","level":3,"remark":"SYS_VEDIO","nodename":"031-B1高压室-球","pid":1},{"id":96,"isused":true,"nodetitle":"47","level":3,"remark":"SYS_VEDIO","nodename":"032-B2南楼梯口-枪","pid":1},{"id":97,"isused":true,"nodetitle":"50","level":3,"remark":"SYS_VEDIO","nodename":"033-B2走廊南-枪","pid":1},{"id":98,"isused":true,"nodetitle":"18","level":3,"remark":"SYS_VEDIO","nodename":"034-B2走廊中-枪","pid":1},{"id":99,"isused":true,"nodetitle":"20","level":3,"remark":"SYS_VEDIO","nodename":"035-B2走廊中2-枪","pid":1},{"id":100,"isused":true,"nodetitle":"19","level":3,"remark":"SYS_VEDIO","nodename":"036-B2走廊北-枪","pid":1},{"id":101,"isused":true,"nodetitle":"21","level":3,"remark":"SYS_VEDIO","nodename":"037-B2电梯厅-枪","pid":1},{"id":102,"isused":true,"nodetitle":"46","level":3,"remark":"SYS_VEDIO","nodename":"038-B2内走廊1-枪","pid":1},{"id":103,"isused":true,"nodetitle":"51","level":3,"remark":"SYS_VEDIO","nodename":"039-B2内走廊2-枪","pid":1},{"id":104,"isused":true,"nodetitle":"16","level":3,"remark":"SYS_VEDIO","nodename":"040-B2融冰泵房1-球","pid":1},{"id":105,"isused":true,"nodetitle":"15","level":3,"remark":"SYS_VEDIO","nodename":"041-B2融冰泵房2-球","pid":1},{"id":106,"isused":true,"nodetitle":"14","level":3,"remark":"SYS_VEDIO","nodename":"042-B2融冰泵房3-球","pid":1},{"id":107,"isused":true,"nodetitle":"17","level":3,"remark":"SYS_VEDIO","nodename":"043-B2融冰泵房4-球","pid":1},{"id":108,"isused":true,"nodetitle":"56","level":3,"remark":"SYS_VEDIO","nodename":"044-B2空调机房-球","pid":1},{"id":109,"isused":true,"nodetitle":"61","level":3,"remark":"SYS_VEDIO","nodename":"045-B2钢球间-球","pid":1}]
             * nodename : 能源站内监控
             * pid : 1
             */

            private int id;
            private boolean isused;
            private String nodetitle;
            private int level;
            private String remark;
            private String nodename;
            private int pid;
            private List<CameraListBean> cameraList;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public boolean isIsused() {
                return isused;
            }

            public void setIsused(boolean isused) {
                this.isused = isused;
            }

            public String getNodetitle() {
                return nodetitle;
            }

            public void setNodetitle(String nodetitle) {
                this.nodetitle = nodetitle;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }

            public String getNodename() {
                return nodename;
            }

            public void setNodename(String nodename) {
                this.nodename = nodename;
            }

            public int getPid() {
                return pid;
            }

            public void setPid(int pid) {
                this.pid = pid;
            }

            public List<CameraListBean> getCameraList() {
                return cameraList;
            }

            public void setCameraList(List<CameraListBean> cameraList) {
                this.cameraList = cameraList;
            }

            public static class CameraListBean implements MultiItemEntity, Serializable{
                /**
                 * id : 56
                 * isused : true
                 * nodetitle : 62
                 * level : 3
                 * remark : SYS_VEDIO
                 * nodename : 001-1F北入口（球）
                 * pid : 1
                 */

                private int id;
                private boolean isused;
                private String nodetitle;
                private int level;
                private String remark;
                private String nodename;
                private int pid;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public boolean isIsused() {
                    return isused;
                }

                public void setIsused(boolean isused) {
                    this.isused = isused;
                }

                public String getNodetitle() {
                    return nodetitle;
                }

                public void setNodetitle(String nodetitle) {
                    this.nodetitle = nodetitle;
                }

                public int getLevel() {
                    return level;
                }

                public void setLevel(int level) {
                    this.level = level;
                }

                public String getRemark() {
                    return remark;
                }

                public void setRemark(String remark) {
                    this.remark = remark;
                }

                public String getNodename() {
                    return nodename;
                }

                public void setNodename(String nodename) {
                    this.nodename = nodename;
                }

                public int getPid() {
                    return pid;
                }

                public void setPid(int pid) {
                    this.pid = pid;
                }

                @Override
                public int getItemType() {
                    return VideoListChildAdapter.TYPE_LEVEL_2;
                }
            }
        }
    }
}
