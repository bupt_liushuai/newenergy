package com.newenergy.ui.video.detail;

import android.os.Bundle;
import android.view.ViewGroup;

import com.newenergy.R;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.base.widge.MVideoView;
import com.newenergy.ui.video.bean.VideoListBean;

import butterknife.BindView;

/**
 * Created by liushuai on 2017/11/13.
 */

public class VideoDetailFragment extends ABaseRequestFragment<String>{

    public final static String PARAM_CAMERA_BEAN = "CameraBean";
    @BindView(R.id.videoView)
    MVideoView mMVideoView;

    VideoListBean.ControlUnitListBean.RegionListBean.CameraListBean mCameraListBean;
    @Override
    public String loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(String response) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null && getArguments() != null) {
            mCameraListBean = (VideoListBean.ControlUnitListBean.RegionListBean.CameraListBean) getArguments().getSerializable(PARAM_CAMERA_BEAN);
        }
    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }

    @Override
    protected BaseVolleyRequest<String> createNetRequest(boolean isRefresh) {
        Request request = RequestDefine.getVideoRun("22");
        CommonRequest<String> r = new CommonRequest<>(request, new IParseNetwork<String>() {
            @Override
            public String parseNetworkResponse(String jsonStr) {
                return jsonStr;
            }
        });
        return r;
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, String response) {
        super.onResponse(isNetResponse, isRefresh, response);

        String url = "rtsp://60.30.168.34:7554/livestream?userID=12345&puID="+ mCameraListBean.getNodetitle()+"&channelNo=0&streamtype=2&netType=0&streamEncoding=1&acsip=60.30.168.34&acsport=7002&acsusr=admin&acspwd=2015yujiapu&usrtye=0&ptzcontrol=1";
        mMVideoView.prepare(url);
        mMVideoView.start();
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_live;
    }

    @Override
    public void setContentView(ViewGroup view) {
        super.setContentView(view);
    }

    @Override
    protected boolean shouldShowProgressBeforeLoad() {
        return false;
    }
}
