package com.newenergy.common.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.TextUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


public class NRCache {
    public static final int TIME_NO_EXPERIED = 0;
    public static final int TIME_HOUR = 60 * 60;
    public static final int TIME_DAY = TIME_HOUR * 24;
    public static final int DEFAULT_EXPERIED_TIME = 3 * TIME_DAY; // 默认过期时间3天
    public static final long MAX_SIZE = 1024 * 1024 * 512; // 512MB
    private static final String TAG = "NRCache";
    private static final int MAX_COUNT = Integer.MAX_VALUE; // 不限制存放数据的数量
    private static String TIME_INFO_SEPARATOR = "_";
    private static Map<String, NRCache> mInstanceMap = new ConcurrentHashMap<>();
    private CacheManager mCache;

    private NRCache(File cacheDir, long max_size, int max_count) {
        if (!cacheDir.exists() && !cacheDir.mkdirs()) {
            return;
            //throw new RuntimeException("can't make dirs in " + cacheDir.getAbsolutePath());
        }
        mCache = new CacheManager(cacheDir, max_size, max_count);
    }

//    public static NRCache get(Context ctx, String cacheName) {
//        // 修改cache目录
//        File f = getDiskCacheDir(ctx, cacheName);
//        return get(f, MAX_SIZE, MAX_COUNT);
//    }

    //    public static NRCache get(File cacheDir) {
//        return get(cacheDir, MAX_SIZE, MAX_COUNT);
//    }

    public static NRCache get(Context ctx, String cacheName, long cacheSize) {
        // 修改cache目录
        File f = getDiskCacheDir(ctx, cacheName);
        return get(f, cacheSize, MAX_COUNT);
    }

    public static boolean isExternalStorageRemovable() {
        return Environment.isExternalStorageRemovable();
    }

    public static File getExternalCacheDir(Context context) {
        return context.getExternalCacheDir();
    }

    public static File getDiskCacheDir(Context context, String uniqueName) {
        // Check if media is mounted or storage is built-in, if so, try and use external cache dir
        // otherwise use internal cache dir
        File file = null;
        String cachePath = null;
        try {
            cachePath =
                    Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                            !isExternalStorageRemovable() ? getExternalCacheDir(context).getPath() :
                            null;
            if (!TextUtils.isEmpty(cachePath)) {
                file = new File(cachePath, uniqueName);
            }
        } catch (Exception e) {

        }
        if (file == null) {
            file = getInternalFile(context, uniqueName);
        }

        if (file != null && !file.exists()) {
            boolean success = file.mkdirs();
        }

        return file;
    }

    public static File getInternalFile(Context context, String uniqueName) {
        String cachePath = null;
        try {
            cachePath = context.getCacheDir().getPath();
        } catch (Exception e1) {
            cachePath = "/data/" + context.getPackageName() + "/cache/";
        }
        return new File(cachePath, uniqueName);
    }

    public static NRCache get(File cacheDir, long max_zise, int max_count) {
        NRCache manager = mInstanceMap.get(cacheDir.getAbsoluteFile() + myPid());
        if (manager == null) {
            manager = new NRCache(cacheDir, max_zise, max_count);
            mInstanceMap.put(cacheDir.getAbsolutePath() + myPid(), manager);
        }
        return manager;
    }

    private static String myPid() {
        return "_" + android.os.Process.myPid();
    }

    /**
     * 生成缓存文件的文件名
     */
    private static String doHashKey(String key) {
        String cacheKey;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(key.getBytes());
            cacheKey = bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }

        return cacheKey;
    }

    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    // =======================================
    // ============== byte 数据 读写 =============
    // =======================================

    /**
     * 获取缓存文件的路径
     *
     * @param key
     * @return 可能会null
     */
    public File getCachedFilePath(String key) {

        if(mCache == null) return null;

        File file = mCache.get(key);
        if (file == null || !file.exists()) {
            return null;
        }

        return file;
    }

    /**
     * 保存 byte数据 到 缓存中
     *
     * @param key   保存的key
     * @param value 保存的数据
     */
    private File putInternal(String key, byte[] value, String timeInfo) {

        if(mCache == null) return null;

        if (TextUtils.isEmpty(key) || value == null) {
            return null;
        }

        remove(key);

        File file = mCache.newFile(key, timeInfo);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            out.write(value);
            mCache.put(file);
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            //mCache.put(file);
        }
        return null;
    }

    // =======================================
    // ============= 序列化 数据 读写 ===============
    // =======================================

    /**
     * 获取 byte 数据
     *
     * @param key
     * @return byte 数据
     */
    public byte[] getAsBinary(String key) {

        if(mCache == null) return null;

        RandomAccessFile RAFile = null;
        boolean removeFile = false;
        try {
            File file = mCache.get(key);
            if (file == null || !file.exists()) {
                return null;
            }
            RAFile = new RandomAccessFile(file, "r");
            byte[] byteArray = new byte[(int) RAFile.length()];
            RAFile.read(byteArray);
            if (!Utils.isDueByFileName(file.getName())) {
                return byteArray;
            } else {
                removeFile = true;
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            if (RAFile != null) {
                try {
                    RAFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (removeFile)
                remove(key);
        }
    }

    public File put(String key, Object value, int saveTime) {

        if (value instanceof Bitmap) {
            // bitmap转成bytes保存
            return putInternal(key, Utils.Bitmap2Bytes((Bitmap) value), Utils.createDateInfo(saveTime));
        }
        // 只有序列化的对象才能保存
        if (value instanceof Serializable) {
            // 把对象转成bytes写入文件
            ObjectOutputStream oos = null;
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                oos = new ObjectOutputStream(baos);
                oos.writeObject(value);
                byte[] data = baos.toByteArray();
                return putInternal(key, data, Utils.createDateInfo(saveTime));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    /**
     * 读取 Serializable数据
     *
     * @param key
     * @return Serializable 数据
     */
    public Object getAsObject(String key) {
        byte[] data = getAsBinary(key);
        if (data != null) {
            ByteArrayInputStream bais = null;
            ObjectInputStream ois = null;
            try {
                bais = new ByteArrayInputStream(data);
                ois = new ObjectInputStream(bais);
                Object reObject = ois.readObject();
                return reObject;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                try {
                    if (bais != null)
                        bais.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    if (ois != null)
                        ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;

    }

//    /**
//     * 获取缓存文件
//     *
//     * @param key
//     * @return value 缓存的文件
//     */
//    public File file(String key) {
//        File f = mCache.newFile(key);
//        if (f.exists())
//            return f;
//        return null;
//    }

    /**
     * 读取 bitmap 数据
     *
     * @param key
     * @return bitmap 数据
     */
    public Bitmap getAsBitmap(String key) {
        byte[] bytes = getAsBinary(key);
        if (bytes == null) {
            return null;
        }
        return Utils.Bytes2Bimap(bytes);
    }

    public List<File> getAllCachedFiles() {
        try {
            if (mCache != null) {
                File[] files = mCache.cacheRootDir.listFiles();
                List<File> list = Arrays.asList(files);
                return list;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 移除某个key
     *
     * @param key
     * @return 是否移除成功
     */
    public boolean remove(String key) {
        if (mCache == null || TextUtils.isEmpty(key)) {
            return false;
        }
        return mCache.remove(key);
    }

    /**
     * 清除所有数据
     */
    public void clear() {
        if(mCache != null) {
            mCache.clear();
        }
    }

    public void clearExpiredFiles() {
        if(mCache != null) {
            mCache.clearExpiredFiles();
        }
    }

    private static class Utils {

        /**
         * 根据文件名判断是否过期了
         *
         * @param fileName
         * @return
         */
        private static boolean isDueByFileName(String fileName) {
            String[] strs = getDateInfoFromFileName(fileName);
            if (strs != null && strs.length == 2) {
                String saveTimeStr = strs[0];
                while (saveTimeStr.startsWith("0")) {
                    saveTimeStr = saveTimeStr.substring(1, saveTimeStr.length());
                }
                long saveTime = 0;
                long deleteAfter = 0;
                try {
                    saveTime = Long.valueOf(saveTimeStr);
                    deleteAfter = Long.valueOf(strs[1]);
                } catch (Exception e) {
                    e.printStackTrace();
                    return true; // 发生错误 视为过期
                }

                if (deleteAfter == TIME_NO_EXPERIED) {
                    return false;
                }

                if (System.currentTimeMillis() > saveTime + deleteAfter * 1000) {
                    return true;
                }
            }
            return false;
        }

        private static String[] getDateInfoFromFileName(String fileName) {
            if (hasDateInfoInFileName(fileName)) {
                try {
                    String[] strs = fileName.split(TIME_INFO_SEPARATOR);
                    if (strs != null && strs.length == 2 && !TextUtils.isEmpty(strs[1])) {
                        String[] tmpStr = strs[1].split("-");
                        String saveDate = tmpStr[0];
                        String deleteAfter = tmpStr[1];
                        return new String[]{saveDate, deleteAfter};
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        private static boolean hasDateInfoInFileName(String fileName) {
            if (!TextUtils.isEmpty(fileName) && fileName.contains(TIME_INFO_SEPARATOR)) {
                String[] strs = fileName.split(TIME_INFO_SEPARATOR);
                if (strs != null && strs.length == 2 && !TextUtils.isEmpty(strs[1])) {
                    return true;
                }
            }
            return false;
        }

        private static String createDateInfo(int second) {
            String currentTime = System.currentTimeMillis() + "";
            while (currentTime.length() < 13) {
                currentTime = "0" + currentTime;
            }
            return currentTime + "-" + second;
            //return currentTime + "-" + second + mSeparator;
        }

        /*
         * Bitmap → byte[]
         */
        private static byte[] Bitmap2Bytes(Bitmap bm) {
            if (bm == null) {
                return null;
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
            return baos.toByteArray();
        }

        /*
         * byte[] → Bitmap
         */
        private static Bitmap Bytes2Bimap(byte[] b) {
            if (b.length == 0) {
                return null;
            }
            return BitmapFactory.decodeByteArray(b, 0, b.length);
        }
    }

    private class CacheManager {
        private final AtomicLong cacheSize;
        private final AtomicInteger cacheCount;
        private final long sizeLimit;
        private final int countLimit;
        private final Map<File, Long> lastUsageDates = Collections.synchronizedMap(new HashMap<File, Long>());
        protected File cacheRootDir;

        private CacheManager(File cacheRootDir, long sizeLimit, int countLimit) {
            this.cacheRootDir = cacheRootDir;
            this.sizeLimit = sizeLimit;
            this.countLimit = countLimit;
            cacheSize = new AtomicLong();
            cacheCount = new AtomicInteger();
            calculateCacheSizeAndCacheCount();
        }

        /**
         * 计算 cacheSize和cacheCount
         */
        private void calculateCacheSizeAndCacheCount() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    int size = 0;
                    int count = 0;
                    File[] cachedFiles = cacheRootDir.listFiles();
                    if (cachedFiles != null) {
                        for (File cachedFile : cachedFiles) {
                            size += calculateSize(cachedFile);
                            count += 1;
                            lastUsageDates.put(cachedFile, cachedFile.lastModified());
                        }
                        cacheSize.set(size);
                        cacheCount.set(count);
                    }
                }
            }).start();
        }

        /**
         * 清楚过期文件 不建议调用 需要读取每一个文件里面的信息 比较耗时
         */
        private void clearExpiredFiles() {
            try {
                if (cacheRootDir != null) {
                    File[] cachedFiles = cacheRootDir.listFiles();
                    if (cachedFiles != null) {
                        for (File cachedFile : cachedFiles) {
                            if (Utils.isDueByFileName(cachedFile.getName())) {
                                cachedFile.delete();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void put(File file) {

            int curCacheCount = cacheCount.get();
            while (curCacheCount + 1 > countLimit) {
                long freedSize = removeNext();
                cacheSize.addAndGet(-freedSize);

                curCacheCount = cacheCount.addAndGet(-1);
            }
            cacheCount.addAndGet(1);

            long valueSize = calculateSize(file);
            long curCacheSize = cacheSize.get();
            while (curCacheSize + valueSize > sizeLimit) {
                long freedSize = removeNext();
                curCacheSize = cacheSize.addAndGet(-freedSize);
            }
            cacheSize.addAndGet(valueSize);

            Long currentTime = System.currentTimeMillis();
            file.setLastModified(currentTime);
            lastUsageDates.put(file, currentTime);
        }

        private File get(String key) {
            if (TextUtils.isEmpty(key)) {
                return null;
            }

            File file = findFile(key);
            if (file == null || !file.exists()) {
                return null;
            }

            Long currentTime = System.currentTimeMillis();
            file.setLastModified(currentTime);
            lastUsageDates.put(file, currentTime);

            return file;
        }

        private File findFile(String key) {
            if (TextUtils.isEmpty(key)) {
                return null;
            }
            String cacheFileName = doHashKey(key);
            File[] files = cacheRootDir.listFiles();
            if (files != null) {
                for (File f : files) {
                    if (f.getName().startsWith(cacheFileName)) {
                        return f;
                    }
                }
            }
            return null;
        }


        private File newFile(String key, String timeInfo) {
            String cacheFileName = doHashKey(key);
            return new File(cacheRootDir, cacheFileName + TIME_INFO_SEPARATOR + timeInfo);
            //return new File(cacheDir, key.hashCode() + "");
        }

        private boolean remove(String key) {
            File image = get(key);
            if (image == null) {
                return false;
            }
            return image.delete();
        }

        private void clear() {
            lastUsageDates.clear();
            cacheSize.set(0);
            File[] files = cacheRootDir.listFiles();
            if (files != null) {
                for (File f : files) {
                    f.delete();
                }
            }
        }

        /**
         * 移除旧的文件
         *
         * @return
         */
        private long removeNext() {
            if (lastUsageDates.isEmpty()) {
                return 0;
            }

            Long oldestUsage = null;
            File mostLongUsedFile = null;
            Set<Entry<File, Long>> entries = lastUsageDates.entrySet();
            synchronized (lastUsageDates) {
                for (Entry<File, Long> entry : entries) {
                    if (mostLongUsedFile == null) {
                        mostLongUsedFile = entry.getKey();
                        oldestUsage = entry.getValue();
                    } else {
                        Long lastValueUsage = entry.getValue();
                        if (lastValueUsage < oldestUsage) {
                            oldestUsage = lastValueUsage;
                            mostLongUsedFile = entry.getKey();
                        }
                    }
                }
            }

            long fileSize = calculateSize(mostLongUsedFile);
            if (mostLongUsedFile.delete()) {
                lastUsageDates.remove(mostLongUsedFile);
            }
            return fileSize;
        }

        private long calculateSize(File file) {
            return file.length();
        }
    }

}
