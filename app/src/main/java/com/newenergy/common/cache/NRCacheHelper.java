package com.newenergy.common.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Looper;
import android.text.TextUtils;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * 用来操作有关cache的工具类
 * Created by zhaonan on 16/10/8.
 */
public class NRCacheHelper {
    public final static String OBJECT_CACHE_DIR = "object_cache";

    private static NRCacheHelper cacheInstance = null;
    private NRCache mObjCache;

    private NRCacheHelper(Context context) {
        mObjCache = NRCache.get(context, OBJECT_CACHE_DIR, NRCache.MAX_SIZE);
    }

    private static NRCacheHelper getInstance(Context context) {
        try {
            if (context == null || context.getApplicationContext() == null) {
                return null;
            }

            if (cacheInstance == null || cacheInstance.mObjCache == null) {
                synchronized (NRCacheHelper.class) {
                    cacheInstance = new NRCacheHelper(context.getApplicationContext());
                }
            }
            return cacheInstance;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 对象文件默认保存时间为3天 bitmap没有时间限制（放入了glide缓存文件夹中） <br>非ui线程调用<br>
     *
     * @param key
     * @param obj
     * @return 是否成功
     */
    public static File put(Context ctx, String key, Object obj) {
        return put(ctx, key, obj, NRCache.DEFAULT_EXPERIED_TIME);
    }

    /**
     * 暂时不对外开放 有需要的时候改成public即可 非ui线程调用
     *
     * @param key      保存的key
     * @param obj      保存的数据
     * @param saveTime 保存的时间，单位：秒
     * @return 保存的文件 没有成功为null
     */
    private static File put(Context ctx, String key, Object obj, int saveTime) {
        try {
            if (ctx == null || TextUtils.isEmpty(key) || obj == null || saveTime < 0) {
                return null;
            }

            if (ensureOnBgT(ctx)) {
                return cacheInstance.mObjCache.put(key, obj, saveTime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * 非ui线程调用
     *
     * @param key
     * @return
     */
    public static String getAsString(Context ctx, String key) {
        return getAsClass(ctx, key, String.class);
    }

    /**
     * 返回一个list
     *
     * @param key
     * @return
     */
    public static List getAsList(Context ctx, String key) {
        return getAsClass(ctx, key, List.class);
    }

    public static Map getAsMap(Context ctx, String key) {
        return getAsClass(ctx, key, Map.class);
    }

    /**
     * 非ui线程调用
     *
     * @param key
     * @return
     */
    public static Bitmap getAsBitmap(Context ctx, String key) {
        try {
            if (ensureOnBgT(ctx)) {
                return cacheInstance.mObjCache.getAsBitmap(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 非ui线程调用
     * 删除缓存文件
     *
     * @param key
     */
    public static void removeFromCache(Context ctx, String key) {
        if (ensureOnBgT(ctx)) {
            cacheInstance.mObjCache.remove(key);
        }
    }

    /**
     * 非ui线程调用
     *
     * @param key
     * @return
     */
    public static Object getAsObject(Context ctx, String key) {
        if (ensureOnBgT(ctx)) {
            return cacheInstance.mObjCache.getAsObject(key);
        }
        return null;
    }

    /**
     * 用来获取对象 bitmap最好不要用这个方式获取 使用{@link #getAsBitmap(Context ctx, String key)} <br>
     * 非ui线程调用
     *
     * @param key
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> T getAsClass(Context ctx, String key, Class<T> cls) {
        try {
            if (cls.toString().equals(Bitmap.class.toString())) {
                return (T) getAsBitmap(ctx, key);
            } else {
                Object obj = cacheInstance.getAsObject(ctx, key);
                if (obj != null) {
                    obj = cls.cast(obj);
                    return (T) obj;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取缓存的bitmap文件 可在ui线程调用
     *
     * @param key
     * @return
     */
    public static File getCachedFile(Context ctx, String key) {
        try {
            if (ctx == null || TextUtils.isEmpty(key)) {
                return null;
            }

            if (cacheInstance == null || cacheInstance.mObjCache == null) {
                getInstance(ctx);
            }

            if (cacheInstance == null || cacheInstance.mObjCache == null) {
                return null;
            }

            return cacheInstance.mObjCache.getCachedFilePath(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 删除所有缓存的对象文件 非ui线程调用
     */
    public static void clearAllCachedFiles(Context ctx) {
        try {
            if (ensureOnBgT(ctx)) {
                cacheInstance.mObjCache.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 确保是BackgroundThread线程
     *
     * @return
     */
    private static boolean ensureOnBgT(Context ctx) {

        if (ctx == null || ctx.getApplicationContext() == null) {
            return false;
        }

        if (cacheInstance == null || cacheInstance.mObjCache == null) {
            getInstance(ctx.getApplicationContext());
        }

        if (cacheInstance != null && cacheInstance.mObjCache != null && Looper.myLooper() != Looper.getMainLooper()) {
            return true;
        }
        return false;
    }

//    /**
//     * 异步清除缓存文件
//     *
//     * @param key
//     * @param activity 可为null 如果为null则不能回调
//     * @param callback 可为null 回调在UI线程
//     */
//    public static void removeFromCacheAsync(final String key, final Activity activity, final CacheAsyncCallback callback) {
//        ThreadPoolManager.getInstance().addExecuteTask(new Runnable() {
//            @Override
//            public void run() {
//                removeFromCache(key);
//                if (callback != null && activity != null && !activity.isFinishing()) {
//                    activity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            callback.onCacheRemoved(key);
//                        }
//                    });
//                }
//            }
//        });
//    }

    /**
     * 比较耗时 非ui线程调用
     */
    public static void clearExpiredFiles(Context ctx) {
        try {
            if (ensureOnBgT(ctx)) {
                cacheInstance.mObjCache.clearExpiredFiles();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
