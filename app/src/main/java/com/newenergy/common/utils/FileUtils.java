package com.newenergy.common.utils;

import java.io.File;

/**
 * Created by liushuai on 2017/10/18.
 */

public class FileUtils {
    public static boolean fileExist(String path) {
        File file = new File(path);
        return file.exists();
    }
}
