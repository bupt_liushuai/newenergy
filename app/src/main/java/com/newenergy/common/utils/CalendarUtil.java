package com.newenergy.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ls on 17/9/12.
 */

public class CalendarUtil {

    public static String getNowTime(String dateformat) {
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateformat);
        String result = dateFormat.format(now);
        return result;
    }

    public static String getTime(long timeStamp, String dateformat){
        Date now = new Date(timeStamp);
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateformat);
        String result = dateFormat.format(now);
        return result;
    }

    public static Date getDate(long timeStamp, String foramate) {
        Date now = new Date(timeStamp);
        SimpleDateFormat dateFormat = new SimpleDateFormat(foramate);
        String result = dateFormat.format(now);
        try {
            return dateFormat.parse(result);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static boolean equals2Date(Date date1, Date date2) {
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String day1 = myFormatter.format(date1);
            String day2 = myFormatter.format(date2);
            return day1.equals(day2);
        } catch (Exception e) {
            return false;
        }
    }

    public static int getYearByDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(calendar.YEAR);
    }

    public static int getMonthByDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(calendar.MONTH);
    }

    public static int getDayByDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(calendar.DAY_OF_MONTH);
    }

    public static String getDayTimeByDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return (calendar.get(calendar.MONTH) + 1) + "月" + calendar.get(calendar.DAY_OF_MONTH) + "日";
    }

    public static Date getDate(int year, int month, int dayOfMonth, String formate) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dayOfMonth);
        return getDate(calendar.getTimeInMillis(), formate);
    }
}
