package com.newenergy.common.threadpool.queue;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Process;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class NRQueue extends HandlerThread {

    private static final String TAG = NRQueue.class.getSimpleName();
    private static final String THREAD_NAME = TAG + "_Thread";

    private Handler mainHandler;
    private Handler workHandler;
    private NRError error;
    private LinkedBlockingQueue<NRWorker> workerQueue = new LinkedBlockingQueue<>();

    private Bundle workArgs;
    private AtomicBoolean isWorking = new AtomicBoolean(false);

    public NRQueue(String name) {
        this(name, Process.THREAD_PRIORITY_DEFAULT);
    }

    public NRQueue(String name, int priority) {
        this(name, priority, null);
    }

    public NRQueue(String name, int priority, Bundle workArgs) {
        super(name, priority);
        this.workArgs = workArgs;
    }

    public static NRQueue queue() {
        return queue(THREAD_NAME);
    }

    public static NRQueue queue(String name) {
        return new NRQueue(name);
    }

    public NRQueue go(NRError NRError) {
        error = NRError;
        if (!isWorking()) {
            isWorking.set(true);
            if (!isAlive()) {
                start();
            }
            if (workHandler == null) {
                workHandler = new Handler(getLooper());
            }
            for (NRWorker worker : workerQueue) {
                worker.queueing(workHandler);
            }
            workerQueue.clear();
        }
        return this;
    }

    public NRQueue go() {
        return go(null);
    }

    public NRQueue rest(final long timeMillis) {
        on(new NRDo() {
            public void doing(NRQueue queue, Bundle args) {
                try {
                    Thread.sleep(timeMillis);
                } catch (Exception e) {
                }
            }
        });
        return this;
    }

    public NRQueue onUI(NRDo thing) {
        if (thing != null) {
            NRWorker work = new NRWorker(true, this, thing);
            if (isWorking()) {
                if (workHandler != null) {
                    workHandler.post(work);
                }
            } else {
                workerQueue.add(work);
            }
        }
        return this;
    }

    public NRQueue onUIAtTime(long time, NRDo thing) {
        if (thing != null) {
            NRWorker work = new NRWorker(true, this, thing, NRWorker.AT_TIME, time);
            if (isWorking()) {
                if (workHandler != null) {
                    workHandler.postAtTime(work, time);
                }
            } else {
                workerQueue.add(work);
            }
        }
        return this;
    }

    public NRQueue onUIDelayed(long time, NRDo thing) {
        if (thing != null) {
            NRWorker work = new NRWorker(true, this, thing, NRWorker.DELAY, time);
            if (isWorking()) {
                if (workHandler != null) {
                    workHandler.postDelayed(work, time);
                }
            } else {
                workerQueue.add(work);
            }
        }
        return this;
    }

    public NRQueue on(NRDo thing) {
        if (thing != null) {
            NRWorker work = new NRWorker(this, thing);
            if (isWorking()) {
                if (workHandler != null) {
                    workHandler.post(work);
                }
            } else {
                workerQueue.add(work);
            }
        }
        return this;
    }

    public NRQueue onAtTime(long time, NRDo thing) {
        if (thing != null) {
            NRWorker work = new NRWorker(this, thing, NRWorker.AT_TIME, time);
            if (isWorking()) {
                if (workHandler != null) {
                    workHandler.postAtTime(work, time);
                }
            } else {
                workerQueue.add(work);
            }
        }
        return this;
    }

    public NRQueue onDelayed(long time, NRDo thing) {
        if (thing != null) {
            NRWorker work = new NRWorker(this, thing, NRWorker.DELAY, time);
            if (isWorking()) {
                if (workHandler != null) {
                    workHandler.postDelayed(work, time);
                }
            } else {
                workerQueue.add(work);
            }
        }
        return this;
    }

    public void off(NRDo thing) {
        if (thing != null) {
            NRWorker work = new NRWorker(this, thing);
            if (isWorking()) {
                if (workHandler != null) {
                    workHandler.removeCallbacks(work);
                }
            } else {
                workerQueue.remove(work);
            }
        }
    }

    public void off() {
        if (workHandler != null) {
            workHandler.removeCallbacksAndMessages(null);
        }
        if (mainHandler != null) {
            mainHandler.removeCallbacksAndMessages(null);
        }
        workerQueue.clear();
    }

    @SuppressLint("NewApi")
    public void release() {
        try {
            if (isWorking()) {
                isWorking = new AtomicBoolean(false);
                quitSafely();
                interrupt();
            }
            workHandler = null;
            mainHandler = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isWorking() {
        return isWorking.get();
    }

    public Handler getMainHandler() {
        if (mainHandler == null) {
            mainHandler = new Handler(Looper.getMainLooper());
        }
        return mainHandler;
    }

    public NRError getNRError() {
        return error;
    }

    public Bundle getWorkArgs() {
        return workArgs;
    }
}