package com.newenergy.common.threadpool.queue;

import android.os.Handler;

import java.lang.ref.WeakReference;

public class NRWorker implements Runnable {

    public static final int NORMAL = 1;
    public static final int AT_TIME = 2;
    public static final int DELAY = 3;

    private NRDo thing;
    private WeakReference<NRQueue> queue;

    private int type;
    private long time = 0;
    private boolean isMainThread = false;

    protected NRWorker(NRQueue queue, NRDo thing) {
        this(false, queue, thing);
    }

    protected NRWorker(boolean isMainThread, NRQueue queue, NRDo thing) {
        this(isMainThread, queue, thing, NORMAL, 0);
    }

    protected NRWorker(NRQueue queue, NRDo thing, int type, long time) {
        this(false, queue, thing, type, time);
    }

    protected NRWorker(boolean isMainThread, NRQueue queue, NRDo thing, int type, long time) {
        this.isMainThread = isMainThread;
        this.queue = new WeakReference<>(queue);
        this.thing = thing;
        this.type = type;
        this.time = time;
    }

    protected void queueing(Handler handler) {
        if (handler != null) {
            if (type == NORMAL) {
                handler.post(this);
            } else if (type == AT_TIME) {
                handler.postAtTime(this, time);
            } else if (type == DELAY) {
                handler.postDelayed(this, time);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        NRWorker easyThing = (NRWorker) o;
        return thing != null ? !thing.equals(easyThing.thing) : easyThing.thing != null;
    }

    @Override
    public int hashCode() {
        int result = thing != null ? thing.hashCode() : 0;
        return result;
    }

    @Override
    public void run() {
        if (this.queue == null || this.thing == null) {
            return;
        }
        final NRQueue queue = this.queue.get();
        if (queue != null && queue.isWorking()) {
            if (isMainThread) {
                // 在UIThread执行
                queue.getMainHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            thing.doing(queue, queue.getWorkArgs());
                        } catch (final Exception e) {
                            handlerException(queue, e);
                        }
                    }
                });
            } else {
                // 在HandlerThread中执行
                try {
                    thing.doing(queue, queue.getWorkArgs());
                } catch (final Exception e) {
                    queue.getMainHandler().post(new Runnable() {
                        @Override
                        public void run() {
                            handlerException(queue, e);
                        }
                    });
                }
            }
        }
    }

    private void handlerException(final NRQueue queue, Exception e) {
        NRError error = queue.getNRError();
        if (error != null) {
            error.doing(e, queue.getWorkArgs());
        }
    }
}