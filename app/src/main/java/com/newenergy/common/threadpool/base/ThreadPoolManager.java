package com.newenergy.common.threadpool.base;


import android.support.v4.BuildConfig;
import android.util.Log;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 线程池工具
 * <p>
 * 由AbNetEaseTask调用 内部控制管理不对外提供调用 对外使用NetEaseRunnable或NetEaseTask即可
 * <p>
 * 使用PriorityBlockingQueue 无限制大小队列 做任务执行队列 并且根据sComparePriority优先级排序
 * <p>
 * Created by jason on 2017/4/1.
 */

final class ThreadPoolManager implements ThreadPool {

    private static final String TAG = ThreadPoolManager.class.getSimpleName();

    // 获取CPU数量
    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
    // 线程池维护线程的最少数量
    private static final int CORE_POOL_SIZE = Math.max(2, Math.min(CPU_COUNT - 1, 4));
    // 线程池维护线程的最大数量
    private static final int MAXIMUM_POOL_SIZE = CPU_COUNT * 2 + 1;
    // 线程池维护线程所允许的空闲时间
    private static final int KEEP_ALIVE_SECONDS = 100;
    // 线程池所使用的执行队列大小
    private static final int WORK_QUEUE_SIZE = 1 << (CPU_COUNT * 2);
    // 保存ID和任务集合
    private static final Map<Long, Task> sMap = new ConcurrentHashMap<>();
    private static ComparePriority sComparePriority = new ComparePriority();
    // 任务执行队列
    private static final BlockingQueue<Runnable> sPoolWorkQueue
            = new PriorityBlockingQueue<>(WORK_QUEUE_SIZE, sComparePriority);
    // 执行线程池
    private static volatile ThreadPoolExecutor sThreadPool;
    // 单例
    private static ThreadPoolManager sInstance;

    // 初始化线程池
    static {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_SECONDS, TimeUnit.SECONDS,
                sPoolWorkQueue,
                // 创建线程工厂
                new ThreadFactory() {
                    private final AtomicInteger mCount = new AtomicInteger(1);

                    public Thread newThread(Runnable r) {
                        String threadName = "NetEase Thread #" + mCount.getAndIncrement();
                        log(threadName);
                        return new Thread(r, threadName);
                    }
                },
                // 任务执行队列溢出保护处理-放入任务缓冲队列
                new RejectedExecutionHandler() {
                    public void rejectedExecution(Runnable task, ThreadPoolExecutor executor) {
                        log(new TaskException("PoolWorkQueue is Full : Rejected Execution Handler"));
                    }
                }) {
            @Override
            protected void beforeExecute(Thread t, Runnable r) {
                super.beforeExecute(t, r);
                if (r instanceof Task) {
                    Task task = (Task) r;
                    if (!sMap.containsKey(task.getTaskId())) {
                        sMap.put(task.getTaskId(), task);
                    }
                    log("beforeExecute " + t.getName() + " @" + task.getTaskId() + " *" + task.getPriority());
                } else {
                    log("beforeExecute " + t.getName());
                }
            }

            @Override
            protected void afterExecute(Runnable r, Throwable t) {
                super.afterExecute(r, t);
                if (r instanceof Task) {
                    Task task = (Task) r;
                    if (sMap.containsKey(task.getTaskId())) {
                        sMap.remove(task.getTaskId());
                    }
                    log("afterExecute NetEase Thread @" + task.getTaskId() + " *" + task.getPriority());
                } else {
                    log("afterExecute NetEase Thread");
                }
            }
        };
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        sThreadPool = threadPoolExecutor;
    }

    private ThreadPoolManager() {
    }

    public static synchronized ThreadPoolManager getInstance() {
        if (sInstance == null) {
            sInstance = new ThreadPoolManager();
        }
        return sInstance;
    }

    /**
     * 执行线程 放入线程池
     *
     * @param task
     */
    @Override
    public void execute(Task task) {
        if (task == null) {
            return;
        }
        sMap.put(task.getTaskId(), task);
        sThreadPool.execute(task);
    }

    /**
     * 执行线程 移出线程池
     * 或任务缓冲队列中移除
     *
     * @param task
     */
    @Override
    public void remove(Task task) {
        if (task == null) {
            return;
        }
        sMap.remove(task.getTaskId());
        sThreadPool.remove(task);
    }

    /**
     * 获取ITask 调用cancel -- 最终会调用 @com.netease.nr.base.ThreadPool.ThreadPoolManager.remove()
     *
     * @param taskId
     */
    @Override
    public void remove(long taskId) {
        Task task = sMap.remove(taskId);
        if (task != null) {
            task.cancel();
        }
    }

    /**
     * 关闭线程池 并清理任务缓冲队列
     */
    @Override
    public void shutdown() {
        sThreadPool.shutdown();
        sMap.clear();
        sPoolWorkQueue.clear();
    }

    private static void log(String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, msg);
        }
    }

    private static void log(TaskException e) {
        Log.e(TAG, e.getMessage());
    }

    /**
     * 任务排序
     * 根据优先级排序 若优先级相同根据taskid排序（taskid越小创建越早）
     *
     * @param <T>
     */
    static class ComparePriority<T extends Task> implements Comparator<T> {

        @Override
        public int compare(T lhs, T rhs) {
            int flag = rhs.getPriority() - lhs.getPriority();
            return flag == 0 ? (rhs.getTaskId() > lhs.getTaskId() ? -1 : 1) : flag;
        }
    }
}
