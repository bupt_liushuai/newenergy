package com.newenergy.common.threadpool.base;

/**
 * 线程池接口
 * <p>
 * execute 执行
 * remove(Task) 移除Task
 * remove(TaskId) 移除Task 通过TaskId
 * shutdown 全部关闭
 * <p>
 * Created by jason on 2017/4/1.
 */

interface ThreadPool {

    void execute(Task runnable);

    void remove(Task runnable);

    void remove(long taskId);

    void shutdown();
}
