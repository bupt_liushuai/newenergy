package com.newenergy.common.constant;

import android.os.Environment;

/**
 * Created by liushuai on 2017/10/18.
 */

public class Constant {
    public final static String PATH_PDF = "/sdcard/train/";

    public final static String RCXC = "RCXC";//日常巡查，
    // "ZYDJ"专业点检，
    // "SBWX"设备维修，
    public final static String RBGL = "RBGL";//日报管理，
    public final static String AQXC = "AQXC";//安全巡查
    public final static String PXYL = "PXYL";//培训演练
    public final static String KEGL_HT = "KEGL_HT";//客户管理-客户合同
    public final static String KEGL_HFJL = "KEGL_HFJL";//客户管理-回访记录
}
