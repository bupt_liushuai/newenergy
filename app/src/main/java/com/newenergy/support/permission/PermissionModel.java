package com.newenergy.support.permission;

import android.Manifest;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by liu_shuai on 2017/6/27.
 */

public class PermissionModel {

    public final static Map<String, Integer> mPermissionMap = new HashMap<>();
    public final static int CODE_PERSION_READ_PHONE = 101;
    public final static String NAME_PERSION_READ_PHONE = Manifest.permission.READ_PHONE_STATE;
    static {
        mPermissionMap.put(NAME_PERSION_READ_PHONE, CODE_PERSION_READ_PHONE);
    }

    public static int getCode(String name){
        return mPermissionMap.get(name);
    }
}
