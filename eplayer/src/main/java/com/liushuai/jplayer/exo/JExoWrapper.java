package com.liushuai.jplayer.exo;

import android.content.Context;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.trackselection.TrackSelector;

/**
 * Created by liushuai on 2017/10/12.
 */

public class JExoWrapper {
    private ExoPlayer mPlayer;

    public JExoWrapper(Context context, ComponentProvider componentProvider) {
        mPlayer = ExoPlayerFactory.newSimpleInstance(componentProvider.buildReanderFactory(), componentProvider.buildTrackSelector(), componentProvider.buildLoadControl());
    }

    public interface ComponentProvider {
        RenderersFactory buildReanderFactory();
        TrackSelector buildTrackSelector();
        LoadControl buildLoadControl();
    }


}
